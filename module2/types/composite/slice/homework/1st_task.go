package main

/*
// Первый вариант решения
func main() {
	s := []int{1, 2, 3}
	Append(s)
	s = Append(s)
	fmt.Println(s)
}

func Append(s []int) []int {
	s = append(s, 4)
	return s
}*/

// Второй вариант решения
// Взят в "/**/"для работы первого варианта
/*
func main() {
	s := []int{1, 2, 3}
	Append(&s)
	fmt.Println(s)
}

func Append(s *[]int) []int {
	*s = append(*s, 4)
	return *s
}*/
