package main

import (
	"encoding/json"
	jsoniter "github.com/json-iterator/go"
)

var jsonData = []byte(`[
  {
    "id": 9223372036854668000,
    "category": {
      "id": 0,
      "name": "string"
    },
    "name": "xiaomi",
    "photoUrls": [
      "string"
    ],
    "tags": [
      {
        "id": 0,
        "name": "string"
      }
    ],
    "status": "available"
  },
  {
    "id": 9223372036854668000,
    "category": {
      "id": 0,
      "name": "string"
    },
    "name": "doggie",
    "photoUrls": [
      "string"
    ],
    "tags": [
      {
        "id": 0,
        "name": "string"
      }
    ],
    "status": "available"
  },
  {
    "id": 9223372036854668000,
    "category": {
      "id": 0,
      "name": "string"
    },
    "name": "fish",
    "photoUrls": [
      "string"
    ],
    "tags": [
      {
        "id": 0,
        "name": "string"
      }
    ],
    "status": "available"
  },
  {
    "id": 9223372036854668000,
    "category": {
      "id": 0,
      "name": "string"
    },
    "name": "fish",
    "photoUrls": [
      "string"
    ],
    "tags": [
      {
        "id": 0,
        "name": "string"
      }
    ],
    "status": "available"
  },
  {
    "id": 9223372036854668000,
    "category": {
      "id": 0,
      "name": "string"
    },
    "name": "fish",
    "photoUrls": [
      "string"
    ],
    "tags": [
      {
        "id": 0,
        "name": "string"
      }
    ],
    "status": "available"
  },
  {
    "id": 9223372036854668000,
    "category": {
      "id": 0,
      "name": "DOg"
    },
    "name": "Fluffy",
    "photoUrls": [
      "string"
    ],
    "tags": [
      {
        "id": 0,
        "name": "string"
      }
    ],
    "status": "available"
  },
  {
    "id": 9223372036854668000,
    "category": {
      "id": 0,
      "name": "string"
    },
    "name": "doggie",
    "photoUrls": [
      "string"
    ],
    "tags": [
      {
        "id": 0,
        "name": "string"
      }
    ],
    "status": "available"
  },
  {
    "id": 9223372036854668000,
    "category": {
      "id": 0,
      "name": "Dog"
    },
    "name": "abc",
    "photoUrls": [
      "string"
    ],
    "tags": [
      {
        "id": 0,
        "name": "string"
      }
    ],
    "status": "available"
  },
  {
    "id": 9223372036854668000,
    "category": {
      "id": 0,
      "name": "string"
    },
    "name": "doggie",
    "photoUrls": [
      "string"
    ],
    "tags": [
      {
        "id": 0,
        "name": "string"
      }
    ],
    "status": "available"
  },
  {
    "id": 9223372036854668000,
    "category": {
      "id": 0,
      "name": "string"
    },
    "name": "doggie",
    "photoUrls": [
      "string"
    ],
    "tags": [
      {
        "id": 0,
        "name": "string"
      }
    ],
    "status": "available"
  }
]`)

type Pets []Pet

func UnmarshalPets(data []byte) (Pets, error) {
	var r Pets
	err := json.Unmarshal(data, &r)
	return r, err
}

func (r *Pets) Marshal() ([]byte, error) {
	return json.Marshal(r)
}

func UnmarshalPets2(data []byte) (Pets, error) {
	var r Pets
	err := jsoniter.Unmarshal(data, &r)
	return r, err
}

func (r *Pets) Marshal2() ([]byte, error) {
	return jsoniter.Marshal(r)
}

type Pet struct {
	ID        float64    `json:"id"`
	Category  Category   `json:"category"`
	Name      string     `json:"name"`
	PhotoUrls []Name     `json:"photoUrls"`
	Tags      []Category `json:"tags"`
	Status    Status     `json:"status"`
}

type Name string

const (
	DOg    Name = "DOg"
	Dog    Name = "Dog"
	String Name = "string"
)

type Category struct {
	ID   int64  `json:"id"`
	Name string `json:"name"`
}

type Status string

const (
	Available Status = "available"
)
