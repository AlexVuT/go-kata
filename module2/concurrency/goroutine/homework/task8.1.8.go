package main

import (
	"fmt"
	"time"
)

//func parseURL(url string) {
//	for i := 0; i < 5; i++ {
//		latency := rand.Intn(500) + 500
//
//		time.Sleep(time.Duration(latency) * time.Millisecond)
//
//		fmt.Printf("Parsing <%s> - Step %d - Latency %d\n", url, i+1, latency)
//	}
//}

//func doHTTP(url string) {
//	t := time.Now()
//
//	resp, err := http.Get(url)
//	if err != nil {
//		fmt.Printf("Failed to get <%s>: %s\n", url, err.Error())
//	}
//
//	defer resp.Body.Close()
//
//	fmt.Printf("<%s> - Status Code [%d] - Latency %d ms\n", url, resp.StatusCode, time.Since(t).Milliseconds())
//}

func main() {
	message1 := make(chan string)
	message2 := make(chan string)

	go func() {
		for {
			time.Sleep(time.Millisecond * 500)
			message1 <- "Прошло пол секунды"
		}
	}()

	go func() {
		for {
			time.Sleep(time.Second * 2)
			message1 <- "Прошло 2 секунды"
		}
	}()

	for {
		select {
		case msg := <-message1:
			fmt.Println(msg)
		case msg := <-message2:
			fmt.Println(msg)
		}
	}
	//	urls := []string{
	//		"https://google.com/",
	//		"https://youtube.com/",
	//		"https://github.com/",
	//		//"https://instagram.com/",закомментено, так как нет доступа, из-за чего вызывается паника
	//		"https://medium.com/",
	//	}
	//	var wg sync.WaitGroup
	//	for _, url := range urls {
	//		wg.Add(1)
	//		go func(url string) {
	//			doHTTP(url)
	//			wg.Done()
	//		}(url)
	//	}
	//	wg.Wait()
	//

	//message := make(chan string, 2)
	//message <- "hello"
	//message <- "world"
	//fmt.Println(<-message)
	//message <- "!"
	//fmt.Println(<-message)
	//fmt.Println(<-message)

	//go func() {
	//	for i := 0; i <= 10; i++ {
	//		message <- fmt.Sprintf("%d", i)
	//		time.Sleep(time.Millisecond * 500)
	//	}
	//	close(message)
	//}()
	//
	//for msg := range message {
	//	fmt.Println(msg)
	//}

	//for {
	//msg, open := <-message
	//if !open {
	//	break
	//}
	//fmt.Println(msg)
	//}
	//go func() {
	//	time.Sleep(2 * time.Second)
	//	message <- "hello"
	//}()
	//
	//msg := <-message
	//fmt.Println(msg)

	//t := time.Now()
	//rand.Seed(t.UnixNano())
	//
	//go parseURL("http://example.com/")
	//parseURL("http://youtube.com/")
	//
	//fmt.Printf("Parsing completed. Time Elapsed: %.2f seconds\n", time.Since(t).Seconds())

	//go fmt.Println("Hello from goroutine")
	//
	//go fmt.Println("Hello from main()")
	//
	//time.Sleep(time.Millisecond)
}
