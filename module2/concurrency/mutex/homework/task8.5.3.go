package main

import (
	"fmt"
	"golang.org/x/sync/errgroup"
	"sync"
)

type Cache struct {
	data  map[string]interface{}
	init  bool
	mutex sync.Mutex
}

func NewCache() *Cache {
	return &Cache{
		data: make(map[string]interface{}, 100),
		init: true,
	}
}

func (c *Cache) Set(key string, v interface{}, mutex sync.Mutex) error {
	if !c.init {
		return fmt.Errorf("cache isnt initialized")
	}
	mutex.Lock()
	c.data[key] = v
	mutex.Unlock()
	return nil
}

func (c *Cache) Get(key string, mutex sync.Mutex) interface{} {
	if !c.init {
		return nil
	}
	mutex.Lock()
	defer mutex.Unlock()
	return c.data[key]
}

func main() {

	cache := NewCache()
	keys := []string{
		"programming",
		"is",
		"so",
		"awesome",
		"write",
		"clean",
		"code",
		"use",
		"solid",
		"principles",
	}
	var eg errgroup.Group
	for i := range keys {
		idx := i
		eg.Go(func() error {
			return cache.Set(keys[idx], idx, cache.mutex)
		})
	}

	err := eg.Wait()
	if err != nil {
		panic(err)
	}
	fmt.Println(cache)
}
