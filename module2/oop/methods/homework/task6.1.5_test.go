package main

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestSum(t *testing.T) {
	type args struct {
		a float64
		b float64
	}

	type testCase struct {
		name string
		args args
		want float64
	}

	tests := []testCase{
		{
			name: "сложение при равных аргументах",
			args: args{a: 4, b: 4},
			want: 8,
		},
		{
			name: "сложение, когда a меньше b",
			args: args{a: 3, b: 7},
			want: 10,
		},
		{
			name: "сложение, когда a больше b",
			args: args{a: 45, b: 8},
			want: 53,
		},

		{
			name: "сложение, когда a равно нулю",
			args: args{a: 0, b: 77},
			want: 77,
		},

		{
			name: "сложение, когда b равно нулю",
			args: args{a: 147, b: 0},
			want: 147,
		},

		{
			name: "сложение, когда один из аргументов является дробным числом",
			args: args{a: 62.782, b: 8},
			want: 70.782,
		},

		{
			name: "сложение, когда оба аргумента являются дробными числами",
			args: args{a: 42.791, b: 8.4474},
			want: 51.2384,
		},

		{
			name: "сложение, когда когда один из аргументов отрицательный",
			args: args{a: -45, b: 542},
			want: 497,
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			assert.Equalf(t, test.want, sum(test.args.a, test.args.b), "Add(%v, %v)",
				test.args.a, test.args.b)
		})
	}
}

func TestMultipy(t *testing.T) {
	type args struct {
		a float64
		b float64
	}

	type testCase struct {
		name string
		args args
		want float64
	}

	tests := []testCase{
		{
			name: "умножение при равных аргументах",
			args: args{a: 4, b: 4},
			want: 16,
		},
		{
			name: "умножение, когда a меньше b",
			args: args{a: 3, b: 7},
			want: 21,
		},
		{
			name: "умножение, когда a больше b",
			args: args{a: 45, b: 8},
			want: 360,
		},

		{
			name: "умножение, когда один из аргументов равен нулю",
			args: args{a: 0, b: 77},
			want: 0,
		},

		{
			name: "умножение, когда a является дробным числом",
			args: args{a: 62.782, b: 8},
			want: 502.256,
		},

		{
			name: "умножение, когда b является дробным числом",
			args: args{a: 47, b: 8.4},
			want: 394.8,
		},

		{
			name: "умножение, когда когда один из аргументов отрицательный",
			args: args{a: -45, b: 542},
			want: -24390,
		},

		{
			name: "умножение, когда когда оба аргумента отрицательные",
			args: args{a: -6, b: -78},
			want: 468,
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			assert.Equalf(t, test.want, multiply(test.args.a, test.args.b), "Add(%v, %v)",
				test.args.a, test.args.b)
		})
	}
}

func TestDivide(t *testing.T) {
	type args struct {
		a float64
		b float64
	}

	type testCase struct {
		name string
		args args
		want float64
	}

	tests := []testCase{
		{
			name: "деление при равных аргументах",
			args: args{a: 14, b: 14},
			want: 1,
		},
		{
			name: "деление, когда a меньше b",
			args: args{a: 5, b: 10},
			want: 0.5,
		},
		{
			name: "деление, когда a больше b",
			args: args{a: 45, b: 8},
			want: 5.625,
		},

		{
			name: "деление, когда первый аргумент равен нулю",
			args: args{a: 0, b: 77},
			want: 0,
		},

		{
			name: "деление, когда a является дробным числом",
			args: args{a: 62.7, b: 8},
			want: 7.8375,
		},

		{
			name: "деление, когда b является дробным числом",
			args: args{a: 48, b: 0.4},
			want: 120,
		},

		{
			name: "деление, когда когда один из аргументов отрицательный",
			args: args{a: -49, b: 7},
			want: -7,
		},

		{
			name: "деление, когда когда оба аргумента отрицательные",
			args: args{a: -152, b: -8},
			want: 19,
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			assert.Equalf(t, test.want, divide(test.args.a, test.args.b), "Add(%v, %v)",
				test.args.a, test.args.b)
		})
	}
}

func TestAverage(t *testing.T) {
	type args struct {
		a float64
		b float64
	}

	type testCase struct {
		name string
		args args
		want float64
	}

	tests := []testCase{
		{
			name: "среднее значение при равных аргументах",
			args: args{a: 14, b: 14},
			want: 14,
		},
		{
			name: "среднее значение, когда a меньше b",
			args: args{a: 5, b: 10},
			want: 7.5,
		},
		{
			name: "среднее значение, когда a больше b",
			args: args{a: 45, b: 8},
			want: 26.5,
		},

		{
			name: "среднее значение, когда первый аргумент равен нулю",
			args: args{a: 0, b: 77},
			want: 38.5,
		},

		{
			name: "среднее значение, когда a является дробным числом",
			args: args{a: 62.7, b: 8},
			want: 35.35,
		},

		{
			name: "среднее значение, когда b является дробным числом",
			args: args{a: 48, b: 0.4},
			want: 24.2,
		},

		{
			name: "среднее значение, когда когда один из аргументов отрицательный",
			args: args{a: -49, b: 7},
			want: -21,
		},

		{
			name: "среднее значение, когда когда оба аргумента отрицательные",
			args: args{a: -152, b: -8},
			want: -80,
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			assert.Equalf(t, test.want, average(test.args.a, test.args.b), "Add(%v, %v)",
				test.args.a, test.args.b)
		})
	}
}
