package main

import (
	jsoniter "github.com/json-iterator/go"
	"github.com/ptflp/godecoder"
)

func main() {
	var json = jsoniter.ConfigCompatibleWithStandardLibrary
	json.Marshal()
	godecoder.Decode{}
}
