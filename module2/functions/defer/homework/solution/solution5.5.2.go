package solution

import "errors"

// MergeDictsJob is a job to merge dictionaries into a single dictionary.
type MergeDictsJob struct {
	Dicts      []map[string]string
	Merged     map[string]string
	IsFinished bool
}

// errors
var (
	errNotEnoughDicts = errors.New("at least 2 dictionaries are required")
	errNilDict        = errors.New("nil dictionary")
)

func ExecuteMergeDictsJob(j *MergeDictsJob) (*MergeDictsJob, error) {
	defer func() { j.IsFinished = true }()

	j.Merged = make(map[string]string)

	if len(j.Dicts) < 2 {
		return j, errNotEnoughDicts
	}
	for _, vDicts := range j.Dicts {
		if vDicts == nil {
			return j, errNilDict
		}
		for key, value := range vDicts {
			j.Merged[key] = value
		}

	}
	return j, nil
}
