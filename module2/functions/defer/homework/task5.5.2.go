package main

import (
	"fmt"
	. "gitlab.com/AlexVuT/go-kata/module2/functions/defer/homework/solution"
)

func main() {

	fmt.Println(ExecuteMergeDictsJob(&MergeDictsJob{}))
	fmt.Println(ExecuteMergeDictsJob(&MergeDictsJob{Dicts: []map[string]string{{"a": "b"}, nil}}))
	fmt.Println(ExecuteMergeDictsJob(&MergeDictsJob{Dicts: []map[string]string{{"a": "b"}, {"b": "c"}}}))

}
