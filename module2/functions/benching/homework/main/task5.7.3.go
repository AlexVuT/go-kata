package main

import (
	"fmt"
	"github.com/brianvoe/gofakeit/v6"
	"math/rand"
)

type User struct {
	ID       int64
	Name     string `fake:"{firstname}"`
	Products []Product
}

type Product struct {
	UserID int64
	Name   string `fake:"{sentence:3}"`
}

func main() {
	users := genUsers()
	products := genProducts()
	users = MapUserProducts(users, products)

	fmt.Println(users)
	fmt.Println("                                                           ")

	users1 := genUsers()
	products1 := genProducts()
	users2 := MapUserProductsVar2(users1, products1)
	fmt.Println(users2)

}

func MapUserProducts(users []User, products []Product) []User {
	// Проинициализируйте карту продуктов по айди пользователей
	for i, user := range users {
		for _, product := range products { // избавьтесь от цикла в цикле
			if product.UserID == user.ID {
				users[i].Products = append(users[i].Products, product)
			}
		}
	}

	return users
}

func MapUserProductsVar2(users []User, products []Product) []User {
	productsMap := make(map[int64][]Product, len(products))
	for _, product := range products {
		productsMap[product.UserID] = append(productsMap[product.UserID], product)
	}

	for i := range users {
		users[i].Products = productsMap[int64(i)]
	}
	return users
}

func genProducts() []Product {
	products := make([]Product, 1000)
	for i, product := range products {
		_ = gofakeit.Struct(&product)
		product.UserID = int64(rand.Intn(100) + 1)
		products[i] = product
	}

	return products
}

func genUsers() []User {
	users := make([]User, 100)
	for i, user := range users {
		_ = gofakeit.Struct(&user)
		user.ID = int64(i)
		users[i] = user
	}

	return users
}
