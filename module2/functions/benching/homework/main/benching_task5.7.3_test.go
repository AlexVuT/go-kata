package main

import (
	"testing"
)

func BenchmarkMapUserProducts(b *testing.B) {
	users := genUsers()
	products := genProducts()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		MapUserProducts(users, products)
	}
}

func BenchmarkMapUserProductsVar2(b *testing.B) {
	users1 := genUsers()
	products1 := genProducts()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		MapUserProductsVar2(users1, products1)
	}
}
