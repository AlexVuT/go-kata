package main

import "fmt"

func main() {

}

func Greet(name string) string {
	if isRussian(name) {
		return fmt.Sprintf("Привет %s, добро пожаловать!", name)
	} else {

		return fmt.Sprintf("Hello %s, you welcome!", name)
	}
}

func isRussian(name string) bool {
	for _, ch := range name {
		if (ch > 'а' && ch < 'я') || (ch > 'А' && ch < 'Я') || ch == 'ё' || ch == 'Ё' {
			return true
		}
	}
	return false
}
