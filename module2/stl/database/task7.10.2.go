package main

import (
	"database/sql"
	"fmt"
	_ "github.com/mattn/go-sqlite3"
)

func main() {
	database, _ := sql.Open("sqlite3", "/home/alex/go/src/AlexVuT/go-kata/module2/stl/database/gopher.db")
	defer database.Close()
	statement, _ := database.Prepare("CREATE TABLE IF NOT EXISTs people (id INTEGER PRIMARY KEY, firstname TEXT, lastname TEXT)")
	statement.Exec()

	statement, _ = database.Prepare("INSERT INTO people (firstname, lastname) VALUES (?,?)")
	statement.Exec("Billy", "Idol")

	//statement, _ = database.Prepare("DELETE FROM people WHERE id= )
	//statement.Exec()

	rows, _ := database.Query("SELECT id, firstname, lastname FROM people")
	var id int
	var firstname string
	var lastname string

	for rows.Next() {
		rows.Scan(&id, &firstname, &lastname)
		fmt.Printf("%d: %s %s\n", id, firstname, lastname)
	}
}
