package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"os"
)

type Config struct {
	AppName    string `json:"app_name"`
	Production bool   `json:"production"`
}

func main() {
	var configName string
	flag.StringVar(&configName, "conf", "", "config file utility")
	flag.Parse()

	configData, err := os.ReadFile(configName)
	if err != nil {
		fmt.Println("Cannot read file, err:", err)
		os.Exit(1)
	}

	conf := Config{}
	err = json.Unmarshal(configData, &conf)
	if err != nil {
		fmt.Println("Cannot unmarshal file, err:", err)
		os.Exit(1)
	}
	fmt.Println("Production:", conf.Production)
	fmt.Println("AppName:", conf.AppName)
}
