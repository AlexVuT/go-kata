package main

import (
	"encoding/json"
	"fmt"
)

//type Student struct {
//	FirstName, lastName string
//	Email               string
//	Age                 int
//	HeightInMeters      float64
//	IsMale              bool
//}

//type Student map[string]interface{}

//type Profile struct {
//	Username  string
//	followers int
//	Grades    map[string]string
//}
//
//type Student struct {
//	FirstName, lastName string
//	Age                 int
//	Profile
//	Languages []string
//}

//type ProfileI interface {
//	Follow()
//}
//
//type Profile struct {
//	Username  string
//	Followers int
//}
//
//func (p *Profile) Follow() {
//	p.Followers++
//}
//
//type Student struct {
//	FirstName, lastName string
//	Age                 int
//	Primary             ProfileI
//	Secondary           ProfileI
//}

//type Profile struct {
//	Username  string
//	Followers int
//}
//
//func (p Profile) MarshalJSON() ([]byte, error) {
//	return []byte(fmt.Sprintf(`{"f_count": "%d"}`, p.Followers)), nil
//}
//
//type Age int
//
//func (a Age) MarshalText() ([]byte, error) {
//	return []byte(fmt.Sprintf(`{"age": %d}`, int(a))), nil
//}
//
//type Student struct {
//	FirstName, lastName string
//	Age                 Age
//	Profile             Profile
//}

type Profile struct {
	Username  string `json:"uname"`
	Followers int    `json:"followers,omitempty,string"`
}

type Student struct {
	FirstName string  `json:"fname"`           // `fname` as field name
	LastName  string  `json:"lname,omitempty"` // discard if value is empty
	Email     string  `json:"-"`               // always discard
	Age       int     `json:"-,"`              // `-` as field name
	IsMale    bool    `json:",string"`         // keep original field name, coerce to a string
	Profile   Profile `json:""`                // no effect
}

func main() {
	john := &Student{
		FirstName: "John",
		LastName:  "",
		Age:       21,
		Email:     "john@doe.com",
		IsMale:    true,
		Profile: Profile{
			Username:  "johndoe91",
			Followers: 1975,
		},
	}

	johnJSON, _ := json.MarshalIndent(john, "", "  ")
	fmt.Println(string(johnJSON))
}
