package main

import (
	"encoding/json"
	"fmt"
	"strings"
)

//type Student struct {
//	FirstName, lastName string
//	Email               string
//	Age                 int
//	HeightInMeters      float64
//}

//type Profile struct {
//	Username  string
//	Followers int
//}
//
//type Student struct {
//	FirstName, lastName string
//	HeightInMeters      float64
//	IsMale              bool
//	Languages           [2]string
//	Subjects            []string
//	Grades              map[string]string
//	Profile             Profile
//}

//type Profile struct {
//	Username  string
//	Followers int
//}
//
//type Student struct {
//	FirstName, lastName string
//	HeightInMeters      float64
//	IsMale              bool
//	Languages           [2]string
//	Subjects            []string
//	Grades              map[string]string
//	Profile             *Profile
//}

//type Profile struct {
//	Username  string
//	Followers int
//}
//
//type Account struct {
//	IsMale bool
//	Email  string
//}
//
//type Student struct {
//	FirstName, lastName string
//	HeightInMeters      float64
//	IsMale              bool
//	Profile
//	Account
//}

//type Profile struct {
//	Username  string `json:"uname"`
//	Followers int    `json:"f_count"`
//}
//
//type Student struct {
//	FirstName      string   `json:"fname"`
//	LastName       string   `json:"-"` // discard
//	HeightInMeters float64  `json:"height"`
//	IsMale         bool     `json:"male"`
//	Languages      []string `json:",omitempty"`
//	Profile        Profile  `json:"profile"`
//}

//type Student map[string]interface{}

//type Profile struct {
//	Username  string
//	Followers string
//}
//
//func (p *Profile) UnmarshalJSON(data []byte) error {
//
//	var container map[string]interface{}
//	_ = json.Unmarshal(data, &container)
//	fmt.Printf("container: %T / %#v\n\n", container, container)
//
//	iuserName, _ := container["Username"]
//	ifollowers, _ := container["f_count"]
//	fmt.Printf("iuserName: %T/%#v\n", iuserName, iuserName)
//	fmt.Printf("ifollowers: %T/%#v\n\n", ifollowers, ifollowers)
//
//	userName, _ := iuserName.(string)    // get `string` value
//	followers, _ := ifollowers.(float64) // get `float64` value
//	fmt.Printf("userName: %T/%#v\n", userName, userName)
//	fmt.Printf("followers: %T/%#v\n\n", followers, followers)
//
//	p.Username = strings.ToUpper(userName)
//	p.Followers = fmt.Sprintf("%.2fk", followers/1000)
//
//	return nil
//}
//
//type Student struct {
//	FirstName string
//	Profile   Profile
//}

//type Person struct {
//	Name string
//	Age  int
//}

type Person struct {
	Name string
	Age  int
}

func main() {
	jsonStream := strings.NewReader(`
{"Name":"Ross Geller","Age":28}
{"Name":"Monica Geller","Age":27}
{"Name":"Jack Geller","Age":56}
`)

	decoder := json.NewDecoder(jsonStream)

	var ross, monica Person

	decoder.Decode(&ross)
	decoder.Decode(&monica)

	fmt.Printf("ross: %#v\n", ross)
	fmt.Printf("monica: %#v\n", monica)
	fmt.Printf("monica: %#v\n", monica)

	//buf := new(bytes.Buffer)
	//bufEncoder := json.NewEncoder(buf)
	//
	//bufEncoder.Encode(Person{"Ross Geller", 28})
	//bufEncoder.Encode(Person{"Monica Geller", 27})
	//bufEncoder.Encode(Person{"Jack Geller", 56})
	//
	//fmt.Println(buf) // calls `buf.String()` method

	//data := []byte(`
	//{
	//	"FirstName": "John",
	//	"Profile": {
	//		"Username": "johndoe91",
	//		"f_count": 1975
	//	}
	//}`)
	//
	//var john Student
	//
	//fmt.Printf("Error: %v\n", json.Unmarshal(data, &john))
	//
	//fmt.Printf("%#v\n", john)

	//data := []byte(`
	//{
	//	"id": 123,
	//	"fname": "John",
	//	"height": 1.75,
	//	"male": true,
	//	"languages": null,
	//	"subjects": [ "Math", "Science" ],
	//	"profile": {
	//		"uname": "johndoe91",
	//		"f_count": 1975
	//	}
	//}`)
	//
	//var john interface{}
	//fmt.Printf("Before: `type` of `john` is %T and its `value` is %v\n", john, john)
	//
	//fmt.Printf("Error: %v\n", json.Unmarshal(data, &john))
	//fmt.Printf("After: `type` of `john` is %T\n\n", john)
	//
	//fmt.Printf("%#v\n", john)
	//johnData := john.(map[string]interface{})
	//fmt.Println(johnData["fname"])

	//data := []byte(`
	//{
	//	"id": 123,
	//	"fname": "John",
	//	"height": 1.75,
	//	"male": true,
	//	"languages": null,
	//	"subjects": [ "Math", "Science" ],
	//	"profile": {
	//		"uname": "johndoe91",
	//		"f_count": 1975
	//	}
	//}`)
	//
	//var john Student
	//
	//fmt.Printf("Error: %v\n", json.Unmarshal(data, &john))
	//
	//fmt.Printf("%#v\n\n", john)
	//
	//i := 1
	//for k, v := range john {
	//	fmt.Printf("%d: key (`%T`)`%v`, value (`%T`)`%#v`\n", i, k, k, v, v)
	//	i++
	//}

	//data := []byte(`
	//{
	//	"fname": "John",
	//	"LastName": "Doe",
	//	"height": 1.75,
	//	"IsMale": true,
	//	"Languages": null,
	//	"profile": {
	//		"uname": "johndoe91",
	//		"Followers": 1975
	//	}
	//}`)
	//
	//var john Student = Student{
	//	Languages: []string{"English", "French"},
	//}
	//
	//fmt.Printf("Error: %v\n", json.Unmarshal(data, &john))
	//
	//fmt.Printf("%#v\n", john)
	//data := []byte(`
	//{
	//	"FirstName": "John",
	//	"HeightInMeters": 1.75,
	//	"IsMale": true,
	//	"Username": "johndoe91",
	//	"Followers": 1975,
	//	"Account": { "IsMale": true, "Email": "john@doe.com" }
	//}`)
	//
	//var john Student
	//
	//fmt.Printf("Error: %v\n", json.Unmarshal(data, &john))
	//
	//fmt.Printf("%#v\n", john)

	//data := []byte(`
	//{
	//	"FirstName": "John",
	//	"HeightInMeters": 1.75,
	//	"IsMale": null,
	//	"Languages": [ "English" ],
	//	"Subjects": [ "Math", "Science" ],
	//	"Grades": null,
	//	"Profile": { "Followers": 1975 }
	//}`)
	//
	//var john Student = Student{
	//	IsMale:    true,
	//	Languages: [2]string{"Korean", "Chinese"},
	//	Subjects:  nil,
	//	Grades:    map[string]string{"Math": "A"},
	//	Profile:   &Profile{Username: "johndoe91"},
	//}
	//
	//fmt.Printf("Error: %v\n\n", json.Unmarshal(data, &john))
	//fmt.Printf("%#v\n\n", john)
	//fmt.Printf("%#v\n", john.Profile)

	//data := []byte(`
	//{
	//	"FirstName": "John",
	//	"HeightInMeters": 1.75,
	//	"IsMale": null,
	//	"Languages": [ "English", "Spanish", "German" ],
	//	"Subjects": [ "Math", "Science" ],
	//	"Grades": { "Math": "A" },
	//	"Profile": {
	//		"Username": "johndoe91",
	//		"Followers": 1975
	//	}
	//}`)
	//
	//	var john Student = Student{
	//	IsMale:   true,
	//	Subjects: []string{"Art"},
	//	Grades:   map[string]string{"Science": "A+"},
	//}
	//fmt.Printf("Error: %v\n", json.Unmarshal(data, &john))
	//fmt.Printf("%#v\n", john)

	//data := []byte(`
	//{
	//	"FirstName": "John",
	//	"lastName": "Doe",
	//	"Age": 21,
	//	"HeightInMeters": 175,
	//	"Username": "johndoe91"
	//}`)
	//
	//var john Student
	//
	//fmt.Printf("Error: %v\n", json.Unmarshal(data, &john))
	//
	//fmt.Printf("%#v\n", john)

	//data := []byte(`
	//{
	//	"FirstName": "John",
	//	"Age": 21,
	//	"Username": "johndoe91",
	//	"Grades": null,
	//	"Languages": [
	//	  "English",
	//	  "French"
	//	]
	//}`)
	//
	//isValid := json.Valid(data)
	//fmt.Println(isValid)
}
