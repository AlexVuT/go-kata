package main

import (
	"fmt"
	"strings"
	"unicode/utf8"
)

var data = ` Сообщения с телеграм канала Go-go!
я, извините, решил, что вы хоть что-то про регулярки знаете 🙂
С Почтением, с уважением 🙂.

Сообщения с телеграм канала Go-get job:
#Вакансия #Vacancy #Удаленно #Remote #Golang #Backend  
👨‍💻  Должность: Golang Tech Lead
🏢  Компания: PointPay
💵  Вилка: от 9000 до 15000 $ до вычета налогов
🌏  Локация: Великобритания
💼  Формат работы: удаленный
💼  Занятость: полная занятость 

Наша компания развивает 15 продуктов в сфере блокчейн и криптовалюты, функционирующих в рамках единой экосистемы. Мы - одни из лучших в своей нише, и за без малого три года мы создали криптовалютную платформу, которая предоставляет комплексные решения и позволяет пользователям проводить практически любые финансовые операции.
Наш проект победил в номинации "лучший блокчейн-стартап 2019 года" на одной из самых крупных конференций по блокчейн, криптовалютам и майнингу – Blockchain Life в Москве.

У нас очень амбициозные планы по развитию наших продуктов, и на данный момент мы находимся в поиске Golang Technical Lead, чтобы вместе создать революционный и самый технологичный продукт в мире.


  
Мы ожидаем от тебя:  
  
✅ Опыт управления финансовыми IT продуктами;
✅ Опыт постановки задач технической команде и приема результатов;
✅ Понимание и использование современных подходов к разработке (Agile);
✅ Опыт разработки не менее 5 лет;
✅ Отличное владение Go, будет плюсом опыт работы на PHP;
✅ Опыт работы с REST / GRPС (protobuf);
✅ Опыт работы с Laravel;
✅ Отличные навыки SQL (мы используем PostgreSQL);
✅ Опыт работы с Redis или аналогичной системой кеширования;
✅ Опыт работы с Kubernetes, с брокерами сообщений (RabbitMQ, Kafka и др.);
✅ Базовые знания Amazon Web Services.

Будем плюсом:

✅ Опыт разработки криптовалютных кошельков / криптовалютных бирж / криптовалютных торговых роботов / криптовалютных платежных систем или опыт работы в FinTech или банковских продуктах.

Чем предстоит заниматься:  
  
📌 Управлением и взаимодействием с командой;
📌 Постановкой задач команде разработчиков;
📌 Разработкой решений для запуска и сопровождения разрабатываемого компанией ПО;
📌 Выстраиванием эффективного процесса разработки;
📌 Достигать результатов, поддерживать и развивать текущие проекты, развивать новые проекты и продукты, искать и предлагать нестандартные решения задач.

Мы гарантируем будущему коллеге:  
  
🔥 Высокую заработную плату до 15 000$ (в валюте);
🔥 Индексацию заработной платы;
🔥 Полностью удаленный формат работы и максимально гибкий график - работайте из любой точки мира в удобное вам время;
🔥 Оформление по бессрочному международному трудовому договору с UK;
🔥 Отсутствие бюрократии и кучи бессмысленных звонков;
🔥 Корпоративную технику за счет компании;
🔥 Сотрудничество с командой высококвалифицированных специалистов, настоящих профессионалов своего дела;
🔥 Работу над масштабным, интересным и перспективным проектом.


Если в этом описании вы узнали  себя - пишите. Обсудим проект, задачи и все детали :)  
  
Павел,  
✍️TG: @pavel_hr  
📫 papolushkin.wanted@gmail.com`

var RuEn = map[rune]string{'А': "A", 'Б': "B", 'В': "V", 'Г': "G", 'Д': "D", 'Е': "Ie", 'Ё': "Io", 'Ж': "J", 'З': "Z",
	'И': "I", 'Й': "Ii", 'К': "K", 'Л': "L", 'М': "M", 'Н': "N", 'О': "O", 'П': "P", 'Р': "R", 'С': "S", 'Т': "T", 'У': "U",
	'Ф': "F", 'Х': "H", 'Ц': "Ts", 'Ч': "Ch", 'Ш': "Sh", 'Щ': "Sch", 'Ъ': "", 'Ы': "Y", 'Ь': "", 'Э': "E", 'Ю': "Iu", 'Я': "Ia",
	'а': "a", 'б': "b", 'в': "v", 'г': "g", 'д': "d", 'е': "ie", 'ё': "io", 'ж': "j", 'з': "z",
	'и': "i", 'й': "ii", 'к': "k", 'л': "l", 'м': "m", 'н': "n", 'о': "o", 'п': "p", 'р': "r", 'с': "s", 'т': "t", 'у': "u",
	'ф': "f", 'х': "h", 'ц': "ts", 'ч': "ch", 'ш': "sh", 'щ': "sch", 'ъ': "", 'ы': "y", 'ь': "", 'э': "e", 'ю': "iu", 'я': "ia"}

func main() {
	fmt.Println(len(data))
	quantity := strings.Count(data, "") - 1
	fmt.Println("количество символов в тексте:", quantity)
	data = strings.Replace(data, "🙂", "=)", -1)
	quantity = strings.Count(data, "") - 1
	fmt.Println("количество символов в тексте после замены смайлов:", quantity)
	translitData := string(toTranslit([]byte(data)))
	quantity = utf8.RuneCountInString(translitData)
	fmt.Println("количество символов в тексте после транслита:", quantity)
	LatinByteCount := len(data)
	fmt.Println(LatinByteCount)
	CyrillicByteCount := len(translitData)
	fmt.Println(CyrillicByteCount)
	res := float64(LatinByteCount) / float64(CyrillicByteCount)
	fmt.Println(res)
}

func isRussianRune(r rune) bool {
	if (r >= 'А' && r <= 'Я') || (r >= 'а' && r <= 'я') || r == 'Ё' || r == 'ё' {
		return true
	}
	return false
}

func toTranslit(text []byte) []byte {
	res := make([]byte, 0)
	for len(text) > 0 {
		r, size := utf8.DecodeRune(text)
		if isRussianRune(r) {
			res = append(res, []byte(RuEn[r])...)
		} else {
			res = append(res, text[:size]...)
		}
		text = text[size:]
	}
	return res
}
