package main

import (
	"bufio"
	"bytes"
	"fmt"
	"io"
	"os"
)

func main() {
	data := []string{
		"there is 3pm, but im still alive to write this code snippet",
		"чистый код лучше, чем заумный код",
		"ваш код станет наследием будущих программистов",
		"задумайтесь об этом",
	}
	buf := new(bytes.Buffer)
	for _, p := range data {
		buf.WriteString(p)
		buf.WriteString("\n")
	}
	file, err := os.Create("/home/alex/go/src/AlexVuT/go-kata/module2/stl/io/homework/example.txt")
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	defer file.Close()
	io.Copy(file, buf)

	_, err = file.Seek(0, 0)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	newBuf := new(bytes.Buffer)
	reader := bufio.NewReader(file)

	for {
		line, err := reader.ReadString('\n')
		if err != nil {
			if err == io.EOF {
				break
			} else {
				fmt.Println(err)
				os.Exit(1)
			}
		}
		newBuf.WriteString(line)
	}
	fmt.Println(newBuf.String())

}
