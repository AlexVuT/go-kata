package main

import (
	"os"
	"unicode/utf8"
)

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func main() {
	rus, err := os.Open("/home/alex/go/src/AlexVuT/go-kata/module2/stl/files/write/secondTask/example.txt")
	check(err)
	defer rus.Close()

	dataRus, err := os.ReadFile(rus.Name())
	check(err)

	dataTrans := toTranslit(dataRus)

	err = os.WriteFile("/home/alex/go/src/AlexVuT/go-kata/module2/stl/files/write/secondTask/example.processed.txt", dataTrans, 0)
	check(err)
}

func isRussianRune(r rune) bool {
	if (r >= 'А' && r <= 'Я') || (r >= 'а' && r <= 'я') || r == 'Ё' || r == 'ё' {
		return true
	}
	return false
}

func toTranslit(text []byte) []byte {
	res := make([]byte, 0)
	for len(text) > 0 {
		r, size := utf8.DecodeRune(text)
		if isRussianRune(r) {
			res = append(res, []byte(RuEn[r])...)
		} else {
			res = append(res, text[:size]...)
		}
		text = text[size:]
	}
	return res
}

var RuEn = map[rune]string{'А': "A", 'Б': "B", 'В': "V", 'Г': "G", 'Д': "D", 'Е': "Ie", 'Ё': "Io", 'Ж': "J", 'З': "Z",
	'И': "I", 'Й': "Ii", 'К': "K", 'Л': "L", 'М': "M", 'Н': "N", 'О': "O", 'П': "P", 'Р': "R", 'С': "S", 'Т': "T", 'У': "U",
	'Ф': "F", 'Х': "H", 'Ц': "Ts", 'Ч': "Ch", 'Ш': "Sh", 'Щ': "Sch", 'Ъ': "", 'Ы': "Y", 'Ь': "", 'Э': "E", 'Ю': "Iu", 'Я': "Ia",
	'а': "a", 'б': "b", 'в': "v", 'г': "g", 'д': "d", 'е': "ie", 'ё': "io", 'ж': "j", 'з': "z",
	'и': "i", 'й': "ii", 'к': "k", 'л': "l", 'м': "m", 'н': "n", 'о': "o", 'п': "p", 'р': "r", 'с': "s", 'т': "t", 'у': "u",
	'ф': "f", 'х': "h", 'ц': "ts", 'ч': "ch", 'ш': "sh", 'щ': "sch", 'ъ': "", 'ы': "y", 'ь': "", 'э': "e", 'ю': "iu", 'я': "ia"}
