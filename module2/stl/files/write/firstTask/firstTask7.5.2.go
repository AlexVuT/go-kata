package main

import (
	"bufio"
	"fmt"
	"os"
)

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func main() {

	reader := bufio.NewReader(os.Stdin)

	fmt.Print("Enter your name: ")

	name, _ := reader.ReadString('\n')
	fmt.Printf("Hello %s\n", name)

	f, err := os.Create("/home/alex/go/src/AlexVuT/go-kata/module2/stl/files/write/firstTask/namesDB")
	check(err)
	defer f.Close()
	w := bufio.NewWriter(f)
	w.WriteString(name)
	check(err)
	w.Flush()
	fmt.Printf("your name is written into: %s\n", f.Name())

}
