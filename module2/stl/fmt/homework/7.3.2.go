package main

import "fmt"

type Person struct {
	Name string
	Age  int
}

func main() {
	generateSelfStory("Van Darkholme", 50, 300)
}

func generateSelfStory(name string, age int, money float64) {
	fmt.Printf("Hello! My name is %s. I'm %d y.o. And I also have $%.2f in my wallet right now.\n", name, age, money)
}
