package handler

import (
	"encoding/json"
	"fmt"
	"github.com/go-chi/chi"
	. "gitlab.com/AlexVuT/go-kata/module4/webserver/http/homework/service"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
)

type Handler struct {
	Service Service
}

func (h *Handler) GetUsers(w http.ResponseWriter, r *http.Request) {
	logger := log.New(os.Stdout, "", log.Ldate|log.Ltime)
	users := h.Service.GetUsers()

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(users)
	logger.Println("List of users was requested")
}

func (h *Handler) GetUserByID(w http.ResponseWriter, r *http.Request) {
	logger := log.New(os.Stdout, "", log.Ldate|log.Ltime)
	users := h.Service.GetUsers()
	w.Header().Set("Content-Type", "application/json")
	id := chi.URLParam(r, "id")
	for _, user := range users {
		if fmt.Sprintf("%d", user.ID) == id {
			json.NewEncoder(w).Encode(user)
			logger.Println("User with ID", id, "was requested")
			return
		}
	}
	w.WriteHeader(http.StatusNotFound)
	fmt.Fprintf(w, `{"error": "User not found"}`)
}

func (h *Handler) CreateUser(w http.ResponseWriter, r *http.Request) {
	logger := log.New(os.Stdout, "", log.Ldate|log.Ltime)
	w.Header().Set("Content-Type", "application/json")

	body, err := ioutil.ReadAll(r.Body)
	user, err := h.Service.CreateUser(body)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprintf(w, `{"error": "%s"}`, err.Error())
		return
	}

	w.WriteHeader(http.StatusCreated)
	json.NewEncoder(w).Encode(user)
	logger.Println("New user was created")
}

func (h *Handler) UploadFile(w http.ResponseWriter, r *http.Request) {
	logger := log.New(os.Stdout, "", log.Ldate|log.Ltime)
	r.ParseMultipartForm(10 << 20)
	file, handler, err := r.FormFile("file")
	if err != nil {
		fmt.Fprintf(w, `{"error": "%s"}`, err.Error())
		return
	}
	defer file.Close()
	f, err := os.OpenFile("/home/alex/go/src/AlexVuT/go-kata/module4/webserver/http/homework/public/"+handler.Filename, os.O_WRONLY|os.O_CREATE, 0666)
	if err != nil {
		fmt.Fprintf(w, `{"error": "%s"}`, err.Error())
		return
	}
	defer f.Close()
	io.Copy(f, file)
	fmt.Fprintf(w, `{"message": "File uploaded successfully"}`)
	logger.Println("File", handler.Filename, "was uploaded")
}

func (h *Handler) GetFile(w http.ResponseWriter, r *http.Request) {
	logger := log.New(os.Stdout, "", log.Ldate|log.Ltime)
	filename := chi.URLParam(r, "filename")
	http.ServeFile(w, r, "/home/alex/go/src/AlexVuT/go-kata/module4/webserver/http/homework/public/"+filename)
	logger.Println(filename, "was requested")
}
