package repo

import (
	"encoding/json"
	. "gitlab.com/AlexVuT/go-kata/module4/webserver/http/homework/model"
	"io"
	"os"
)

type UserRepository interface {
	NewUsers(users []User) error
	ReadAll() []User
}

type Repository struct {
	Path string
}

func NewRepository() *Repository {
	return &Repository{Path: "/home/alex/go/src/AlexVuT/go-kata/module4/webserver/http/homework/users.json"}
}

func (r *Repository) NewUsers(users []User) error {
	data, err := json.MarshalIndent(users, "", " ")
	if err != nil {
		return err
	}
	err = os.WriteFile(r.Path, data, 0644)
	if err != nil {
		return err
	}
	return nil
}

func (r *Repository) ReadAll() []User {
	file, _ := os.OpenFile(r.Path, os.O_RDWR|os.O_CREATE, 0644)
	var users []User

	file, err := os.Open(r.Path)
	if err != nil {
		return nil
	}
	defer file.Close()

	content, err := io.ReadAll(file)
	if err != nil {
		return nil
	}

	if err = json.Unmarshal(content, &users); err != nil {
		return nil
	}

	return users
}
