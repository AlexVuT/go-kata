package service

import (
	"encoding/json"
	. "gitlab.com/AlexVuT/go-kata/module4/webserver/http/homework/model"
	. "gitlab.com/AlexVuT/go-kata/module4/webserver/http/homework/repo"
)

type Service struct {
	repo UserRepository
}

func NewService() *Service {
	return &Service{NewRepository()}
}

func (s *Service) GetUsers() []User {
	users := s.repo.ReadAll()
	return users
}

func (s *Service) CreateUser(body []byte) (User, error) {
	var user User
	users := s.repo.ReadAll()
	err := json.Unmarshal(body, &user)
	if err != nil {
		return User{}, err
	}

	user.ID = len(users) + 1
	users = append(users, user)
	err = s.repo.NewUsers(users)
	if err != nil {
		return User{}, err
	}
	return user, nil
}
