package main

import (
	"context"
	"fmt"
	"github.com/go-chi/chi"
	. "gitlab.com/AlexVuT/go-kata/module4/webserver/http/homework/handler"
	. "gitlab.com/AlexVuT/go-kata/module4/webserver/http/homework/service"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"
)

func main() {
	r := chi.NewRouter()
	h := &Handler{*NewService()}
	r.Get("/", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		fmt.Fprintf(w, `{"message": "Welcome!"}`)
	})
	r.Get("/users", h.GetUsers)
	r.Get("/users/{id}", h.GetUserByID)
	r.Post("/users", h.CreateUser)
	r.Post("/upload", h.UploadFile)
	r.Get("/public/{filename}", h.GetFile)

	logger := log.New(os.Stdout, "", log.Ldate|log.Ltime)
	server := &http.Server{
		Addr:    ":8080",
		Handler: r,
	}
	go func() {
		if err := server.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			logger.Fatalf("listen:%s\n", err)
		}
	}()
	logger.Printf("Server started at %s\n", server.Addr)

	stop := make(chan os.Signal, 1)
	signal.Notify(stop, os.Interrupt, syscall.SIGTERM)

	<-stop
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	logger.Println("Shutting down server...")
	if err := server.Shutdown(ctx); err != nil {
		logger.Fatalf("Server shutdown failed:%+s", err)
	}
	logger.Println("Server stopped")
}
