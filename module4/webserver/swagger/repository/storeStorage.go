package repository

import (
	"fmt"
	"gitlab.com/AlexVuT/go-kata/module4/webserver/swagger/model"
	"sync"
)

type StoreStorager interface {
	Create(order model.Store) model.Store
	FindByID(orderID int) (model.Store, error)
	Delete(orderID int) error
	GetInventory() map[string]int
}

type StoreStorage struct {
	Data               []*model.Store
	PrimaryKeyIDx      map[int]*model.Store
	AutoIncrementCount int
	sync.Mutex
}

func NewStoreStorage() *StoreStorage {
	return &StoreStorage{
		Data:               make([]*model.Store, 0, 13),
		PrimaryKeyIDx:      make(map[int]*model.Store, 13),
		AutoIncrementCount: 1,
	}
}

func (s *StoreStorage) Create(order model.Store) model.Store {
	s.Lock()
	defer s.Unlock()
	order.ID = s.AutoIncrementCount
	s.PrimaryKeyIDx[order.ID] = &order
	s.AutoIncrementCount++
	s.Data = append(s.Data, &order)
	return order
}

func (s *StoreStorage) FindByID(orderID int) (model.Store, error) {
	if v, ok := s.PrimaryKeyIDx[orderID]; ok && v != nil {
		return *v, nil
	}

	return model.Store{}, fmt.Errorf("not found")

}

func (s *StoreStorage) Delete(orderID int) error {
	s.Lock()
	defer s.Unlock()
	var data []*model.Store
	for _, order := range s.Data {
		if order.ID != orderID {
			data = append(data, order)
		}
	}
	s.PrimaryKeyIDx[orderID] = nil
	if len(data) == len(s.Data) {
		return fmt.Errorf("not found")
	}
	s.Data = data
	return nil

}

func (s *StoreStorage) GetInventory() map[string]int {
	s.Lock()
	defer s.Unlock()
	info := make(map[string]int, len(s.PrimaryKeyIDx))
	for _, order := range s.Data {
		info[order.Status] += order.Quantity
	}
	return info
}
