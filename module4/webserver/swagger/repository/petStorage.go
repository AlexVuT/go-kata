package repository

import (
	"fmt"
	"gitlab.com/AlexVuT/go-kata/module4/webserver/swagger/model"
	"sync"
)

type PetStorager interface {
	Create(pet model.Pet) model.Pet
	Update(pet model.Pet) (model.Pet, error)
	Delete(petID int) error
	GetByID(petID int) (model.Pet, error)
	GetList(status string) ([]model.Pet, error)
}

type PetStorage struct {
	data               []*model.Pet
	primaryKeyIDx      map[int]*model.Pet
	autoIncrementCount int
	sync.Mutex
}

func NewPetStorage() *PetStorage {
	return &PetStorage{
		data:               make([]*model.Pet, 0, 13),
		primaryKeyIDx:      make(map[int]*model.Pet, 13),
		autoIncrementCount: 1,
	}
}

func (p *PetStorage) Delete(petID int) error {
	var data []*model.Pet
	for _, pet := range p.data {
		if pet.ID != petID {
			data = append(data, pet)
		}
	}
	p.primaryKeyIDx[petID] = nil
	if len(data) == len(p.data) {
		return fmt.Errorf("not found")
	}
	p.data = data
	return nil

}

func (p *PetStorage) Update(pet model.Pet) (model.Pet, error) {
	p.Lock()
	defer p.Unlock()
	curr, ok := p.primaryKeyIDx[pet.ID]
	if !ok {
		return model.Pet{}, fmt.Errorf("not found")
	}
	if pet.Name != "" {
		curr.Name = pet.Name
	}
	if pet.Category.Name != "" {
		curr.Category = pet.Category
	}
	if pet.Status != "" {
		curr.Status = pet.Status
	}
	if len(pet.PhotoUrls) != 0 {
		curr.PhotoUrls = pet.PhotoUrls
	}
	if pet.Tags != nil {
		curr.Tags = pet.Tags
	}

	for _, v := range p.data {
		if v.ID == pet.ID {
			v = curr
			break
		}
	}
	return *curr, nil
}

func (p *PetStorage) Create(pet model.Pet) model.Pet {
	p.Lock()
	defer p.Unlock()
	pet.ID = p.autoIncrementCount
	p.primaryKeyIDx[pet.ID] = &pet
	p.autoIncrementCount++
	p.data = append(p.data, &pet)

	return pet
}

func (p *PetStorage) GetByID(petID int) (model.Pet, error) {
	if v, ok := p.primaryKeyIDx[petID]; ok && v != nil {
		return *v, nil
	}

	return model.Pet{}, fmt.Errorf("not found")
}

func (p *PetStorage) GetList(status string) ([]model.Pet, error) {
	var data []model.Pet
	for _, v := range p.data {
		if v.Status == status {
			data = append(data, *v)
		}
	}
	if len(data) == 0 {
		return data, fmt.Errorf("not found")
	} else {
		return data, nil
	}
}
