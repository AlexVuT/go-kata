package repository

import (
	"fmt"
	"gitlab.com/AlexVuT/go-kata/module4/webserver/swagger/model"
	"sync"
)

type UserStorager interface {
	Create(user model.User) model.User
	FindByUsername(username string) (model.User, error)
	Update(username string, user model.User) (model.User, error)
	Delete(username string) error
	CreateArrayOfUsers(array []model.User) []model.User
}

type UserStorage struct {
	Data               []*model.User
	PrimaryKeyUsername map[string]*model.User
	AutoIncrementCount int
	sync.Mutex
}

func NewUserStorage() *UserStorage {
	return &UserStorage{
		Data:               make([]*model.User, 0, 13),
		PrimaryKeyUsername: make(map[string]*model.User, 13),
		AutoIncrementCount: 1,
	}
}

func (u *UserStorage) Create(user model.User) model.User {
	u.Lock()
	defer u.Unlock()
	user.ID = u.AutoIncrementCount
	u.PrimaryKeyUsername[user.Username] = &user
	u.AutoIncrementCount++
	u.Data = append(u.Data, &user)
	return user
}

func (u *UserStorage) FindByUsername(username string) (model.User, error) {
	if v, ok := u.PrimaryKeyUsername[username]; ok && v != nil {
		return *v, nil
	}

	return model.User{}, fmt.Errorf("not found")
}

func (u *UserStorage) Update(username string, user model.User) (model.User, error) {
	u.Lock()
	defer u.Unlock()
	curr, ok := u.PrimaryKeyUsername[username]
	if !ok {
		return model.User{}, fmt.Errorf("not found")
	}

	if user.Username != "" {
		curr.Username = user.Username
	}
	if user.FirstName != "" {
		curr.FirstName = user.FirstName
	}
	if user.LastName != "" {
		curr.LastName = user.LastName
	}
	if user.Email != "" {
		curr.Email = user.Email
	}
	if user.Password != "" {
		curr.Password = user.Password
	}
	if user.Phone != "" {
		curr.Phone = user.Phone
	}
	if user.UserStatus != 0 {
		curr.UserStatus = user.UserStatus
	}

	if username == curr.Username {
		u.PrimaryKeyUsername[curr.Username] = curr
	} else {
		u.PrimaryKeyUsername[curr.Username] = curr
		u.PrimaryKeyUsername[username] = nil
	}
	for _, v := range u.Data {
		if v.Username == user.Username {
			v = curr
			break
		}
	}
	return *curr, nil
}

func (u *UserStorage) Delete(username string) error {
	var data []*model.User
	if u.PrimaryKeyUsername[username] == nil {
		return fmt.Errorf("not found")
	}
	u.PrimaryKeyUsername[username] = nil
	for _, v := range u.Data {
		if v.Username != username {
			data = append(data, v)
		}
	}
	u.Data = data
	return nil
}

func (u *UserStorage) CreateArrayOfUsers(array []model.User) []model.User {
	var data []model.User
	for _, v := range array {
		data = append(data, u.Create(v))
	}
	return data
}
