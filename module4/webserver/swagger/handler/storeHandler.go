package handler

import (
	"encoding/json"
	"github.com/go-chi/chi"
	. "gitlab.com/AlexVuT/go-kata/module4/webserver/swagger/model"
	. "gitlab.com/AlexVuT/go-kata/module4/webserver/swagger/repository"
	"net/http"
	"strconv"
)

type StoreController struct {
	storage StoreStorager
}

func NewStoreController() *StoreController {
	return &StoreController{storage: NewStoreStorage()}
}

func (s *StoreController) StoreOrderCreate(w http.ResponseWriter, r *http.Request) {
	var store Store
	err := json.NewDecoder(r.Body).Decode(&store)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	store = s.storage.Create(store)
	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	err = json.NewEncoder(w).Encode(store)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (s *StoreController) StoreFindOrderByID(w http.ResponseWriter, r *http.Request) {
	var (
		order      Store
		err        error
		orderIDRaw string
		orderID    int
	)
	orderIDRaw = chi.URLParam(r, "orderID")

	orderID, err = strconv.Atoi(orderIDRaw)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	order, err = s.storage.FindByID(orderID)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	err = json.NewEncoder(w).Encode(order)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (s *StoreController) StoreDeleteByID(w http.ResponseWriter, r *http.Request) {
	var (
		orderIDRaw string
		orderID    int
	)

	orderIDRaw = chi.URLParam(r, "orderID")

	orderID, err := strconv.Atoi(orderIDRaw)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	err = s.storage.Delete(orderID)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}
}

func (s *StoreController) StoreGetInventory(w http.ResponseWriter, r *http.Request) {
	info := s.storage.GetInventory()
	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	err := json.NewEncoder(w).Encode(info)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}
