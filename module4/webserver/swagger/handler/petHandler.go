package handler

import (
	"encoding/json"
	"fmt"
	"github.com/go-chi/chi"
	. "gitlab.com/AlexVuT/go-kata/module4/webserver/swagger/model"
	. "gitlab.com/AlexVuT/go-kata/module4/webserver/swagger/repository"
	"io"
	"net/http"
	"os"
	"strconv"
)

type PetController struct { // Pet контроллер
	storage PetStorager
}

func NewPetController() *PetController { // конструктор нашего контроллера
	return &PetController{storage: NewPetStorage()}
}

func (p *PetController) PetUploadImage(w http.ResponseWriter, r *http.Request) {
	var (
		pet      Pet
		petIDRaw string
		petID    int
	)
	petIDRaw = chi.URLParam(r, "petID")
	fmt.Println(petIDRaw)
	petID, err := strconv.Atoi(petIDRaw)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	file, handler, err := r.FormFile("file")
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	defer file.Close()
	f, err := os.OpenFile("/home/alex/go/src/AlexVuT/go-kata/module4/webserver/swagger/public/"+handler.Filename, os.O_WRONLY|os.O_CREATE, 0666)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	defer f.Close()
	io.Copy(f, file)
	pet.ID = petID
	pet.PhotoUrls = append(pet.PhotoUrls, "/home/alex/go/src/AlexVuT/go-kata/module4/webserver/swagger/public/"+handler.Filename)
	p.storage.Update(pet)
}

func (p *PetController) PetDelete(w http.ResponseWriter, r *http.Request) {
	var (
		petIDRaw string
		petID    int
	)

	petIDRaw = chi.URLParam(r, "petID") // получаем petID из chi router

	petID, err := strconv.Atoi(petIDRaw) // конвертируем в int
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	err = p.storage.Delete(petID)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}
}

func (p *PetController) PetCreate(w http.ResponseWriter, r *http.Request) {
	var pet Pet
	err := json.NewDecoder(r.Body).Decode(&pet) // считываем приходящий json из *http.Request в структуру Pet

	if err != nil { // в случае ошибки отправляем ошибку Bad request code 400
		http.Error(w, err.Error(), http.StatusBadRequest)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

	pet = p.storage.Create(pet) // создаем запись в нашем storage

	w.Header().Set("Content-Type", "application/json;charset=utf-8") // выставляем заголовки, что отправляем json в utf8
	err = json.NewEncoder(w).Encode(pet)                             // записываем результат Pet json в http.ResponseWriter

	if err != nil { // отправляем 500 ошибку в случае неудачи
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return // не забываем прекратить обработку нашего handler (ручки)
	}
}

func (p *PetController) PetUpdateByID(w http.ResponseWriter, r *http.Request) {
	var (
		pet                    Pet
		petIDRaw, name, status string
		petID                  int
	)
	petIDRaw = chi.URLParam(r, "petID")

	petID, err := strconv.Atoi(petIDRaw) // конвертируем в int
	if err != nil {                      // в случае ошибки отправляем код 400
		http.Error(w, err.Error(), http.StatusBadRequest)
		return // не забываем прекратить обработку нашего handler (ручки)
	}
	name = r.FormValue("name")
	status = r.FormValue("status")

	pet.ID = petID
	pet.Name = name
	pet.Status = status

	_, err = p.storage.Update(pet)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8") // выставляем заголовки, что отправляем json в utf8
	err = json.NewEncoder(w).Encode(pet)
	if err != nil { // отправляем 500 ошибку в случае неудачи
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return // не забываем прекратить обработку нашего handler (ручки)
	}
}

func (p *PetController) PetUpdate(w http.ResponseWriter, r *http.Request) {
	var pet Pet

	err := json.NewDecoder(r.Body).Decode(&pet)

	if err != nil { // в случае ошибки отправляем ошибку Bad request code 400
		http.Error(w, err.Error(), http.StatusBadRequest)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

	pet, _ = p.storage.Update(pet)
	w.Header().Set("Content-Type", "application/json;charset=utf-8") // выставляем заголовки, что отправляем json в utf8
	err = json.NewEncoder(w).Encode(pet)
	if err != nil { // отправляем 500 ошибку в случае неудачи
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return // не забываем прекратить обработку нашего handler (ручки)
	}
}

func (p *PetController) PetsGetByStatus(w http.ResponseWriter, r *http.Request) {
	status := r.URL.Query()["status"]
	list, _ := p.storage.GetList(status[0])

	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	err := json.NewEncoder(w).Encode(list)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

}

func (p *PetController) PetGetByID(w http.ResponseWriter, r *http.Request) {
	var ( // заранее аллоцируем все необходимые переменные во избежание shadowing
		pet      Pet
		err      error
		petIDRaw string
		petID    int
	)

	petIDRaw = chi.URLParam(r, "petID") // получаем petID из chi router

	petID, err = strconv.Atoi(petIDRaw) // конвертируем в int
	if err != nil {                     // в случае ошибки отправляем код 400
		http.Error(w, err.Error(), http.StatusBadRequest)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

	pet, err = p.storage.GetByID(petID) // пытаемся получить Pet по id
	if err != nil {                     // в случае ошибки отправляем Not Found код 404
		http.Error(w, err.Error(), http.StatusNotFound)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	err = json.NewEncoder(w).Encode(pet)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return // не забываем прекратить обработку нашего handler (ручки)
	}
}
