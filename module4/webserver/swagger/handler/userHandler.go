package handler

import (
	"encoding/json"
	"github.com/go-chi/chi"
	. "gitlab.com/AlexVuT/go-kata/module4/webserver/swagger/model"
	. "gitlab.com/AlexVuT/go-kata/module4/webserver/swagger/repository"
	"net/http"
)

type UserController struct {
	storage UserStorager
}

func NewUserController() *UserController {
	return &UserController{storage: NewUserStorage()}
}

func (u *UserController) UserCreate(w http.ResponseWriter, r *http.Request) {
	var user User
	err := json.NewDecoder(r.Body).Decode(&user)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	user = u.storage.Create(user)
	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	err = json.NewEncoder(w).Encode(user)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (u *UserController) UserFindByUsername(w http.ResponseWriter, r *http.Request) {
	var (
		user     User
		err      error
		username string
	)
	username = chi.URLParam(r, "username")

	user, err = u.storage.FindByUsername(username)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	err = json.NewEncoder(w).Encode(user)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (u *UserController) UserUpdate(w http.ResponseWriter, r *http.Request) {
	var (
		user     User
		username string
		err      error
	)
	username = chi.URLParam(r, "username")

	err = json.NewDecoder(r.Body).Decode(&user)

	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	user, _ = u.storage.Update(username, user)
	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	err = json.NewEncoder(w).Encode(user)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return // не забываем прекратить обработку нашего handler (ручки)
	}
}

func (u *UserController) UserDeleteByUsername(w http.ResponseWriter, r *http.Request) {
	username := chi.URLParam(r, "username")
	err := u.storage.Delete(username)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
	}

}

func (u *UserController) UserCreateArrayOfUsers(w http.ResponseWriter, r *http.Request) {
	var (
		users []User
		err   error
	)
	err = json.NewDecoder(r.Body).Decode(&users)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
	}
	users = u.storage.CreateArrayOfUsers(users)
	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	err = json.NewEncoder(w).Encode(users)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}
