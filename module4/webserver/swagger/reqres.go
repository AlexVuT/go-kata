package main

import (
	. "gitlab.com/AlexVuT/go-kata/module4/webserver/swagger/model"
)

//go:generate swagger generate spec -o /home/alex/go/src/AlexVuT/go-kata/module4/webserver/swagger/public/swagger.json --scan-models

// swagger:route POST /pet pet petAddRequest
// Добавление питомца.
// responses:
//   200: petAddResponse

// swagger:parameters petAddRequest
type petAddRequest struct {
	// in:body
	Body Pet
}

// swagger:response petAddResponse
type petAddResponse struct {
	// in:body
	Body Pet
}

// swagger:route GET /pet/{id} pet petGetByIDRequest
// Получение питомца по id.
// responses:
//   200: petGetByIDResponse

// swagger:parameters petGetByIDRequest
type petGetByIDRequest struct {
	// ID of an item
	//
	// In: path
	ID string `json:"id"`
}

// swagger:response petGetByIDResponse
type petGetByIDResponse struct {
	// in:body
	Body Pet
}

// swagger:route GET /pet/findByStatus pet petGetByStatusRequest
// Find pets by status
// responses:
//   200: petGetByStatusResponse
//   400: description: Invalid status value

// swagger:parameters petGetByStatusRequest
type petGetByStatusRequest struct {
	// Status values that need to be considered for filter
	// in: query
	// explode: true
	// schema:
	// 	default: available
	// 	type: string
	// 	enum: available,pending,sold
	// 	format: multi
	Status string `json:"status"`
}

// swagger:response petGetByStatusResponse
type petGetByStatusResponse struct {
	// in:body
	Body []Pet
}

// swagger:route PUT /pet pet petUpdateRequest
// Редактирование питомца.
// responses:
//   200: petUpdateResponse

// swagger:parameters petUpdateRequest
type petUpdateRequest struct {
	// in:body
	Body Pet
}

// swagger:response petUpdateResponse
type petUpdateResponse struct {
	// in:body
	Body Pet
}

// swagger:route DELETE /pet/{id} pet petDeleteRequest
// Удаление питомца.
// responses:
//  200: petDeleteResponse
// 400: description: Invalid ID supplied
// 404: description: Pet not found

// swagger:parameters petDeleteRequest
type petDeleteRequest struct {
	// ID of an item
	//
	// In: path
	ID string `json:"id"`
}

// swagger:response petDeleteResponse
type petDeleteResponse struct {
}

// swagger:route POST /pet/{petId} pet PetUpdateByIDRequest
// Updates a pet in the store with form data.
// consumes:
// - application/x-www-form-urlencoded
// responses:
//
//	405: UpdatePetPutInvalidIdSuppliedResponse
//
// swagger:parameters PetUpdateByIDRequest
type PetUpdateByIDRequest struct {
	//ID of pet that needs to be updated
	//required:true
	// in:path
	PetId string `json:"petId"`
	//Updated name of the pet
	//required:false
	//in:formData
	Name string `json:"name"`
	//Updated status of the pet
	//in:formData
	Status string `json:"status"`
}

// swagger:response PetUpdateByIDResponse
type PetUpdateByIDResponse struct {
	// in:body
	Body Pet
}

// swagger:route POST /pet/{petId}/uploadImage pet PetUploadImageRequest
// Upload image of a pet
// responses:
//
//	405: UpdatePetPutInvalidIdSuppliedResponse
//
// swagger:parameters PetUploadImageRequest
type PetUploadImageRequest struct {
	//ID of pet
	//required:true
	// in:path
	PetId string `json:"petId"`
	//Additional data
	//in:formData
	Metadata string `json:"data"`
	//File to upload
	//in:formData
	//swagger:file
	File string `json:"file"`
}

// swagger:response PetUploadImageResponse
type PetUploadImageResponse struct {
	//in:body
	Body Pet
}

// swagger:route POST /store/order store storeOrderCreateRequest
// Order create.
// responses:
//   200: storeOrderCreateResponse

// swagger:parameters storeOrderCreateRequest
type storeOrderCreateRequest struct {
	// in:body
	Body Store
}

// swagger:response storeOrderCreateResponse
type storeOrderCreateResponse struct {
	// in:body
	Body Store
}

// swagger:route GET /store/order/{orderID} store storeFindOrderByIDRequest
// Find order by ID.
// responses:
//   200: storeFindOrderByIDResponse
// 404: description: order not found

// swagger:parameters storeFindOrderByIDRequest
type storeFindOrderByIDRequest struct {
	// ID of an order
	//
	// In: path
	OrderID string `json:"orderID"`
}

// swagger:response storeFindOrderByIDResponse
type storeFindOrderByIDResponse struct {
	// in:body
	Body Store
}

// swagger:route DELETE /store/order/{orderID} store storeDeleteByIDRequest
// Deleting order.
// responses:
//
//	200: storeDeleteByIDResponse
//
// 400: description: Invalid ID supplied
// 404: description: order not found
// swagger:parameters storeDeleteByIDRequest
type storeDeleteByIDRequest struct {
	// ID of an order
	//
	// In: path
	ID string `json:"orderID"`
}

// swagger:response storeDeleteByIDResponse
type storeDeleteByIDResponse struct {
}

// swagger:route GET /store/inventory store storeGetInventoryRequest
// Find quantity of pets by status of order.
// responses:
//   200: storeGetInventoryResponse
// 500: StatusInternalServerError

// swagger:parameters storeGetInventoryRequest
type storeGetInventoryRequest struct {
}

// swagger:response storeGetInventoryResponse
type storeGetInventoryResponse struct {
	// in:body
	Body map[string]int
}

// swagger:route POST /user user userCreateRequest
// User create.
// responses:
//   200: userCreateResponse

// swagger:parameters userCreateRequest
type userCreateRequest struct {
	// in:body
	Body User
}

// swagger:response userCreateResponse
type userCreateResponse struct {
	// in:body
	Body User
}

// swagger:route GET /user/{username} user userFindByUsernameRequest
// Find user by username.
// responses:
//   200: userFindByUsernameResponse
// 404: description: user not found

// swagger:parameters userFindByUsernameRequest
type userFindByUsernameRequest struct {
	// Username of a user
	//
	// In: path
	Username string `json:"username"`
}

// swagger:response userFindByUsernameResponse
type userFindByUsernameResponse struct {
	// in:body
	Body User
}

// swagger:route PUT /user/{username} user userUpdateByUsernameRequest
// Edit user.
// responses:
//   200: userUpdateByUsernameResponse

// swagger:parameters userUpdateByUsernameRequest
type userUpdateByUsernameRequest struct {
	//required:true
	// in:path
	Username string `json:"username"`
	// in:body
	Body User
}

// swagger:response userUpdateByUsernameResponse
type userUpdateByUsernameResponse struct {
	// in:body
	Body User
}

// swagger:route DELETE /user/{username} user userDeleteByUsernameRequest
// Deleting user.
// responses:
//
//	200: userDeleteByUsernameResponse
//
// 400: description: Invalid username supplied
// 404: description: user not found
// swagger:parameters userDeleteByUsernameRequest
type userDeleteByUsernameRequest struct {
	// Username of a user
	//
	// In: path
	Username string `json:"username"`
}

// swagger:response userDeleteByUsernameResponse
type userDeleteByUsernameResponse struct {
}

// swagger:route POST /user/createWithArray user userCreateArrayOfUsersRequest
// Create multiple users with array.
// responses:
//   200: userCreateArrayOfUsersResponse

// swagger:parameters userCreateArrayOfUsersRequest
type userCreateArrayOfUsersRequest struct {
	// in:body
	Body []User
}

// swagger:response userCreateArrayOfUsersResponse
type userCreateArrayOfUsersResponse struct {
	// in:body
	Body []User
}
