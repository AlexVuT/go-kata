// Package classification Four Paws.
//
// Documentation of your project API.
//
//	Schemes:
//	- http
//	- https
//	BasePath: /
//	Version: 1.0.0
//
//	Consumes:
//	- application/json
//	- multipart/form-data
//
//	Produces:
//	- application/json
//
//	Security:
//	- basic
//
//
//	SecurityDefinitions:
//	  Bearer:
//	    type: apiKey
//	    name: Authorization
//	    in: header
//
// swagger:meta
package main

//go:generate swagger generate spec -o /home/alex/go/src/AlexVuT/go-kata/module4/webserver/swagger/public/swagger.json --scan-models
