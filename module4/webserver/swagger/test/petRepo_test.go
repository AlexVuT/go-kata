package test

import (
	"gitlab.com/AlexVuT/go-kata/module4/webserver/swagger/model"
	"gitlab.com/AlexVuT/go-kata/module4/webserver/swagger/repository"
	"reflect"
	"sync"
	"testing"
)

func TestPetStorage_Create(t *testing.T) {
	type fields struct {
		data               []*model.Pet
		primaryKeyIDx      map[int64]*model.Pet
		autoIncrementCount int64
		Mutex              sync.Mutex
	}
	type args struct {
		pet model.Pet
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   model.Pet
	}{
		{
			name: "first test",
			args: args{
				pet: model.Pet{
					ID: 0,
					Category: model.Category{
						ID:   0,
						Name: "test category",
					},
					Name: "Alma",
					PhotoUrls: []string{
						"https://sobakovod.club/uploads/posts/2021-12/1639834742_26-sobakovod-club-p-sobaki-laika-chernaya-29.jpg",
						"http://catfishes.ru/wp-content/uploads/2021/06/ruslaika1.jpg",
						"https://lapkins.ru/upload/iblock/ebd/ebd19f44cd131f425feed81f9b578198.jpg",
					},
					Tags:   []model.Category{},
					Status: "active",
				},
			},
			want: model.Pet{
				ID: 0,
				Category: model.Category{
					ID:   0,
					Name: "test category",
				},
				Name: "Alma",
				PhotoUrls: []string{
					"https://sobakovod.club/uploads/posts/2021-12/1639834742_26-sobakovod-club-p-sobaki-laika-chernaya-29.jpg",
					"http://catfishes.ru/wp-content/uploads/2021/06/ruslaika1.jpg",
					"https://lapkins.ru/upload/iblock/ebd/ebd19f44cd131f425feed81f9b578198.jpg",
				},
				Tags:   []model.Category{},
				Status: "active",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := repository.NewPetStorage()
			var got model.Pet
			var err error
			got = p.Create(tt.args.pet)
			tt.want.ID = got.ID // fix autoincrement value
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Create() = %v, want %v", got, tt.want)
			}
			if got, err = p.GetByID(got.ID); err != nil || !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Create() = %v, want %v, err %s", got, tt.want, err)
			}
		})
	}
}

func TestPetStorage_Update(t *testing.T) {
	type fields struct {
		data               []*model.Pet
		primaryKeyIDx      map[int64]*model.Pet
		autoIncrementCount int64
		Mutex              sync.Mutex
	}
	type args struct {
		pet model.Pet
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   model.Pet
	}{
		{
			name: "first test",
			args: args{
				pet: model.Pet{
					ID: 0,
					Category: model.Category{
						ID:   0,
						Name: "test category",
					},
					Name: "Alma",
					PhotoUrls: []string{
						"https://sobakovod.club/uploads/posts/2021-12/1639834742_26-sobakovod-club-p-sobaki-laika-chernaya-29.jpg",
						"http://catfishes.ru/wp-content/uploads/2021/06/ruslaika1.jpg",
						"https://lapkins.ru/upload/iblock/ebd/ebd19f44cd131f425feed81f9b578198.jpg",
					},
					Tags:   []model.Category{},
					Status: "active",
				},
			},
			want: model.Pet{
				ID: 0,
				Category: model.Category{
					ID:   0,
					Name: "test category",
				},
				Name: "Rose",
				PhotoUrls: []string{
					"https://sobakovod.club/uploads/posts/2021-12/1639834742_26-sobakovod-club-p-sobaki-laika-chernaya-29.jpg",
				},
				Tags:   []model.Category{},
				Status: "active",
			},
		},
	}
	newInfo := model.Pet{
		ID: 0,
		Category: model.Category{
			ID:   0,
			Name: "test category",
		},
		Name: "Rose",
		PhotoUrls: []string{
			"https://sobakovod.club/uploads/posts/2021-12/1639834742_26-sobakovod-club-p-sobaki-laika-chernaya-29.jpg",
		},
		Tags:   []model.Category{},
		Status: "active",
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := repository.NewPetStorage()
			var got model.Pet
			var err error
			got = p.Create(tt.args.pet)
			tt.want.ID = got.ID
			newInfo.ID = got.ID
			got, err = p.Update(newInfo)
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Create() = %v, want %v", got, tt.want)
			}
			if got, err = p.GetByID(got.ID); err != nil || !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Create() = %v, want %v, err %s", got, tt.want, err)
			}
		})
	}
}

func TestPetStorage_Delete(t *testing.T) {
	petStorage := repository.NewPetStorage()
	pet := model.Pet{
		Name: "Cat",
		Category: model.Category{
			Name: "Animal",
		},
		PhotoUrls: []string{"http://example.com/cat.jpg"},
		Tags: []model.Category{
			{ID: 1, Name: "cute"},
			{ID: 2, Name: "fluffy"},
		},
		Status: "available",
	}
	createdPet := petStorage.Create(pet)
	err := petStorage.Delete(createdPet.ID)
	if err != nil {
		t.Errorf("Expected no error, but got %v", err)
	}
	_, err = petStorage.GetByID(createdPet.ID)
	if err == nil {
		t.Errorf("Expected error, but got no error")
	}

	// Test deleting a non-existent pet
	err = petStorage.Delete(100)
	if err == nil {
		t.Errorf("Expected error, but got no error")
	}
}

func TestPetStorage_GetByID(t *testing.T) {
	petStorage := repository.NewPetStorage()
	pet := model.Pet{
		Name: "Cat",
		Category: model.Category{
			Name: "Animal",
		},
		PhotoUrls: []string{"http://example.com/cat.jpg"},
		Tags: []model.Category{
			{ID: 1, Name: "cute"},
			{ID: 2, Name: "fluffy"},
		},
		Status: "available",
	}
	createdPet := petStorage.Create(pet)
	retrievedPet, err := petStorage.GetByID(createdPet.ID)
	if err != nil {
		t.Errorf("Expected no error, but got %v", err)
	}

	if retrievedPet.Category != createdPet.Category {
		t.Errorf("Expected %v, but got %v", createdPet.Category, retrievedPet.Category)
	}
	if retrievedPet.Name != createdPet.Name {
		t.Errorf("Expected %v, but got %v", createdPet.Name, retrievedPet.Name)
	}
	if retrievedPet.Status != createdPet.Status {
		t.Errorf("Expected %v, but got %v", createdPet.Status, retrievedPet.Status)
	}
	if len(retrievedPet.PhotoUrls) != len(createdPet.PhotoUrls) {
		t.Errorf("Expected %v, but got %v", len(createdPet.PhotoUrls), len(retrievedPet.PhotoUrls))
	}
	for i := 0; i < len(retrievedPet.PhotoUrls); i++ {
		if retrievedPet.PhotoUrls[i] != createdPet.PhotoUrls[i] {
			t.Errorf("Expected %v, but got %v", createdPet.PhotoUrls[i], retrievedPet.PhotoUrls[i])
		}
	}
	if len(retrievedPet.Tags) != len(createdPet.Tags) {
		t.Errorf("Expected %v, but got %v", len(createdPet.Tags), len(retrievedPet.Tags))
	}
	for i := 0; i < len(retrievedPet.Tags); i++ {
		if retrievedPet.Tags[i] != createdPet.Tags[i] {
			t.Errorf("Expected %v, but got %v", createdPet.Tags[i], retrievedPet.Tags[i])
		}
	}

	_, err = petStorage.GetByID(100)
	if err == nil {
		t.Errorf("Expected error, but got no error")
	}
}

func TestPetStorage_GetList(t *testing.T) {
	petStorage := repository.NewPetStorage()
	pet1 := model.Pet{
		Name: "Cat",
		Category: model.Category{
			Name: "Animal",
		},
		PhotoUrls: []string{"http://example.com/cat.jpg"},
		Tags: []model.Category{
			{ID: 1, Name: "cute"},
			{ID: 2, Name: "fluffy"},
		},
		Status: "available",
	}
	pet2 := model.Pet{
		Name: "Dog",
		Category: model.Category{
			Name: "Animal",
		},
		PhotoUrls: []string{"http://example.com/dog.jpg"},
		Tags: []model.Category{
			{ID: 3, Name: "loyal"},
			{ID: 4, Name: "friendly"},
		},
		Status: "pending",
	}
	petStorage.Create(pet1)
	petStorage.Create(pet2)

	retrievedPets, err := petStorage.GetList("available")
	if err != nil {
		t.Errorf("Expected no error, but got %v", err)
	}

	if len(retrievedPets) != 1 || retrievedPets[0].Name != pet1.Name || retrievedPets[0].Status != pet1.Status ||
		retrievedPets[0].Category != pet1.Category || len(retrievedPets[0].Tags) != len(pet1.Tags) || len(retrievedPets[0].PhotoUrls) != len(pet1.PhotoUrls) {
		t.Errorf("Expected %v, but got %v", []model.Pet{pet1}, retrievedPets[0])
	}
	for i := 0; i < len(retrievedPets[0].PhotoUrls); i++ {
		if retrievedPets[0].PhotoUrls[i] != pet1.PhotoUrls[i] {
			t.Errorf("Expected %v, but got %v", pet1.PhotoUrls[i], retrievedPets[0].PhotoUrls[i])
		}
	}
	for i := 0; i < len(retrievedPets[0].Tags); i++ {
		if retrievedPets[0].Tags[i] != pet1.Tags[i] {
			t.Errorf("Expected %v, but got %v", pet1.Tags[i], retrievedPets[0].Tags[i])
		}
	}

	_, err = petStorage.GetList("sold")
	if err == nil {
		t.Errorf("Expected error, but got no error")
	}
}
