package test

import (
	"gitlab.com/AlexVuT/go-kata/module4/webserver/swagger/model"
	"gitlab.com/AlexVuT/go-kata/module4/webserver/swagger/repository"
	"testing"
)

func TestUserStorage_Create(t *testing.T) {
	userStorage := repository.NewUserStorage()
	user := model.User{
		Username:   "user",
		FirstName:  "Sandy",
		LastName:   "White",
		Email:      "user@example.com",
		Password:   "password",
		Phone:      "+1234567890",
		UserStatus: 1,
	}
	createdUser := userStorage.Create(user)
	user.ID = createdUser.ID
	if createdUser.ID != 1 {
		t.Errorf("Expected user ID to be 1 but got %d", createdUser.ID)
	}
	if createdUser != user {
		t.Errorf("Expected %v but got %v", user, createdUser)
	}
	if *userStorage.Data[0] != user {
		t.Errorf("Expected %v but got %v", user, *userStorage.Data[0])
	}
	if *userStorage.PrimaryKeyUsername[user.Username] != user {
		t.Errorf("Expected %v but got %v", user, *userStorage.PrimaryKeyUsername[user.Username])
	}
}

func TestUserStorage_FindByUsername(t *testing.T) {
	userStorage := repository.NewUserStorage()
	user := model.User{
		Username:   "user",
		FirstName:  "Sandy",
		LastName:   "White",
		Email:      "user@example.com",
		Password:   "password",
		Phone:      "+1234567890",
		UserStatus: 1,
	}
	newUser := userStorage.Create(user)
	user.ID = newUser.ID
	foundUser, err := userStorage.FindByUsername("user")
	if err != nil {
		t.Errorf("Expected no error but got %v", err)
	}
	if foundUser.ID != 1 {
		t.Errorf("Expected user ID to be 1 but got %d", foundUser.ID)
	}
	if foundUser != user {
		t.Errorf("Expected %v but got %v", user, foundUser)
	}
}

func TestUserStorage_Delete(t *testing.T) {
	userStorage := repository.NewUserStorage()
	user := model.User{
		Username:   "user",
		FirstName:  "Sandy",
		LastName:   "White",
		Email:      "user@example.com",
		Password:   "password",
		Phone:      "+1234567890",
		UserStatus: 1,
	}
	createdUser := userStorage.Create(user)
	err := userStorage.Delete(createdUser.Username)
	if err != nil {
		t.Errorf("Expected no error, but got %v", err)
	}
	if len(userStorage.Data) != 0 {
		t.Errorf("Expected data length to be 0, but got %v", len(userStorage.Data))
	}
	if userStorage.PrimaryKeyUsername[createdUser.Username] != nil {
		t.Errorf("Expected user to be deleted from primaryKeyUsername")
	}

	err = userStorage.Delete("test_user")
	if err == nil {
		t.Errorf("Expected error, but got no error")
	}
}

func TestUserStorage_Update(t *testing.T) {
	userStorage := repository.NewUserStorage()
	user := model.User{
		Username:   "user",
		FirstName:  "Sandy",
		LastName:   "White",
		Email:      "user@example.com",
		Password:   "password",
		Phone:      "+1234567890",
		UserStatus: 1,
	}
	newUser := userStorage.Create(user)
	updatedUser := model.User{
		FirstName:  "Updated",
		LastName:   "User",
		Email:      "updateduser@example.com",
		Password:   "newpassword",
		Phone:      "+1234567890",
		UserStatus: 2,
	}
	updatedUser.ID = newUser.ID
	updatedUser.Username = newUser.Username
	_, err := userStorage.Update("user", updatedUser)
	if err != nil {
		t.Errorf("Expected no error but got %v", err)
	}
	foundUser, _ := userStorage.FindByUsername("user")
	if foundUser != updatedUser {
		t.Errorf("Expected %v but got %v", updatedUser, foundUser)
	}
}

func TestUserStorage_CreateArrayOfUsers(t *testing.T) {
	userStorage := repository.NewUserStorage()
	users := []model.User{
		{
			Username:   "user",
			FirstName:  "Sandy",
			LastName:   "White",
			Email:      "user@example.com",
			Password:   "password",
			Phone:      "+1234567890",
			UserStatus: 1,
		},
		{
			FirstName:  "Updated",
			LastName:   "User",
			Email:      "updateduser@example.com",
			Password:   "newpassword",
			Phone:      "+1234567890",
			UserStatus: 2,
		},
	}
	createdUsers := userStorage.CreateArrayOfUsers(users)
	users[0].ID = createdUsers[0].ID
	users[1].ID = createdUsers[1].ID
	if len(createdUsers) != len(users) {
		t.Errorf("Expected %v but got %v", len(users), len(createdUsers))
	}
	for i := 0; i < len(createdUsers); i++ {
		if createdUsers[i] != users[i] {
			t.Errorf("Expected %v but got %v", users[i], createdUsers[i])
		}
	}
	if len(userStorage.Data) != len(users) {
		t.Errorf("Expected %v but got %v", len(users), len(userStorage.Data))
	}
	for i := 0; i < len(userStorage.Data); i++ {
		if *userStorage.Data[i] != users[i] {
			t.Errorf("Expected %v but got %v", users[i], userStorage.Data[i])
		}
	}
}
