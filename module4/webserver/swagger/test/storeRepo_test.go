package test

import (
	"gitlab.com/AlexVuT/go-kata/module4/webserver/swagger/model"
	"gitlab.com/AlexVuT/go-kata/module4/webserver/swagger/repository"
	"testing"
)

func TestStoreStorage_Create(t *testing.T) {
	storeStorage := repository.NewStoreStorage()
	order := model.Store{
		PetID:    1,
		Quantity: 2,
		ShipDate: "2022-01-01T00:00:00Z",
		Status:   "placed",
		Complete: false,
	}

	createdOrder := storeStorage.Create(order)
	order.ID = createdOrder.ID
	if createdOrder.ID != 1 {
		t.Errorf("Expected ID to be 1, but got %v", createdOrder.ID)
	}
	if len(storeStorage.Data) != 1 {
		t.Errorf("Expected data length to be 1, but got %v", len(storeStorage.Data))
	}
	if storeStorage.AutoIncrementCount != 2 {
		t.Errorf("Expected autoIncrementCount to be 2, but got %v", storeStorage.AutoIncrementCount)
	}
	if *storeStorage.Data[0] != order {
		t.Errorf("Expected %v, but got %v", *storeStorage.Data[0], order)
	}
}

func TestStoreStorage_FindByID(t *testing.T) {
	storeStorage := repository.NewStoreStorage()
	order := model.Store{
		PetID:    1,
		Quantity: 2,
		ShipDate: "2022-01-01T00:00:00Z",
		Status:   "placed",
		Complete: false,
	}
	createdOrder := storeStorage.Create(order)
	retrievedOrder, err := storeStorage.FindByID(createdOrder.ID)
	if err != nil {
		t.Errorf("Expected no error, but got %v", err)
	}
	if retrievedOrder != createdOrder {
		t.Errorf("Expected %v, but got %v", createdOrder, retrievedOrder)
	}

	_, err = storeStorage.FindByID(100)
	if err == nil {
		t.Errorf("Expected error, but got no error")
	}
}

func TestStoreStorage_Delete(t *testing.T) {
	storeStorage := repository.NewStoreStorage()
	order := model.Store{
		PetID:    1,
		Quantity: 2,
		ShipDate: "2022-01-01T00:00:00Z",
		Status:   "placed",
		Complete: false,
	}
	createdOrder := storeStorage.Create(order)
	err := storeStorage.Delete(createdOrder.ID)
	if err != nil {
		t.Errorf("Expected no error, but got %v", err)
	}
	if len(storeStorage.Data) != 0 {
		t.Errorf("Expected data length to be 0, but got %v", len(storeStorage.Data))
	}
	if storeStorage.PrimaryKeyIDx[createdOrder.ID] != nil {
		t.Errorf("Expected order to be deleted from primaryKeyIDx")
	}

	err = storeStorage.Delete(100)
	if err == nil {
		t.Errorf("Expected error, but got no error")
	}
}

func TestStoreStorage_GetInventory(t *testing.T) {
	storeStorage := repository.NewStoreStorage()
	order1 := model.Store{
		PetID:    1,
		Quantity: 2,
		ShipDate: "2022-01-01T00:00:00Z",
		Status:   "placed",
		Complete: false,
	}
	order2 := model.Store{
		PetID:    2,
		Quantity: 1,
		ShipDate: "2022-01-02T00:00:00Z",
		Status:   "delivered",
		Complete: true,
	}
	order3 := model.Store{
		PetID:    4,
		Quantity: 5,
		ShipDate: "2022-01-02T00:00:00Z",
		Status:   "delivered",
		Complete: true,
	}
	storeStorage.Create(order1)
	storeStorage.Create(order2)
	storeStorage.Create(order3)

	inventory := storeStorage.GetInventory()
	if len(inventory) != 2 {
		t.Errorf("Expected inventory length to be 2, but got %v", len(inventory))
	}
	if inventory["placed"] != 2 {
		t.Errorf("Expected placed count to be 1, but got %v", inventory["placed"])
	}
	if inventory["delivered"] != order2.Quantity+order3.Quantity {
		t.Errorf("Expected delivered count to be %d, but got %v", order2.Quantity+order3.Quantity, inventory["delivered"])
	}
}
