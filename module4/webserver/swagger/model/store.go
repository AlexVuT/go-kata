// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse and unparse this JSON data, add this code to your project and do:
//
//    store, err := UnmarshalStore(bytes)
//    bytes, err = store.Marshal()

package model

import "encoding/json"

func UnmarshalStore(data []byte) (Store, error) {
	var r Store
	err := json.Unmarshal(data, &r)
	return r, err
}

func (r *Store) Marshal() ([]byte, error) {
	return json.Marshal(r)
}

type Store struct {
	ID       int    `json:"id"`
	PetID    int    `json:"petId"`
	Quantity int    `json:"quantity"`
	ShipDate string `json:"shipDate"`
	Status   string `json:"status"`
	Complete bool   `json:"complete"`
}
