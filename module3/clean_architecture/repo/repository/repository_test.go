package repository

import (
	"encoding/json"
	"os"
	"reflect"
	"testing"
)

func TestUserRepository_Save(t *testing.T) {
	file, err := os.CreateTemp("/home/alex/go/src/AlexVuT/go-kata/module3/clean_architecture/repo", "test.*.json")
	if err != nil {
		t.Fatal(err)
	}
	defer os.Remove(file.Name())
	defer file.Close()
	repo := NewUserRepository(file)

	user1 := User{ID: 1, Name: "Alice"}
	err = repo.Save(user1)
	if err != nil {
		t.Fatal(err)
	}

	data, err := os.ReadFile(file.Name())
	if err != nil {
		t.Fatal(err)
	}
	var users []User
	err = json.Unmarshal(data, &users)
	if err != nil {
		t.Fatal(err)
	}
	expected := []User{user1}
	if !reflect.DeepEqual(users, expected) {
		t.Errorf("Expected %v, but got %v", expected, users)
	}

	user2 := User{ID: 2, Name: "Bob"}
	user3 := User{ID: 3, Name: "Charlie"}
	err = repo.Save([]User{user2, user3})
	if err != nil {
		t.Fatal(err)
	}

	data, err = os.ReadFile(file.Name())
	if err != nil {
		t.Fatal(err)
	}
	err = json.Unmarshal(data, &users)
	if err != nil {
		t.Fatal(err)
	}
	expected = []User{user1, user2, user3}
	if !reflect.DeepEqual(users, expected) {
		t.Errorf("Expected %v, but got %v", expected, users)
	}
}

func TestUserRepository_FindAll(t *testing.T) {
	file, err := os.CreateTemp("/home/alex/go/src/AlexVuT/go-kata/module3/clean_architecture/repo", "test.*.json")
	if err != nil {
		t.Fatal(err)
	}
	defer os.Remove(file.Name())
	defer file.Close()
	repo := NewUserRepository(file)

	user1 := User{ID: 1, Name: "Alice"}
	user2 := User{ID: 2, Name: "Bob"}
	err = repo.Save([]User{user1, user2})
	if err != nil {
		t.Fatal(err)
	}

	users, err := repo.FindAll()
	if err != nil {
		t.Fatal(err)
	}

	expected := []interface{}{[]User{user1, user2}}
	if !reflect.DeepEqual(users, expected) {
		t.Errorf("Expected %v, but got %v", expected, users)
	}
}

func TestUserRepository_Find(t *testing.T) {
	file, err := os.CreateTemp("/home/alex/go/src/AlexVuT/go-kata/module3/clean_architecture/repo", "test.*.json")
	if err != nil {
		t.Fatal(err)
	}
	defer os.Remove(file.Name())
	defer file.Close()

	repo := NewUserRepository(file)

	user1 := User{ID: 1, Name: "Alice"}
	user2 := User{ID: 2, Name: "Bob"}
	err = repo.Save([]User{user1, user2})
	if err != nil {
		t.Fatal(err)
	}

	user, err := repo.Find(1)
	if err != nil {
		t.Fatal(err)
	}

	expected := user1
	if !reflect.DeepEqual(user, expected) {
		t.Errorf("Expected %v, but got %v", expected, user)
	}

	_, err = repo.Find(3)
	if err == nil {
		t.Errorf("Expected an error")
	}
}
