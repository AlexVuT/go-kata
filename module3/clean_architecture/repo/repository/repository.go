package repository

import (
	"encoding/json"
	"errors"
	"io"
	"os"
)

type Repository interface {
	Save(record interface{}) error
	Find(id int) (interface{}, error)
	FindAll() ([]User, error)
}

type UserRepository struct {
	File *os.File
}

func NewUserRepository(file *os.File) *UserRepository {
	return &UserRepository{
		File: file,
	}
}

type User struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}

func (r *UserRepository) Save(record interface{}) error {
	var users []User
	r.File.Seek(0, io.SeekStart)
	data, err := io.ReadAll(r.File)
	if err == io.EOF {
		os.Create(r.File.Name())
		return err
	}
	err = json.Unmarshal(data, &users)
	if err != nil {
		users = []User{}
	}
	switch user := record.(type) {
	case []User:
		users = append(users, user...)
	case User:
		users = append(users, user)
	}
	data1, err := json.MarshalIndent(users, "", " ")
	if err != nil {
		return err
	}
	_ = os.WriteFile(r.File.Name(), data1, 0644)
	return nil
}

func (r *UserRepository) FindAll() ([]interface{}, error) {
	var users []User
	r.File.Seek(0, io.SeekStart)
	decoder := json.NewDecoder(r.File)

	if err := decoder.Decode(&users); err != nil {
		return nil, err
	}
	var result []interface{}
	result = append(result, users)

	return result, nil
}

func (r *UserRepository) Find(id int) (interface{}, error) {
	r.File.Seek(0, io.SeekStart)
	data, err := io.ReadAll(r.File)
	if err != nil {
		return nil, err
	}

	var users []User
	err = json.Unmarshal(data, &users)
	if err != nil {
		return nil, err
	}
	var result interface{}
	for _, user := range users {
		if user.ID == id {
			result = user
			return result, nil
		}
	}
	return nil, errors.New("not found")
}
