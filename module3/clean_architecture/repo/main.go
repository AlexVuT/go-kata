package main

import (
	"fmt"
	. "gitlab.com/AlexVuT/go-kata/module3/clean_architecture/repo/repository"
	"os"
)

func main() {

	file, _ := os.OpenFile("/home/alex/go/src/AlexVuT/go-kata/module3/clean_architecture/repo/db.json", os.O_RDWR, 0644)
	repo := NewUserRepository(file)

	user := User{ID: 1, Name: "Andy"}
	users := []User{{ID: 2, Name: "Charlie"}, {ID: 3, Name: "Stan"}, {ID: 4, Name: "John"}, {ID: 5, Name: "Kate"}}
	err := repo.Save(user)
	if err != nil {
		panic(err)
	}
	fmt.Println(repo.FindAll())
	_ = repo.Save(users)
	fmt.Println(repo.FindAll())
	fmt.Println(repo.Find(3))
}
