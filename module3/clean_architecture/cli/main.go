package main

import (
	. "gitlab.com/AlexVuT/go-kata/module3/clean_architecture/cli/command"
	. "gitlab.com/AlexVuT/go-kata/module3/clean_architecture/cli/repo"
	. "gitlab.com/AlexVuT/go-kata/module3/clean_architecture/cli/service"

	"os"
)

func main() {
	file, _ := os.OpenFile("/home/alex/go/src/AlexVuT/go-kata/module3/clean_architecture/cli/tasks.json", os.O_RDWR|os.O_CREATE, 0644)
	repo := &FileTaskRepository{file.Name()}
	command := &Command{Service: TodoService{repo}}
	command.Run()
}
