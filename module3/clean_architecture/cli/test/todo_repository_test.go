package test

import (
	"encoding/json"
	. "gitlab.com/AlexVuT/go-kata/module3/clean_architecture/cli/model"
	. "gitlab.com/AlexVuT/go-kata/module3/clean_architecture/cli/repo"
	"os"
	"testing"
)

func TestGetTodoByTitle(t *testing.T) {
	file, err := os.CreateTemp("/home/alex/go/src/AlexVuT/go-kata/module3/clean_architecture/cli", "test*.*json")
	if err != nil {
		t.Fatal(err)
	}
	defer os.Remove(file.Name())
	defer file.Close()

	data := []Todo{
		{ID: 1, Title: "Task 1", Description: "Description 1", IsComplete: false},
		{ID: 2, Title: "Task 2", Description: "Description 2", IsComplete: true},
		{ID: 3, Title: "Task 3", Description: "Description 3", IsComplete: true},
	}
	oneTodo := Todo{ID: 2, Title: "Task 2", Description: "Description 2", IsComplete: true}

	fileData, err := json.Marshal(data)
	if err != nil {
		t.Fatal(err)
	}

	if _, err = file.Write(fileData); err != nil {
		t.Fatal(err)
	}

	repo := &FileTaskRepository{FilePath: file.Name()}

	res, err := repo.GetTodoByTitle("Task 2")
	if err != nil {
		t.Fatal(err)
	}

	if oneTodo.ID != res.ID || oneTodo.Title != res.Title ||
		oneTodo.Description != res.Description || oneTodo.IsComplete != res.IsComplete {
		t.Errorf("Todo does not match expected data")
	}
}

func TestGetTodoByID(t *testing.T) {
	file, err := os.CreateTemp("/home/alex/go/src/AlexVuT/go-kata/module3/clean_architecture/cli", "test*.*json")
	if err != nil {
		t.Fatal(err)
	}
	defer os.Remove(file.Name())
	defer file.Close()

	data := []Todo{
		{ID: 1, Title: "Task 1", Description: "Description 1", IsComplete: false},
		{ID: 2, Title: "Task 2", Description: "Description 2", IsComplete: true},
		{ID: 3, Title: "Task 3", Description: "Description 3", IsComplete: true},
	}
	oneTodo := Todo{ID: 2, Title: "Task 2", Description: "Description 2", IsComplete: true}

	fileData, err := json.Marshal(data)
	if err != nil {
		t.Fatal(err)
	}

	if _, err = file.Write(fileData); err != nil {
		t.Fatal(err)
	}

	repo := &FileTaskRepository{FilePath: file.Name()}

	res, err := repo.GetTodoByID(2)
	if err != nil {
		t.Fatal(err)
	}

	if oneTodo.ID != res.ID || oneTodo.Title != res.Title ||
		oneTodo.Description != res.Description || oneTodo.IsComplete != res.IsComplete {
		t.Errorf("Todo does not match expected data")
	}
}

func TestUpdateTodo(t *testing.T) {
	file, err := os.CreateTemp("/home/alex/go/src/AlexVuT/go-kata/module3/clean_architecture/cli", "test*.*json")
	if err != nil {
		t.Fatal(err)
	}
	defer os.Remove(file.Name())
	defer file.Close()

	data := []Todo{
		{ID: 1, Title: "Task 1", Description: "Description 1", IsComplete: false},
		{ID: 2, Title: "Task 2", Description: "Description 2", IsComplete: true},
		{ID: 3, Title: "Task 3", Description: "Description 3", IsComplete: true},
	}

	newTodo := Todo{ID: 3, Title: "Updated Task 3", Description: "Updated Description 3", IsComplete: false}

	newData := []Todo{
		{ID: 1, Title: "Task 1", Description: "Description 1", IsComplete: false},
		{ID: 2, Title: "Task 2", Description: "Description 2", IsComplete: true},
		{ID: 3, Title: "Updated Task 3", Description: "Updated Description 3", IsComplete: false},
	}
	fileData, err := json.Marshal(data)
	if err != nil {
		t.Fatal(err)
	}

	if _, err = file.Write(fileData); err != nil {
		t.Fatal(err)
	}

	repo := &FileTaskRepository{FilePath: file.Name()}

	err = repo.UpdateTodo(newTodo)
	if err != nil {
		t.Fatal(err)
	}
	todos, err := repo.GetTodos()
	if err != nil {
		t.Fatal(err)
	}

	if len(todos) != len(newData) {
		t.Errorf("Expected %d todos, but got %d", len(newData), len(todos))
	}
	for i, todo := range todos {
		if todo.ID != newData[i].ID || todo.Title != newData[i].Title ||
			todo.Description != newData[i].Description || todo.IsComplete != newData[i].IsComplete {
			t.Errorf("Todo #%d does not match expected data", i+1)
		}
	}

}

func TestCreateTodo(t *testing.T) {
	file, err := os.CreateTemp("/home/alex/go/src/AlexVuT/go-kata/module3/clean_architecture/cli", "test*.*json")
	if err != nil {
		t.Fatal(err)
	}
	defer os.Remove(file.Name())
	defer file.Close()

	data := []Todo{
		{ID: 1, Title: "Task 1", Description: "Description 1", IsComplete: false},
		{ID: 2, Title: "Task 2", Description: "Description 2", IsComplete: true},
	}
	newTodo := Todo{ID: 3, Title: "Task 3", Description: "Description 3", IsComplete: true}
	data1 := []Todo{
		{ID: 1, Title: "Task 1", Description: "Description 1", IsComplete: false},
		{ID: 2, Title: "Task 2", Description: "Description 2", IsComplete: true},
		{ID: 3, Title: "Task 3", Description: "Description 3", IsComplete: true},
	}
	fileData, err := json.Marshal(data)
	if err != nil {
		t.Fatal(err)
	}

	if _, err = file.Write(fileData); err != nil {
		t.Fatal(err)
	}

	repo := &FileTaskRepository{FilePath: file.Name()}

	err = repo.CreateTodo(newTodo)
	if err != nil {
		t.Fatal(err)
	}
	todos, err := repo.GetTodos()
	if err != nil {
		t.Fatal(err)
	}

	if len(todos) != len(data1) {
		t.Errorf("Expected %d todos, but got %d", len(data1), len(todos))
	}
	for i, todo := range todos {
		if todo.ID != data1[i].ID || todo.Title != data1[i].Title ||
			todo.Description != data1[i].Description || todo.IsComplete != data1[i].IsComplete {
			t.Errorf("Todo #%d does not match expected data", i+1)
		}
	}

}

func TestNewTodos(t *testing.T) {
	file, err := os.CreateTemp("/home/alex/go/src/AlexVuT/go-kata/module3/clean_architecture/cli", "test*.*json")
	if err != nil {
		t.Fatal(err)
	}
	defer os.Remove(file.Name())
	defer file.Close()

	data := []Todo{
		{ID: 1, Title: "Task 1", Description: "Description 1", IsComplete: false},
		{ID: 2, Title: "Task 2", Description: "Description 2", IsComplete: true},
	}
	data1 := []Todo{
		{ID: 3, Title: "Task 3", Description: "Description 3", IsComplete: true},
		{ID: 4, Title: "Task 4", Description: "Description 4", IsComplete: false},
	}

	fileData, err := json.Marshal(data)
	if err != nil {
		t.Fatal(err)
	}

	if _, err = file.Write(fileData); err != nil {
		t.Fatal(err)
	}

	repo := &FileTaskRepository{FilePath: file.Name()}

	err = repo.NewTodos(data1)
	if err != nil {
		t.Fatal(err)
	}
	todos, err := repo.GetTodos()
	if err != nil {
		t.Fatal(err)
	}

	if len(todos) != len(data1) {
		t.Errorf("Expected %d todos, but got %d", len(data1), len(todos))
	}
	for i, todo := range todos {
		if todo.ID != data1[i].ID || todo.Title != data1[i].Title ||
			todo.Description != data1[i].Description || todo.IsComplete != data1[i].IsComplete {
			t.Errorf("Todo #%d does not match expected data", i+1)
		}
	}

}

func TestDeleteTodo(t *testing.T) {
	file, err := os.CreateTemp("/home/alex/go/src/AlexVuT/go-kata/module3/clean_architecture/cli", "test*.*json")
	if err != nil {
		t.Fatal(err)
	}
	defer os.Remove(file.Name())
	defer file.Close()

	data := []Todo{
		{ID: 1, Title: "Task 1", Description: "Description 1", IsComplete: false},
		{ID: 2, Title: "Task 2", Description: "Description 2", IsComplete: true},
		{ID: 3, Title: "Task 3", Description: "Description 3", IsComplete: false},
	}

	fileData, err := json.Marshal(data)
	if err != nil {
		t.Fatal(err)
	}

	if _, err = file.Write(fileData); err != nil {
		t.Fatal(err)
	}
	data = []Todo{
		{ID: 1, Title: "Task 1", Description: "Description 1", IsComplete: false},
		{ID: 3, Title: "Task 3", Description: "Description 3", IsComplete: false},
	}

	repo := &FileTaskRepository{FilePath: file.Name()}
	err = repo.DeleteTodo(2)
	if err != nil {
		t.Fatal(err)
	}
	todos, err := repo.GetTodos()
	if err != nil {
		t.Fatal(err)
	}

	if len(todos) != len(data) {
		t.Errorf("Expected %d todos, but got %d", len(data), len(todos))
	}
	for i, todo := range todos {
		if todo.ID != data[i].ID || todo.Title != data[i].Title ||
			todo.Description != data[i].Description || todo.IsComplete != data[i].IsComplete {
			t.Errorf("Todo #%d does not match expected data", i+1)
		}
	}

}

func TestGetTodos(t *testing.T) {
	file, err := os.CreateTemp("/home/alex/go/src/AlexVuT/go-kata/module3/clean_architecture/cli", "test*.*json")
	if err != nil {
		t.Fatal(err)
	}
	defer os.Remove(file.Name())
	defer file.Close()

	data := []Todo{
		{ID: 1, Title: "Task 1", Description: "Description 1", IsComplete: false},
		{ID: 2, Title: "Task 2", Description: "Description 2", IsComplete: true},
		{ID: 3, Title: "Task 3", Description: "Description 3", IsComplete: false},
	}

	fileData, err := json.Marshal(data)
	if err != nil {
		t.Fatal(err)
	}

	if _, err = file.Write(fileData); err != nil {
		t.Fatal(err)
	}

	repo := &FileTaskRepository{FilePath: file.Name()}
	todos, err := repo.GetTodos()
	if err != nil {
		t.Fatal(err)
	}

	if len(todos) != len(data) {
		t.Errorf("Expected %d todos, but got %d", len(data), len(todos))
	}
	for i, todo := range todos {
		if todo.ID != data[i].ID || todo.Title != data[i].Title ||
			todo.Description != data[i].Description || todo.IsComplete != data[i].IsComplete {
			t.Errorf("Todo #%d does not match expected data", i+1)
		}
	}
}
