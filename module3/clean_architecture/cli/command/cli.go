package command

import (
	"bufio"
	"fmt"
	. "gitlab.com/AlexVuT/go-kata/module3/clean_architecture/cli/model"
	. "gitlab.com/AlexVuT/go-kata/module3/clean_architecture/cli/service"
	"os"
)

type ServiceTodo interface {
	ListTodos() ([]Todo, error)
	GetTodo(id int) (Todo, error)
	CreateTodo(title string) error
	CompleteTodo(title string) error
	RemoveTodo(id int) error
	UpdateTodo(id int, title string, description string, isCompleted bool) error
}

type Command struct {
	Service TodoService
}

func (c *Command) Run() {
	var input int
	for {
		fmt.Println("Введите номер команды:\n",
			"1. Показать список задач\n", "2. Добавить задачу\n", "3. Удалить задачу\n",
			"4. Редактировать задачу по ID\n", "5. Завершить задачу\n", "6. Выход\n")
		fmt.Scan(&input)

		switch input {
		case 1:
			c.ShowListOfTasks()
		case 2:
			c.AddTask()
		case 3:
			c.DeleteTask()
		case 4:
			c.EditTaskById()
		case 5:
			c.CompleteTask()
		case 6:
			return
		}
	}
}

func (c *Command) ShowListOfTasks() {
	list, err := c.Service.ListTodos()
	if err != nil {
		fmt.Println(err)
	} else {
		for _, task := range list {
			fmt.Printf("ID: %d Название: %s Описание: %s Статус выполнения: %v\n", task.ID, task.Title, task.Description, task.IsComplete)
		}
	}
}

func (c *Command) AddTask() {
	var title, description string
	scanner := bufio.NewScanner(os.Stdin)
	fmt.Println("Введите название задачи")
	scanner.Scan()
	title = scanner.Text()
	fmt.Println("Введите описание задачи")
	scanner.Scan()
	description = scanner.Text()
	err := c.Service.CreateTodo(title, description)
	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Println("Задача успешно добавлена")
	}
}

func (c *Command) DeleteTask() {
	var id int
	fmt.Println("Введите ID задачи")
	fmt.Scan(&id)
	err := c.Service.RemoveTodo(id)
	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Println("Задача успешно удалена")
	}
}

func (c *Command) EditTaskById() {
	var id int
	var title, description, isComplete string
	var completed bool
	scanner := bufio.NewScanner(os.Stdin)
	fmt.Println("Введите ID задачи")
	fmt.Scan(&id)
	fmt.Println("Введите новое название задачи")
	scanner.Scan()
	title = scanner.Text()
	fmt.Println("Введите новое описание задачи")
	scanner.Scan()
	description = scanner.Text()
	fmt.Println("Выполнена ли задача? (да/нет)")
	fmt.Scan(&isComplete)
	if isComplete == "да" {
		completed = true
	}
	if isComplete == "нет" {
		completed = false
	}
	err := c.Service.UpdateTodo(id, title, description, completed)
	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Println("Задача успешно отредактирована")
	}
}

func (c *Command) CompleteTask() {
	var title string
	scanner := bufio.NewScanner(os.Stdin)
	fmt.Println("Введите название задачи")
	scanner.Scan()
	title = scanner.Text()
	err := c.Service.CompleteTodo(title)
	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Println("Задача успешно выполнена")
	}
}
