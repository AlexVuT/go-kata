package test

import (
	"encoding/json"
	. "gitlab.com/AlexVuT/go-kata/module3/clean_architecture/service/model"
	. "gitlab.com/AlexVuT/go-kata/module3/clean_architecture/service/repo"
	. "gitlab.com/AlexVuT/go-kata/module3/clean_architecture/service/service"
	"os"
	"testing"
)

func TestServiceUpdateTodo(t *testing.T) {
	file, err := os.CreateTemp("/home/alex/go/src/AlexVuT/go-kata/module3/clean_architecture/cli", "test*.*json")
	if err != nil {
		t.Fatal(err)
	}
	defer os.Remove(file.Name())
	defer file.Close()
	repo := &FileTaskRepository{FilePath: file.Name()}
	service := TodoService{Repository: repo}
	data := []Todo{
		{ID: 1, Title: "Task 1", Description: "Description 1", IsComplete: false},
		{ID: 2, Title: "Task 2", Description: "Description 2", IsComplete: true},
		{ID: 3, Title: "Task 3", Description: "Description 3", IsComplete: false},
	}
	data1 := []Todo{
		{ID: 1, Title: "Task 1", Description: "Description 1", IsComplete: false},
		{ID: 2, Title: "Updated Task 2", Description: "Updated Description 2", IsComplete: false},
		{ID: 3, Title: "Task 3", Description: "Description 3", IsComplete: false},
	}

	fileData, err := json.Marshal(data)
	if err != nil {
		t.Fatal(err)
	}

	if _, err = file.Write(fileData); err != nil {
		t.Fatal(err)
	}

	err = service.UpdateTodo(2, "Updated Task 2", "Updated Description 2", false)
	if err != nil {
		t.Fatal(err)
	}
	todos, err := service.ListTodos()
	if err != nil {
		t.Fatal(err)
	}

	if len(todos) != len(data1) {
		t.Errorf("Expected %d todos, but got %d", len(data1), len(todos))
	}
	for i, todo := range todos {
		if todo.ID != data1[i].ID || todo.Title != data1[i].Title ||
			todo.Description != data1[i].Description || todo.IsComplete != data1[i].IsComplete {
			t.Errorf("Todo #%d does not match expected data", i+1)
		}
	}
}

func TestServiceRemoveTodo(t *testing.T) {
	file, err := os.CreateTemp("/home/alex/go/src/AlexVuT/go-kata/module3/clean_architecture/cli", "test*.*json")
	if err != nil {
		t.Fatal(err)
	}
	defer os.Remove(file.Name())
	defer file.Close()
	repo := &FileTaskRepository{FilePath: file.Name()}
	service := TodoService{Repository: repo}
	data := []Todo{
		{ID: 1, Title: "Task 1", Description: "Description 1", IsComplete: false},
		{ID: 2, Title: "Task 2", Description: "Description 2", IsComplete: true},
		{ID: 3, Title: "Task 3", Description: "Description 3", IsComplete: false},
	}
	data1 := []Todo{
		{ID: 1, Title: "Task 1", Description: "Description 1", IsComplete: false},
		{ID: 3, Title: "Task 3", Description: "Description 3", IsComplete: false},
	}

	fileData, err := json.Marshal(data)
	if err != nil {
		t.Fatal(err)
	}

	if _, err = file.Write(fileData); err != nil {
		t.Fatal(err)
	}

	err = service.RemoveTodo(2)
	if err != nil {
		t.Fatal(err)
	}
	todos, err := service.ListTodos()
	if err != nil {
		t.Fatal(err)
	}

	if len(todos) != len(data1) {
		t.Errorf("Expected %d todos, but got %d", len(data1), len(todos))
	}
	for i, todo := range todos {
		if todo.ID != data1[i].ID || todo.Title != data1[i].Title ||
			todo.Description != data1[i].Description || todo.IsComplete != data1[i].IsComplete {
			t.Errorf("Todo #%d does not match expected data", i+1)
		}
	}
}

func TestServiceCompleteTodo(t *testing.T) {
	file, err := os.CreateTemp("/home/alex/go/src/AlexVuT/go-kata/module3/clean_architecture/cli", "test*.*json")
	if err != nil {
		t.Fatal(err)
	}
	defer os.Remove(file.Name())
	defer file.Close()
	repo := &FileTaskRepository{FilePath: file.Name()}
	service := TodoService{Repository: repo}
	data := []Todo{
		{ID: 1, Title: "Task 1", Description: "Description 1", IsComplete: false},
		{ID: 2, Title: "Task 2", Description: "Description 2", IsComplete: true},
		{ID: 3, Title: "Task 3", Description: "Description 3", IsComplete: false},
	}
	data1 := []Todo{
		{ID: 1, Title: "Task 1", Description: "Description 1", IsComplete: false},
		{ID: 2, Title: "Task 2", Description: "Description 2", IsComplete: true},
		{ID: 3, Title: "Task 3", Description: "Description 3", IsComplete: true},
	}

	fileData, err := json.Marshal(data)
	if err != nil {
		t.Fatal(err)
	}

	if _, err = file.Write(fileData); err != nil {
		t.Fatal(err)
	}

	err = service.CompleteTodo("Task 3")
	if err != nil {
		t.Fatal(err)
	}
	todos, err := service.ListTodos()
	if err != nil {
		t.Fatal(err)
	}

	if len(todos) != len(data1) {
		t.Errorf("Expected %d todos, but got %d", len(data1), len(todos))
	}
	for i, todo := range todos {
		if todo.ID != data1[i].ID || todo.Title != data1[i].Title ||
			todo.Description != data1[i].Description || todo.IsComplete != data1[i].IsComplete {
			t.Errorf("Todo #%d does not match expected data", i+1)
		}
	}
}

func TestServiceCreateTodo(t *testing.T) {
	file, err := os.CreateTemp("/home/alex/go/src/AlexVuT/go-kata/module3/clean_architecture/cli", "test*.*json")
	if err != nil {
		t.Fatal(err)
	}
	defer os.Remove(file.Name())
	defer file.Close()
	repo := &FileTaskRepository{FilePath: file.Name()}
	service := TodoService{Repository: repo}
	data := []Todo{
		{ID: 1, Title: "Task 1", Description: "Description 1", IsComplete: false},
		{ID: 2, Title: "Task 2", Description: "Description 2", IsComplete: true},
	}
	data1 := []Todo{
		{ID: 1, Title: "Task 1", Description: "Description 1", IsComplete: false},
		{ID: 2, Title: "Task 2", Description: "Description 2", IsComplete: true},
		{ID: 3, Title: "Task 3", Description: "Description 3"},
	}

	fileData, err := json.Marshal(data)
	if err != nil {
		t.Fatal(err)
	}

	if _, err = file.Write(fileData); err != nil {
		t.Fatal(err)
	}

	err = service.CreateTodo("Task 3", "Description 3")
	if err != nil {
		t.Fatal(err)
	}

	todos, err := service.ListTodos()
	if err != nil {
		t.Fatal(err)
	}

	if len(todos) != len(data1) {
		t.Errorf("Expected %d todos, but got %d", len(data1), len(todos))
	}
	for i, todo := range todos {
		if todo.ID != data1[i].ID || todo.Title != data1[i].Title ||
			todo.Description != data1[i].Description || todo.IsComplete != data1[i].IsComplete {
			t.Errorf("Todo #%d does not match expected data", i+1)
		}
	}
}

func TestServiceGetTodo(t *testing.T) {
	file, err := os.CreateTemp("/home/alex/go/src/AlexVuT/go-kata/module3/clean_architecture/cli", "test*.*json")
	if err != nil {
		t.Fatal(err)
	}
	defer os.Remove(file.Name())
	defer file.Close()
	repo := &FileTaskRepository{FilePath: file.Name()}
	service := TodoService{Repository: repo}
	data := []Todo{
		{ID: 1, Title: "Task 1", Description: "Description 1", IsComplete: false},
		{ID: 2, Title: "Task 2", Description: "Description 2", IsComplete: true},
		{ID: 3, Title: "Task 3", Description: "Description 3", IsComplete: false},
	}
	oneTodo := Todo{ID: 2, Title: "Task 2", Description: "Description 2", IsComplete: true}

	fileData, err := json.Marshal(data)
	if err != nil {
		t.Fatal(err)
	}

	if _, err = file.Write(fileData); err != nil {
		t.Fatal(err)
	}

	res, err := service.GetTodo(2)
	if err != nil {
		t.Fatal(err)
	}

	if oneTodo.ID != res.ID || oneTodo.Title != res.Title ||
		oneTodo.Description != res.Description || oneTodo.IsComplete != res.IsComplete {
		t.Errorf("Todo does not match expected data")
	}
}

func TestServiceListTodos(t *testing.T) {
	file, err := os.CreateTemp("/home/alex/go/src/AlexVuT/go-kata/module3/clean_architecture/cli", "test*.*json")
	if err != nil {
		t.Fatal(err)
	}
	defer os.Remove(file.Name())
	defer file.Close()
	repo := &FileTaskRepository{FilePath: file.Name()}
	service := TodoService{Repository: repo}
	data := []Todo{
		{ID: 1, Title: "Task 1", Description: "Description 1", IsComplete: false},
		{ID: 2, Title: "Task 2", Description: "Description 2", IsComplete: true},
		{ID: 3, Title: "Task 3", Description: "Description 3", IsComplete: false},
	}

	fileData, err := json.Marshal(data)
	if err != nil {
		t.Fatal(err)
	}

	if _, err = file.Write(fileData); err != nil {
		t.Fatal(err)
	}

	todos, err := service.ListTodos()
	if err != nil {
		t.Fatal(err)
	}

	if len(todos) != len(data) {
		t.Errorf("Expected %d todos, but got %d", len(data), len(todos))
	}
	for i, todo := range todos {
		if todo.ID != data[i].ID || todo.Title != data[i].Title ||
			todo.Description != data[i].Description || todo.IsComplete != data[i].IsComplete {
			t.Errorf("Todo #%d does not match expected data", i+1)
		}
	}
}
