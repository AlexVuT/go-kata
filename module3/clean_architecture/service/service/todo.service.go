package service

import (
	. "gitlab.com/AlexVuT/go-kata/module3/clean_architecture/service/model"
)

type TodoRepository interface {
	GetTodos() ([]Todo, error)
	GetTodoByID(id int) (Todo, error)
	GetTodoByTitle(title string) (Todo, error)
	CreateTodo(task Todo) error
	UpdateTodo(task Todo) error
	DeleteTodo(id int) error
	NewTodos([]Todo) error
}

type TodoService struct {
	Repository TodoRepository
}

func (s *TodoService) UpdateTodo(id int, title string, description string, isCompleted bool) error {
	todo := Todo{
		ID:          id,
		Title:       title,
		Description: description,
		IsComplete:  isCompleted,
	}
	err := s.Repository.UpdateTodo(todo)
	if err != nil {
		return err
	}
	return nil
}

func (s *TodoService) CompleteTodo(title string) error {
	todo, err := s.Repository.GetTodoByTitle(title)
	if err != nil {
		return err
	}
	todo.IsComplete = true
	err = s.Repository.UpdateTodo(todo)
	if err != nil {
		return err
	}
	return nil
}

func (s *TodoService) GetTodo(id int) (Todo, error) {
	task, err := s.Repository.GetTodoByID(id)
	if err != nil {
		return Todo{}, err
	}
	return task, nil
}

func (s *TodoService) ListTodos() ([]Todo, error) {
	tasks, err := s.Repository.GetTodos()
	if err != nil {
		return nil, err
	}
	return tasks, nil
}

func (s *TodoService) CreateTodo(title string, description string) error {
	err := s.Repository.CreateTodo(Todo{Title: title, Description: description, IsComplete: false})
	if err != nil {
		return err
	}
	return nil
}

func (s *TodoService) RemoveTodo(id int) error {
	err := s.Repository.DeleteTodo(id)
	if err != nil {
		return err
	}
	return nil
}
