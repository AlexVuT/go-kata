package repo

import (
	"encoding/json"
	"fmt"
	. "gitlab.com/AlexVuT/go-kata/module3/clean_architecture/service/model"
	"io"
	"os"
)

type FileTaskRepository struct {
	FilePath string
}

func (repo *FileTaskRepository) DeleteTodo(id int) error {
	tasks, err := repo.GetTodos()
	if err != nil {
		return err
	}

	_, err = repo.GetTodoByID(id)
	if err != nil {
		return err
	}

	for i, task := range tasks {
		if task.ID == id {
			tasks = append(tasks[:i], tasks[i+1:]...)
		}
	}
	err = repo.NewTodos(tasks)
	if err != nil {
		return err
	}
	return nil
}

func (repo *FileTaskRepository) NewTodos(tasks []Todo) error {
	data, err := json.Marshal(tasks)
	if err != nil {
		return err
	}
	err = os.WriteFile(repo.FilePath, data, 0644)
	if err != nil {
		return err
	}
	return nil
}

func (repo *FileTaskRepository) GetTodos() ([]Todo, error) {
	var tasks []Todo

	file, err := os.Open(repo.FilePath)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	content, err := io.ReadAll(file)
	if err != nil {
		return nil, err
	}

	if err = json.Unmarshal(content, &tasks); err != nil {
		return nil, err
	}

	return tasks, nil
}

func (repo *FileTaskRepository) GetTodoByID(id int) (Todo, error) {
	var task Todo

	tasks, err := repo.GetTodos()
	if err != nil {
		return task, err
	}

	for _, t := range tasks {
		if t.ID == id {
			return t, nil
		}
	}

	return task, fmt.Errorf("task is not found")
}

func (repo *FileTaskRepository) GetTodoByTitle(title string) (Todo, error) {
	var task Todo
	tasks, err := repo.GetTodos()
	if err != nil {
		return task, err
	}

	for _, t := range tasks {
		if t.Title == title {
			return t, nil
		}
	}

	return task, fmt.Errorf("task is not found")
}

func (repo *FileTaskRepository) CreateTodo(task Todo) error {
	tasks, _ := repo.GetTodos()
	var count int
	count++
repeat:
	for i, t := range tasks {
		if count == t.ID {
			count++
			goto repeat
		}
		if i == len(tasks) {
			count++
		}

	}
	task.ID = count
	tasks = append(tasks, task)

	if err := repo.NewTodos(tasks); err != nil {
		return err
	}
	return nil
}

func (repo *FileTaskRepository) UpdateTodo(task Todo) error {
	tasks, err := repo.GetTodos()
	if err != nil {
		return err
	}

	for i, t := range tasks {
		if t.ID == task.ID {
			tasks[i] = task
			break
		}
	}

	if err = repo.NewTodos(tasks); err != nil {
		return err
	}

	return nil
}
