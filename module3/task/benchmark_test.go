package main

import (
	"github.com/brianvoe/gofakeit/v6"
	"math/rand"
	"testing"
	"time"
)

func BenchmarkDoubleLinkedList_Pop(b *testing.B) {
	d := &DoubleLinkedList{}
	GenerateJSON(1000, "/home/alex/go/src/AlexVuT/go-kata/module3/task/test_bench.json")
	_ = d.LoadData("/home/alex/go/src/AlexVuT/go-kata/module3/task/test_bench.json")
	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		_, _ = d.Pop()
	}
}

func BenchmarkDoubleLinkedList_Shift(b *testing.B) {
	d := &DoubleLinkedList{}
	GenerateJSON(1000, "/home/alex/go/src/AlexVuT/go-kata/module3/task/test_bench.json")
	_ = d.LoadData("/home/alex/go/src/AlexVuT/go-kata/module3/task/test_bench.json")
	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		_, _ = d.Shift()
	}
}

func BenchmarkDoubleLinkedList_Insert(b *testing.B) {
	d := &DoubleLinkedList{}
	GenerateJSON(1000, "/home/alex/go/src/AlexVuT/go-kata/module3/task/test_bench.json")
	_ = d.LoadData("/home/alex/go/src/AlexVuT/go-kata/module3/task/test_bench.json")
	commit := Commit{Message: gofakeit.Animal()}
	rand.NewSource(time.Now().UnixNano())
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		_ = d.Insert(rand.Intn(1000+1), commit)
	}
}

func BenchmarkDoubleLinkedList_Delete(b *testing.B) {
	d := &DoubleLinkedList{}
	GenerateJSON(1000, "/home/alex/go/src/AlexVuT/go-kata/module3/task/test_bench.json")
	_ = d.LoadData("/home/alex/go/src/AlexVuT/go-kata/module3/task/test_bench.json")
	rand.NewSource(time.Now().UnixNano())
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		_ = d.Delete(rand.Intn(d.len + 1))
	}
}

func BenchmarkDoubleLinkedList_Reverse(b *testing.B) {
	d := &DoubleLinkedList{}
	GenerateJSON(1000, "/home/alex/go/src/AlexVuT/go-kata/module3/task/test_bench.json")
	_ = d.LoadData("/home/alex/go/src/AlexVuT/go-kata/module3/task/test_bench.json")
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		_ = d.Reverse()
	}
}

func BenchmarkDoubleLinkedList_SearchByIndex(b *testing.B) {
	d := &DoubleLinkedList{}
	GenerateJSON(1000, "/home/alex/go/src/AlexVuT/go-kata/module3/task/test_bench.json")
	_ = d.LoadData("/home/alex/go/src/AlexVuT/go-kata/module3/task/test_bench.json")
	rand.NewSource(time.Now().UnixNano())
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		_, _ = d.SearchByIndex(rand.Intn(d.len + 1))
	}
}

func BenchmarkGenerateJSON100(b *testing.B) {
	d := &DoubleLinkedList{}
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		GenerateJSON(100, "/home/alex/go/src/AlexVuT/go-kata/module3/task/test_bench.json")
		_ = d.LoadData("/home/alex/go/src/AlexVuT/go-kata/module3/task/test_bench.json")
	}
}

func BenchmarkGenerateJSON1000(b *testing.B) {
	d := &DoubleLinkedList{}
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		GenerateJSON(1000, "/home/alex/go/src/AlexVuT/go-kata/module3/task/test_bench.json")
		_ = d.LoadData("/home/alex/go/src/AlexVuT/go-kata/module3/task/test_bench.json")
	}
}
