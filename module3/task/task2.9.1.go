package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/brianvoe/gofakeit/v6"
	"math/rand"
	"os"
	"time"
)

type DoubleLinkedList struct {
	head *Node // начальный элемент в списке
	tail *Node // последний элемент в списке
	curr *Node // текущий элемент меняется при использовании методов next, prev
	len  int   // количество элементов в списке
}

type LinkedLister interface {
	Len() int
	Current() *Node
	Next() *Node
	Prev() *Node
	LoadData(path string) error
	Insert(n int, c Commit) error
	Delete(n int) error
	DeleteCurrent() error
	Index() (int, error)
	SearchByIndex(n int) (*Node, error)
	isEmpty() bool
	Pop() *Node
	Shift() *Node
	SearchUUID(uuID string) *Node
	Search(message string) *Node
	Reverse() *DoubleLinkedList
}

func QuickSort(commits []Commit) []Commit {
	if len(commits) <= 1 {
		return commits
	}
	median := commits[rand.Intn(len(commits))]

	early_part := make([]Commit, 0, len(commits))
	late_part := make([]Commit, 0, len(commits))
	current_part := make([]Commit, 0, len(commits))

	for _, commit := range commits {
		switch {
		case commit.Date.Before(median.Date):
			early_part = append(early_part, commit)
		case commit.Date.After(median.Date):
			late_part = append(late_part, commit)
		case commit.Date.Equal(median.Date):
			current_part = append(current_part, commit)
		}
	}

	early_part = QuickSort(early_part)
	late_part = QuickSort(late_part)

	early_part = append(early_part, current_part...)
	early_part = append(early_part, late_part...)
	return early_part
}

// LoadData загрузка данных из подготовленного json файла
func (d *DoubleLinkedList) LoadData(path string) error {
	data, err := os.ReadFile(path)
	if err != nil {
		return err
	}
	var commits []Commit
	err = json.Unmarshal(data, &commits)
	if err != nil {
		return err
	}
	commits = QuickSort(commits)
	// отсортировать список используя самописный QuickSort
	for _, commit := range commits {
		err := d.Insert(d.len, commit)
		if err != nil {
			return err
		}
	}
	return nil
}

// Len получение длины списка
func (d *DoubleLinkedList) Len() int {
	return d.len
}

// Current получение текущего элемента
func (d *DoubleLinkedList) Current() *Node {
	return d.curr
}

// Next получение следующего элемента
func (d *DoubleLinkedList) Next() *Node {
	if d.curr == d.tail {
		panic(errors.New("Current element is last"))
	}
	d.curr = d.curr.next
	return d.curr
}

// Prev получение предыдущего элемента
func (d *DoubleLinkedList) Prev() *Node {
	if d.curr == d.head {
		panic(errors.New("current element is first"))
	}
	d.curr = d.curr.prev
	return d.curr
}

// Insert вставка элемента после n элемента
func (d *DoubleLinkedList) Insert(n int, c Commit) error {
	if n < 0 || n > d.len {
		return errors.New("index is out of range")
	}
	if d.len == 0 {
		node := &Node{data: &c}
		d.head = node
		d.tail = node
		d.curr = node
		d.len++
		return nil
	}
	if n == 0 {
		node := &Node{data: &c, next: d.head}
		d.head.prev = node
		d.head = node
		d.len++
		return nil
	}
	if n == d.len {
		node := &Node{data: &c, prev: d.tail}
		d.tail.next = node
		d.tail = node
		d.len++
		return nil
	}

	_, err := d.SearchByIndex(n)
	if err != nil {
		return err
	}
	node := &Node{data: &c, next: d.curr.next, prev: d.curr}
	d.curr.next = node
	d.curr.next.next.prev = node
	d.len++
	return nil
}

// Delete удаление n элемента
func (d *DoubleLinkedList) Delete(n int) error {
	_, err := d.SearchByIndex(n)
	if err != nil {
		return err
	}
	err = d.DeleteCurrent()
	if err != nil {
		return err
	}
	return nil
}

// DeleteCurrent удаление текущего элемента
func (d *DoubleLinkedList) DeleteCurrent() error {
	if d.len == 0 {
		return errors.New("list is empty")
	}
	if d.curr == nil {
		return errors.New("current element is empty")
	}
	if d.head == d.tail {
		d.head = nil
		d.curr = nil
		d.tail = nil
		d.len--
		return nil
	}
	if d.curr == d.head {
		d.head = d.head.next
		d.head.prev = nil
		d.curr = d.head
		d.len--
		return nil
	}
	if d.curr == d.tail {
		d.tail = d.tail.prev
		d.tail.next = nil
		d.curr = d.tail
		d.len--
		return nil
	}

	d.curr.prev.next = d.curr.next
	d.curr.next.prev = d.curr.prev
	d.len--
	return nil
}

// Index получение индекса текущего элемента
func (d *DoubleLinkedList) Index() (int, error) {
	if d.curr == nil {
		return 0, errors.New("current element is empty")
	}
	var count int
	count++
	start := d.head
	for start != d.curr {
		start = start.next
		count++
	}
	return count, nil
}

func (d *DoubleLinkedList) SearchByIndex(n int) (*Node, error) {
	n--
	if n < 0 || n > d.len {
		return nil, errors.New("index is out of range")
	}
	start := d.head
	end := d.tail
	var result *Node
	if n <= d.len/2 {
		for i := 0; i <= n; i++ {
			result = start
			start = start.next
		}
	} else {
		for i := d.len - 1; i >= n; i-- {
			result = end
			end = end.prev
		}
	}
	d.curr = result
	return result, nil
}

func (d *DoubleLinkedList) isEmpty() bool {
	return d.len == 0
}

// Pop Операция Pop
func (d *DoubleLinkedList) Pop() (*Node, error) {
	if d.isEmpty() {
		return nil, errors.New("empty")
	}
	last := d.tail
	err := d.Delete(d.len)
	if err != nil {
		return nil, err
	}
	return last, nil
}

// Shift операция shift
func (d *DoubleLinkedList) Shift() (*Node, error) {
	if d.isEmpty() {
		return nil, errors.New("empty")
	}
	first := d.head
	err := d.Delete(1)
	if err != nil {
		return nil, err
	}
	return first, nil
}

// SearchUUID поиск коммита по uuid
func (d *DoubleLinkedList) SearchUUID(uuID string) (*Node, error) {
	current := d.head
	for current != nil {
		if current.data.UUID == uuID {
			d.curr = current
			return current, nil
		} else {
			current = current.next
		}
	}
	return nil, errors.New("requested commit not found")
}

// Search поиск коммита по message
func (d *DoubleLinkedList) Search(message string) (*Node, error) {
	current := d.head
	for current != nil {
		if current.data.Message == message {
			d.curr = current
			return current, nil
		} else {
			current = current.next
		}
	}
	return nil, errors.New("requested commit not found")
}

// Reverse возвращает перевернутый список
func (d *DoubleLinkedList) Reverse() *DoubleLinkedList {
	reverseList := &DoubleLinkedList{len: d.len}
	if d.len <= 0 {
		return reverseList
	}
	node := d.tail
	reverseNode := &Node{}
	reverseList.head = reverseNode
	for i := 0; i < d.len-1; i++ {
		reverseNode.data = node.data
		node = node.prev
		reverseNode.next = &Node{prev: reverseNode}
		reverseNode = reverseNode.next
	}
	reverseNode.data = d.head.data
	reverseList.tail = reverseNode
	return reverseList
}

type Node struct {
	data *Commit
	prev *Node
	next *Node
}

type Commit struct {
	Message string    `json:"message"`
	UUID    string    `json:"uuid"`
	Date    time.Time `json:"date"`
}

func GenerateJSON(n int, path string) {
	commits := make([]Commit, n)
	for i := 0; i < len(commits); i++ {
		commits[i] = Commit{
			Message: gofakeit.Animal(),
			UUID:    gofakeit.UUID(),
			Date:    gofakeit.Date(),
		}
		file, err := json.Marshal(commits)
		if err != nil {
			panic(err)
		}
		err = os.WriteFile(path, file, 0644)
		if err != nil {
			panic(err)
		}
	}

}
func main() {

	d := &DoubleLinkedList{}
	err := d.LoadData("/home/alex/go/src/AlexVuT/go-kata/module3/task/test.json")
	if err != nil {
		panic(err)
	}

	c := &Commit{
		Message: "ape",
		UUID:    "f1ae1d04-71d1-41a4-a267-0a35e4f2b24a",
		Date:    gofakeit.Date(),
	}
	d.Insert(0, *c)
	d.SearchByIndex(1)
	fmt.Println(d.curr.data)
	d.SearchByIndex(2)
	fmt.Println(d.curr.data)
	d.SearchByIndex(3)
	fmt.Println(d.curr.data)
	d.SearchByIndex(4)
	fmt.Println(d.curr.data)
	d.SearchByIndex(5)
	fmt.Println(d.curr.data)
	d.SearchByIndex(6)
	fmt.Println(d.curr.data)
	d.SearchByIndex(7)

	r := d.Reverse()

	fmt.Println(r)
	r.SearchByIndex(1)
	fmt.Println(r.curr.data)
	r.SearchByIndex(2)
	fmt.Println(r.curr.data)
	r.SearchByIndex(3)
	fmt.Println(r.curr.data)
	r.SearchByIndex(4)
	fmt.Println(r.curr.data)
	r.SearchByIndex(5)
	fmt.Println(r.curr.data)
	r.SearchByIndex(6)
	fmt.Println(r.curr.data)
	r.Shift()
	r.Shift()
	r.Shift()
	r.Pop()
	r.Pop()
	r.Pop()

	fmt.Println(r)

}
