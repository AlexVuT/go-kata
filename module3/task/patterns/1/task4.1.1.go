package main

import "fmt"

type Order struct {
	product  string
	price    float64
	quantity int
}

type RegularPricing struct {
}

type SalesPricing struct {
	name     string
	discount int
}

type PricingStrategy interface {
	Calculate(o Order) float64
}

func (s *RegularPricing) Calculate(o Order) float64 {
	sum := o.price * float64(o.quantity)
	return sum
}

func (s *SalesPricing) Calculate(o Order) float64 {
	var sum, discount float64
	sum = o.price * float64(o.quantity)
	discount = o.price * float64(o.quantity) * (float64(s.discount) / 100)
	return sum - discount
}

func main() {
	slice := []PricingStrategy{&RegularPricing{}, &SalesPricing{name: "Regular customer", discount: 5},
		&SalesPricing{name: "First order", discount: 10}, &SalesPricing{name: "Anniversary sale", discount: 15}}

	shoes := Order{
		product:  "boots",
		price:    75,
		quantity: 10,
	}

	for _, v := range slice {
		total := v.Calculate(shoes)
		fmt.Printf("Total cost with %T strategy: %.2f\n", v, total)
	}
}
