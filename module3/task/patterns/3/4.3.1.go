package main

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
)

const apiKey = "d10b1e4bf1eb1045ee7e649fb6f4300d"

type fullData struct {
	Coord struct {
		Lon float64 `json:"lon"`
		Lat int     `json:"lat"`
	} `json:"coord"`
	Weather []struct {
		Id          int    `json:"id"`
		Main        string `json:"main"`
		Description string `json:"description"`
		Icon        string `json:"icon"`
	} `json:"weather"`
	Base string `json:"base"`
	Main struct {
		Temp      float64 `json:"temp"`
		FeelsLike float64 `json:"feels_like"`
		TempMin   float64 `json:"temp_min"`
		TempMax   float64 `json:"temp_max"`
		Pressure  int     `json:"pressure"`
		Humidity  int     `json:"humidity"`
		SeaLevel  int     `json:"sea_level"`
		GrndLevel int     `json:"grnd_level"`
	} `json:"main"`
	Visibility int `json:"visibility"`
	Wind       struct {
		Speed float64 `json:"speed"`
		Deg   int     `json:"deg"`
		Gust  float64 `json:"gust"`
	} `json:"wind"`
	Clouds struct {
		All int `json:"all"`
	} `json:"clouds"`
	Dt  int `json:"dt"`
	Sys struct {
		Type    int    `json:"type"`
		Id      int    `json:"id"`
		Country string `json:"country"`
		Sunrise int    `json:"sunrise"`
		Sunset  int    `json:"sunset"`
	} `json:"sys"`
	Timezone int    `json:"timezone"`
	Id       int    `json:"id"`
	Name     string `json:"name"`
	Cod      int    `json:"cod"`
}

type WeatherAPI interface {
	GetTemperature(data fullData) float64
	GetHumidity(data fullData) int
	GetWindSpeed(data fullData) float64
	GetWeatherInfo(location string) (fullData, error)
}

type OpenWeatherAPI struct {
	apiKey string
	api    *http.Client
}

func (o *OpenWeatherAPI) GetWeatherInfo(location string) (fullData, error) {
	var data fullData
	get, err := o.api.Get(fmt.Sprintf("https://api.openweathermap.org/data/2.5/weather?q=%s&appid=%s", location, o.apiKey))
	if err != nil {
		return fullData{}, err
	}
	body, err := io.ReadAll(get.Body)
	if err != nil {
		return fullData{}, err
	}
	err = json.Unmarshal(body, &data)

	return data, nil
}

func (o *OpenWeatherAPI) GetTemperature(data fullData) float64 {
	return data.Main.Temp
}

func (o *OpenWeatherAPI) GetHumidity(data fullData) int {
	return data.Main.Humidity
}

func (o *OpenWeatherAPI) GetWindSpeed(data fullData) float64 {
	return data.Wind.Speed
}

type WeatherFacade struct {
	weatherAPI WeatherAPI
}

func (w *WeatherFacade) GetWeatherInfo(location string) (float64, int, float64) {
	data, err := w.weatherAPI.GetWeatherInfo(location)
	if err != nil {
		fmt.Println(err)
		return 0, 0, 0
	}
	temperature := w.weatherAPI.GetTemperature(data)
	temperature = kelvinToCelsius(temperature)
	humidity := w.weatherAPI.GetHumidity(data)
	windSpeed := w.weatherAPI.GetWindSpeed(data)

	return temperature, humidity, windSpeed
}

func NewWeatherFacade(apiKey string) WeatherFacade {
	return WeatherFacade{
		weatherAPI: &OpenWeatherAPI{apiKey: apiKey,
			api: &http.Client{},
		},
	}
}

func kelvinToCelsius(kelvin float64) float64 {
	return kelvin - 273.15
}

func main() {
	weatherFacade := NewWeatherFacade(apiKey)
	cities := []string{"Москва", "Санкт Петербург", "Казань", "Якутск"}

	for _, city := range cities {
		temperature, humidity, windSpeed := weatherFacade.GetWeatherInfo(city)
		fmt.Printf("Temperature in "+city+": %.1f\n", temperature)
		fmt.Printf("Humidity in "+city+": %.d\n", humidity)
		fmt.Printf("Wind speed in "+city+": %.1f\n\n", windSpeed)
	}
}
