package main

import "fmt"

type RealAirConditioner struct {
	temperature int
	IsOn        bool
}

func NewRealAirConditioner() *RealAirConditioner {
	return &RealAirConditioner{}
}

func (r *RealAirConditioner) TurnOn() {
	r.IsOn = true
	fmt.Println("Air conditioner is ON")
}

func (r *RealAirConditioner) TurnOff() {
	r.IsOn = false
	fmt.Println("Air conditioner is OFF")
}

func (r *RealAirConditioner) SetTemperature(degrees int) {
	r.temperature = degrees
	fmt.Printf("Temperature is set to %d Celsius degrees", degrees)
}
