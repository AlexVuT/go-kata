package main

type AirConditionerAdapter struct {
	airConditioner *RealAirConditioner
}

func NewAirConditionerAdapter() *AirConditionerAdapter {
	return &AirConditionerAdapter{airConditioner: NewRealAirConditioner()}
}

func (a *AirConditionerAdapter) TurnOn() {
	a.airConditioner.TurnOn()
}

func (a *AirConditionerAdapter) TurnOff() {
	a.airConditioner.TurnOff()
}

func (a *AirConditionerAdapter) SetTemperature(degrees int) {
	a.airConditioner.SetTemperature(degrees)
}
