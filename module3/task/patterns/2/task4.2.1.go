package main

type AirConditioner interface {
	TurnOn()
	TurnOff()
	SetTemperature()
}

func main() {
	airConditioner := NewAirConditionerProxy(false)
	airConditioner.TurnOn()
	airConditioner.TurnOff()
	airConditioner.SetTemperature(25)

	airConditioner = NewAirConditionerProxy(true)
	airConditioner.TurnOn()
	airConditioner.TurnOff()
	airConditioner.SetTemperature(25)
}
