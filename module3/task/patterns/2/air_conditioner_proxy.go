package main

import (
	"fmt"
	"os"
	"time"
)

type AirConditionerProxy struct {
	adapter       *AirConditionerAdapter
	authenticated bool
	logs          *os.File
}

func NewAirConditionerProxy(authenticated bool) *AirConditionerProxy {
	logs, err := os.OpenFile("/home/alex/go/src/AlexVuT/go-kata/module3/task/patterns/2/logs.txt", os.O_WRONLY|os.O_APPEND|os.O_CREATE, 0644)
	if err != nil {
		panic(err)
	}
	return &AirConditionerProxy{adapter: NewAirConditionerAdapter(), authenticated: authenticated, logs: logs}
}

func (a *AirConditionerProxy) TurnOn() {
	if a.authenticated == true {
		a.logs.WriteString(fmt.Sprintf("Access to turning on was granted at %s\n", time.Now()))
		a.adapter.TurnOn()
	} else {
		fmt.Println("Access to turning on is denied")
		a.logs.WriteString(fmt.Sprintf("Access to turning on was denied at %s\n", time.Now()))
	}
}

func (a *AirConditionerProxy) TurnOff() {
	if a.authenticated == true {
		a.logs.WriteString(fmt.Sprintf("Access to turning off was granted at %s\n", time.Now()))
		a.adapter.TurnOff()
	} else {
		fmt.Println("Access to turning off is denied")
		a.logs.WriteString(fmt.Sprintf("Access to turning off was denied at %s\n", time.Now()))
	}
}

func (a *AirConditionerProxy) SetTemperature(degrees int) {
	if a.authenticated == true {
		a.logs.WriteString(fmt.Sprintf("Temperature was set to %d Celsius degrees at %s\n", degrees, time.Now()))
		a.adapter.SetTemperature(degrees)
	} else {
		fmt.Println("Access to setting temperature is denied")
		a.logs.WriteString(fmt.Sprintf("Access to setting temperature was denied at %s\n", time.Now()))
	}
}
