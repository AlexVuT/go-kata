package main

import (
	"reflect"
	"testing"
)

func Test_DoubleLinkedList_Len(t *testing.T) {
	type fields struct {
		head *Node
		tail *Node
		curr *Node
		len  int
	}
	tests := []struct {
		name   string
		fields fields
		want   int
	}{
		{
			name: "Get len when empty",
			fields: fields{
				len: 0,
			},
			want: 0,
		},
		{
			name: "Get len when not empty",
			fields: fields{
				len: 120,
			},
			want: 120,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}
			if got := d.Len(); got != tt.want {
				t.Errorf("Len() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_DoubleLinkedList_Current(t *testing.T) {
	type fields struct {
		head *Node
		tail *Node
		curr *Node
		len  int
	}

	first := Node{prev: nil}
	second := Node{}
	third := Node{next: nil}
	first.next = &second
	second.prev = &first
	second.next = &third
	third.prev = &second

	tests := []struct {
		name   string
		fields fields
		want   *Node
	}{
		{
			name: "When empty",
			fields: fields{
				head: nil,
				tail: nil,
				curr: nil,
				len:  0,
			},
			want: nil,
		},
		{

			name: "When current is first",
			fields: fields{
				head: &first,
				tail: &third,
				curr: &first,
			},
			want: &first,
		},
		{
			name: "When current is last",
			fields: fields{
				head: &first,
				tail: &third,
				curr: &third,
			},
			want: &third,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}
			if got := d.Current(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Current() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_DoubleLinkedList_Next(t *testing.T) {
	type fields struct {
		head *Node
		tail *Node
		curr *Node
		len  int
	}

	first := Node{prev: nil}
	second := Node{}
	third := Node{next: nil}
	first.next = &second
	second.prev = &first
	second.next = &third
	third.prev = &second

	tests := []struct {
		name   string
		fields fields
		want   *Node
	}{
		{

			name: "When current is first",
			fields: fields{
				head: &first,
				tail: &third,
				curr: &first,
			},
			want: &second,
		},
		{
			name: "When current is second",
			fields: fields{
				head: &first,
				tail: &third,
				curr: &second,
			},
			want: &third,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}
			if got := d.Next(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Next() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_DoubleLinkedList_Prev(t *testing.T) {
	type fields struct {
		head *Node
		tail *Node
		curr *Node
		len  int
	}

	first := Node{prev: nil}
	second := Node{}
	third := Node{next: nil}
	first.next = &second
	second.prev = &first
	second.next = &third
	third.prev = &second

	tests := []struct {
		name   string
		fields fields
		want   *Node
	}{
		{

			name: "When current is second",
			fields: fields{
				head: &first,
				tail: &third,
				curr: &second,
			},
			want: &first,
		},
		{
			name: "When current is last",
			fields: fields{
				head: &first,
				tail: &third,
				curr: &third,
			},
			want: &second,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}
			if got := d.Prev(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Prev() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_DoubleLinkedList_Delete(t *testing.T) {
	type args struct {
		n int
	}

	type fields struct {
		head *Node
		tail *Node
		curr *Node
		len  int
	}

	m := &DoubleLinkedList{}
	m.LoadData("/home/alex/go/src/AlexVuT/go-kata/module3/task/test.json")
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
		wantLen int
	}{
		{
			name: "Delete first element",
			fields: fields{
				head: m.head,
				tail: m.tail,
				curr: m.curr,
				len:  m.len,
			},
			args:    args{1},
			wantErr: false,
			wantLen: m.len - 1,
		},

		{
			name: "Delete last element",
			fields: fields{
				head: m.head,
				tail: m.tail,
				curr: m.curr,
				len:  m.len,
			},
			args:    args{5},
			wantErr: false,
			wantLen: m.len - 1,
		},

		{
			name: "Delete third element",
			fields: fields{
				head: m.head,
				tail: m.tail,
				curr: m.curr,
				len:  m.len,
			},
			args:    args{3},
			wantErr: false,
			wantLen: m.len - 1,
		},

		{
			name:    "Delete when empty",
			fields:  fields{len: 0},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}
			if err := d.Delete(tt.args.n); (err != nil) != tt.wantErr {
				t.Errorf("Delete() error = %v, wantErr %v", err, tt.wantErr)
			} else if d.len != tt.wantLen {
				t.Errorf("Delete() len = %v, wantLen %v", d.len, tt.wantLen)
			}

		})
	}
}

func Test_DoubleLinkedList_DeleteCurrent(t *testing.T) {

	type fields struct {
		head *Node
		tail *Node
		curr *Node
		len  int
	}

	m := &DoubleLinkedList{}
	m.LoadData("/home/alex/go/src/AlexVuT/go-kata/module3/task/test.json")
	tests := []struct {
		name    string
		fields  fields
		wantErr bool
		wantLen int
	}{
		{
			name: "Delete when empty",
			fields: fields{
				len: 0,
			},
			wantErr: true,
		},

		{
			name: "Delete when not empty",
			fields: fields{
				head: m.head,
				tail: m.tail,
				curr: m.curr,
				len:  m.len,
			},
			wantErr: false,
			wantLen: m.len - 1,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}
			if err := d.DeleteCurrent(); (err != nil) != tt.wantErr {
				t.Errorf("DeleteCurrent() error = %v, wantErr %v", err, tt.wantErr)
			}
			if d.len != tt.wantLen {
				t.Errorf("DeleteCurrent() len = %v, wantLen %v", d.len, tt.wantLen)
			}

		})
	}
}

func Test_DoubleLinkedList_Insert(t *testing.T) {
	type args struct {
		n int
		c Commit
	}

	type fields struct {
		head *Node
		tail *Node
		curr *Node
		len  int
	}

	m := &DoubleLinkedList{}
	m.LoadData("/home/alex/go/src/AlexVuT/go-kata/module3/task/test.json")
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
		wantLen int
	}{
		{
			name: "Insert before first element",
			fields: fields{
				head: m.head,
				tail: m.tail,
				curr: m.curr,
				len:  m.len,
			},
			args: args{
				0, Commit{},
			},
			wantErr: false,
			wantLen: m.len + 1,
		},

		{
			name: "Insert after last element",
			fields: fields{
				head: m.head,
				tail: m.tail,
				curr: m.curr,
				len:  m.len,
			},
			args: args{
				5, Commit{},
			},
			wantErr: false,
			wantLen: m.len + 1,
		},

		{
			name: "Insert after first element",
			fields: fields{
				head: m.head,
				tail: m.tail,
				curr: m.curr,
				len:  m.len,
			},
			args: args{
				1, Commit{},
			},
			wantErr: false,
			wantLen: m.len + 1,
		},

		{
			name: "Insert out of list range",
			fields: fields{
				head: m.head,
				tail: m.tail,
				curr: m.curr,
				len:  m.len,
			},
			args: args{
				147, Commit{},
			},
			wantErr: true,
			wantLen: m.len,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}
			if err := d.Insert(tt.args.n, tt.args.c); (err != nil) != tt.wantErr {
				t.Errorf("Insert() error = %v, wantErr %v", err, tt.wantErr)
			}
			if d.len != tt.wantLen {
				t.Errorf("Insert() len = %v, wantLen %v", d.len, tt.wantLen)
			}

		})
	}
}

func Test_DoubleLinkedList_Index(t *testing.T) {
	type fields struct {
		head *Node
		tail *Node
		curr *Node
		len  int
	}

	first := Node{prev: nil}
	second := Node{}
	third := Node{next: nil}
	first.next = &second
	second.prev = &first
	second.next = &third
	third.prev = &second

	tests := []struct {
		name    string
		fields  fields
		want    int
		wantErr bool
	}{
		{
			name: "When list is empty",
			fields: fields{
				head: nil,
				tail: nil,
				curr: nil,
				len:  0,
			},
			wantErr: true,
		},
		{

			name: "When current is first",
			fields: fields{
				head: &first,
				tail: &third,
				curr: &first,
			},
			want: 1,
		},
		{
			name: "When current is last",
			fields: fields{
				head: &first,
				tail: &third,
				curr: &third,
			},
			want: 3,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}

			if _, err := d.Index(); (err != nil) != tt.wantErr {
				t.Errorf("Index() error = %v, wantErr %v", err, tt.wantErr)
			}

			if got, _ := d.Index(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Index() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_DoubleLinkedList_SearchByIndex(t *testing.T) {
	type fields struct {
		head *Node
		tail *Node
		curr *Node
		len  int
	}
	type args struct {
		n int
	}

	m := &DoubleLinkedList{}
	m.LoadData("/home/alex/go/src/AlexVuT/go-kata/module3/task/test.json")

	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *Node
		wantErr bool
	}{
		{
			name: "When list is empty",
			fields: fields{
				head: nil,
				tail: nil,
				curr: nil,
				len:  0,
			},
			args:    args{2},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Search in range",
			fields: fields{
				head: m.head,
				tail: m.tail,
				len:  m.len,
			},
			args:    args{3},
			want:    m.head.next.next,
			wantErr: false,
		},

		{
			name: "Search out of range",
			fields: fields{
				head: m.head,
				tail: m.tail,
				len:  m.len,
			},
			args:    args{54},
			want:    nil,
			wantErr: true,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}

			if _, err := d.SearchByIndex(tt.args.n); (err != nil) != tt.wantErr {
				t.Errorf("SearchByIndex() error = %v, wantErr %v", err, tt.wantErr)
			}

			if got, _ := d.SearchByIndex(tt.args.n); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("SearchByIndex() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_DoubleLinkedList_Pop(t *testing.T) {
	type fields struct {
		head *Node
		tail *Node
		curr *Node
		len  int
	}

	m := &DoubleLinkedList{}
	m.LoadData("/home/alex/go/src/AlexVuT/go-kata/module3/task/test.json")

	tests := []struct {
		name    string
		fields  fields
		want    *Node
		wantLen int
		wantErr bool
	}{
		{
			name: "When list is empty",
			fields: fields{
				head: nil,
				tail: nil,
				curr: nil,
				len:  0,
			},
			want:    nil,
			wantErr: true,
			wantLen: 0,
		},
		{
			name: "When list is not empty",
			fields: fields{
				head: m.head,
				tail: m.tail,
				len:  m.len,
			},

			want:    m.tail,
			wantErr: false,
			wantLen: m.len - 1,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}

			if got, err := d.Pop(); (err != nil) != tt.wantErr {
				t.Errorf("Pop() error = %v, wantErr %v", err, tt.wantErr)
			} else if !reflect.DeepEqual(d.len, tt.wantLen) {
				t.Errorf("Pop() = %v, want %v", d.len, tt.wantLen)
			} else if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Pop() = %v, want %v", got, tt.want)
			}

		})
	}
}

func Test_DoubleLinkedList_Shift(t *testing.T) {
	type fields struct {
		head *Node
		tail *Node
		curr *Node
		len  int
	}

	m := &DoubleLinkedList{}
	m.LoadData("/home/alex/go/src/AlexVuT/go-kata/module3/task/test.json")

	tests := []struct {
		name    string
		fields  fields
		want    *Node
		wantLen int
		wantErr bool
	}{
		{
			name: "When list is empty",
			fields: fields{
				head: nil,
				tail: nil,
				curr: nil,
				len:  0,
			},
			want:    nil,
			wantErr: true,
			wantLen: 0,
		},
		{
			name: "When list is not empty",
			fields: fields{
				head: m.head,
				tail: m.tail,
				len:  m.len,
			},

			want:    m.head,
			wantErr: false,
			wantLen: m.len - 1,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}

			if got, err := d.Shift(); (err != nil) != tt.wantErr {
				t.Errorf("Shift() error = %v, wantErr %v", err, tt.wantErr)
			} else if !reflect.DeepEqual(d.len, tt.wantLen) {
				t.Errorf("Shift() = %v, want %v", d.len, tt.wantLen)
			} else if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Shift() = %v, want %v", got, tt.want)
			}

		})
	}
}

func Test_DoubleLinkedList_SearchUUID(t *testing.T) {
	type fields struct {
		head *Node
		tail *Node
		curr *Node
		len  int
	}
	type args struct {
		text string
	}

	m := &DoubleLinkedList{}
	m.LoadData("/home/alex/go/src/AlexVuT/go-kata/module3/task/test.json")

	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *Node
		wantErr bool
	}{
		{
			name: "When list is empty",
			fields: fields{
				head: nil,
				tail: nil,
				curr: nil,
				len:  0,
			},
			args:    args{m.head.data.UUID},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Searching first element",
			fields: fields{
				head: m.head,
				tail: m.tail,
				len:  m.len,
			},
			args:    args{m.head.data.UUID},
			want:    m.head,
			wantErr: false,
		},
		{
			name: "Searching last element",
			fields: fields{
				head: m.head,
				tail: m.tail,
				len:  m.len,
			},
			args:    args{m.tail.data.UUID},
			want:    m.tail,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}

			if got, err := d.SearchUUID(tt.args.text); (err != nil) != tt.wantErr {
				t.Errorf("SearchUUID() error = %v, wantErr %v", err, tt.wantErr)
			} else if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("SearchUUID() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_DoubleLinkedList_Reverse(t *testing.T) {
	type fields struct {
		head *Node
		tail *Node
		curr *Node
		len  int
	}
	first := Node{data: &Commit{Message: "ape"}}
	second := Node{data: &Commit{Message: "elephant"}}
	third := Node{data: &Commit{Message: "tiger"}}
	first.next = &second
	second.prev = &first
	second.next = &third
	third.prev = &second

	tests := []struct {
		name   string
		fields fields
		want   *DoubleLinkedList
	}{

		{
			name: "Reverse sorting",
			fields: fields{
				head: &first,
				tail: &third,
				len:  3,
			},
			want: &DoubleLinkedList{},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}

			if got := d.Reverse(); !reflect.DeepEqual(got.head.data, d.tail.data) {
				t.Errorf("Reverse() = %v, want %v", got.head.data, d.tail.data)
			} else if !reflect.DeepEqual(got.tail.data, d.head.data) {
				t.Errorf("Reverse() = %v, want %v", got.tail.data, d.head.data)
			} else if reflect.DeepEqual(got.head.next, d.tail.prev) {
				t.Errorf("Reverse() = %v, want %v", got.head.next.next, d.tail.prev.prev)
			}
		})
	}
}
