package main

import (
	"fmt"
)

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func removeLeafNodes(t *TreeNode, target int) *TreeNode {
	if target < 1 || target > 1000 || t.Val < 1 || t.Val > 1000 {
		fmt.Println("Incorrect input")
		return nil
	}

	if t.Left != nil {
		removeLeafNodes(t.Left, target)
	}
	if t.Right != nil {
		removeLeafNodes(t.Right, target)
	}
	if t.Left != nil && t.Left.Left == nil && t.Left.Right == nil && t.Left.Val == target {
		t.Left = nil
	}
	if t.Right != nil && t.Right.Left == nil && t.Right.Right == nil && t.Right.Val == target {
		t.Right = nil
	}
	if t.Left == nil && t.Right == nil && t.Val == target {
		t = nil
	}

	return t
}

//func main() {
//	slice := []string{"1", "3", "3", "3", "2"}
//	tree := treeFromArray(slice)
//	tree = removeLeafNodes(tree, 3)
//	fmt.Println(tree.Left)
//
//	slice1 := []string{"1", "2", "null", "2", "null", "2"}
//	tree1 := treeFromArray(slice1)
//	tree1 = removeLeafNodes(tree1, 2)
//	fmt.Println(tree1)
//
//	slice2 := []string{"1", "2", "3", "2", "null", "2", "4"}
//	tree2 := treeFromArray(slice2)
//	tree2 = removeLeafNodes(tree2, 2)
//	fmt.Println(tree2.Right)
//}
//
//func treeFromArray(data []string) *TreeNode {
//	if len(data) < 1 {
//		fmt.Println("Empty array")
//		return nil
//	}
//	node := createNode(data[0])
//	var index int
//	line := make([]*TreeNode, 0)
//	line = append(line, node)
//	index = 1
//
//	for index < len(data) {
//		newLine := make([]*TreeNode, 0)
//		for i := 0; i < len(line); i++ {
//			if line[i] != nil {
//				if index >= len(data) {
//					break
//				}
//				line[i].Left = createNode(data[index])
//				newLine = append(newLine, line[i].Left)
//				index++
//				if index >= len(data) {
//					break
//				}
//				line[i].Right = createNode(data[index])
//				newLine = append(newLine, line[i].Right)
//				index++
//			}
//		}
//		line = newLine
//	}
//	return node
//}
//
//func createNode(text string) *TreeNode {
//	if text == "null" {
//		return nil
//	}
//	n, err := strconv.Atoi(text)
//	if err != nil {
//		panic(errors.New("incorrect value in array"))
//	}
//	return &TreeNode{Val: n}
//}
