package main

import "fmt"

func countPoints(points [][]int, queries [][]int) []int {
	if len(points) < 1 || len(points) > 500 {
		fmt.Println("Incorrect length")
	}
	result := make([]int, 0)

	for _, query := range queries {
		x, y, r := query[0], query[1], query[2]
		if x < 0 || x > 500 || y < 0 || y > 500 || r < 1 || r > 500 {
			fmt.Println("Incorrect elements")
			return nil
		}
		count := 0

		for _, point := range points {
			if len(point) != 2 {
				fmt.Println("Incorrect point length")
				return nil
			}
			px, py := point[0], point[1]
			if px < 0 || px > 500 || py < 0 || py > 500 {
				fmt.Println("Incorrect elements")
				return nil
			}
			if (px-x)*(px-x)+(py-y)*(py-y) <= r*r {
				count++
			}
		}
		result = append(result, count)
	}

	return result
}

//func main() {
//	points := [][]int{{1, 3}, {3, 3}, {5, 3}, {2, 2}}
//	queries := [][]int{{2, 3, 1}, {4, 3, 1}, {1, 1, 2}}
//	count := countPoints(points, queries)
//	fmt.Println(count)
//
//	points1 := [][]int{{1, 1}, {2, 2}, {3, 3}, {4, 4}, {5, 5}}
//	queries1 := [][]int{{1, 2, 2}, {2, 2, 2}, {4, 3, 2}, {4, 3, 3}}
//	count1 := countPoints(points1, queries1)
//	fmt.Println(count1)
//}
