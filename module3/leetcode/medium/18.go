package main

func checkUnique(nums [][]int, target int) bool {
	for _, row := range nums {
		for _, num := range row {
			if num == target {
				return false
			}
		}
	}
	return true
}

func groupThePeople(groupSizes []int) [][]int {
	res := make([][]int, 0)
	for i := 0; i < len(groupSizes); i++ {
		group := make([]int, 0)
		if checkUnique(res, i) == false {
			continue
		}
		group = append(group, i)
		for r := 0; r < len(groupSizes); r++ {
			if checkUnique(res, r) == false {
				continue
			}
			if groupSizes[i] == groupSizes[r] && i != r {
				if len(group) < groupSizes[i] {
					group = append(group, r)
				}
			}
		}

		res = append(res, group)

	}
	return res
}

//func main() {
//	slice := []int{3, 3, 3, 3, 3, 1, 3}
//	fmt.Println(groupThePeople(slice))
//
//	slice1 := []int{2, 1, 3, 3, 3, 2}
//	fmt.Println(groupThePeople(slice1))
//}
