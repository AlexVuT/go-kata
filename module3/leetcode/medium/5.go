package main

import "fmt"

type ListNode struct {
	Val  int
	Next *ListNode
}

func pairSum(list *ListNode) int {
	if list == nil {
		fmt.Println("Empty list")
		return 0
	}
	var result int
	nums := make([]int, 0)
	curr := list
	for {
		nums = append(nums, curr.Val)
		if curr.Next != nil {
			curr = curr.Next
		} else {
			break
		}
	}
	for i := 0; i < len(nums)/2; i++ {
		if result < nums[i]+nums[len(nums)-1-i] {
			result = nums[i] + nums[len(nums)-1-i]
		}
	}

	return result
}

//func fromArrayToNode(nums []int) *ListNode {
//	if len(nums)%2 != 0 {
//		fmt.Println("Incorrect array's length")
//		return nil
//	}
//	var start, curr *ListNode
//	start = &ListNode{
//		Val:  nums[0],
//		Next: nil,
//	}
//	curr = start
//	for i := 1; i < len(nums); i++ {
//		node := &ListNode{
//			Val:  nums[i],
//			Next: nil,
//		}
//		curr.Next = node
//		curr = curr.Next
//
//	}
//	return start
//}
//
//func main() {
//	slice := []int{5, 4, 2, 1}
//	node := fromArrayToNode(slice)
//	fmt.Println(pairSum(node))
//
//	slice1 := []int{4, 2, 2, 3}
//	node1 := fromArrayToNode(slice1)
//	fmt.Println(pairSum(node1))
//
//	slice2 := []int{1, 1000}
//	node2 := fromArrayToNode(slice2)
//	fmt.Println(pairSum(node2))
//
//	slice3 := []int{1, 1000, 3}
//	node3 := fromArrayToNode(slice3)
//	fmt.Println(pairSum(node3))
//}
