package main

import "fmt"

func findSmallestSetOfVertices(n int, vertices [][]int) []int {
	if n < 2 || n > 100000 || len(vertices) < 1 {
		fmt.Println("Incorrect input")
		return nil
	}
	result := make([]int, 0)
	reachable := make([]int, n)
	check := make([]int, n)
	for _, e := range vertices {
		if e[0] == e[1] || e[0] < 0 || e[1] < 0 || e[0] >= n || e[1] >= n {
			fmt.Println("Incorrect input")
			return nil
		}
		if len(e) != 2 {
			fmt.Println("Incorrect length")
			return nil
		}
		check[e[0]]++
		check[e[1]]++
		reachable[e[1]]++
	}
	for i := 0; i < n; i++ {
		if check[i] == 0 {
			fmt.Println("Lack of vertices")
			return nil
		}
	}

	for i := 0; i < n; i++ {
		if reachable[i] == 0 {
			result = append(result, i)
		}
	}
	return result
}

//func main() {
//	vertices := [][]int{{0, 1}, {0, 2}, {2, 5}, {3, 4}, {4, 2}}
//	fmt.Println(findSmallestSetOfVertices(6, vertices))
//
//	vertices1 := [][]int{{0, 1}, {2, 1}, {3, 1}, {1, 4}, {2, 4}}
//	fmt.Println(findSmallestSetOfVertices(5, vertices1))
//
//	vertices2 := [][]int{{0, 1}, {2, 1}, {4, 1}, {1, 4}, {2, 4}}
//	fmt.Println(findSmallestSetOfVertices(5, vertices2))
//}
