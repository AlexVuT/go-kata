package main

import (
	"fmt"
	"strconv"
)

func minPartitions(n string) int {
	if len(n) < 1 || len(n) > 100000 {
		fmt.Println("Incorrect input")
		return 0
	}
	if n[0] == '0' {
		fmt.Println("Leading zero")
		return 0
	}
	var result int
	for i := 0; i < len(n); i++ {
		if n[i] >= '0' && n[i] <= '9' {
		} else {
			fmt.Println("Incorrect element")
			return 0
		}
		temp, _ := strconv.Atoi(string(n[i]))
		if temp > result {
			result = temp
		}
	}
	return result
}

//func main() {
//	num := "32"
//	fmt.Println(minPartitions(num))
//
//	num1 := "82734"
//	fmt.Println(minPartitions(num1))
//
//	num2 := "27346209830709182346"
//	fmt.Println(minPartitions(num2))
//}
