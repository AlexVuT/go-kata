package main

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func countSubtree(root *TreeNode, count *int) (int, int) {
	if root == nil {
		return 0, 0
	}

	leftSum, leftCount := countSubtree(root.Left, count)
	rightSum, rightCount := countSubtree(root.Right, count)

	sum := leftSum + rightSum + root.Val
	nodeCount := leftCount + rightCount + 1

	if sum/nodeCount == root.Val {
		*count++
	}
	return sum, nodeCount
}

func averageOfSubtree(root *TreeNode) int {
	count := 0
	countSubtree(root, &count)
	return count
}
