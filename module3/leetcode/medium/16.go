package main

import (
	"errors"
	"fmt"
)

type SubrectangleQueries struct {
	rectangle [][]int
	rows      int
	cols      int
}

func Constructor(nums [][]int) SubrectangleQueries {
	if len(nums) < 1 || len(nums) > 100 {
		fmt.Println("Incorrect number of rows")
		return SubrectangleQueries{}
	}
	check := len(nums[0])
	for _, v := range nums {
		if len(v) < 1 || len(v) > 100 || len(v) != check {
			fmt.Println("Incorrect number of cols")
			return SubrectangleQueries{}
		}
	}

	s := SubrectangleQueries{rectangle: nums,
		rows: len(nums),
		cols: check,
	}
	return s
}

func (s *SubrectangleQueries) UpdateSubrectangle(row1 int, col1 int, row2 int, col2 int, newValue int) {
	if row1 < 0 || row1 > row2 || row2 > s.rows || col1 < 0 || col1 > col2 || col2 > s.cols || newValue < 1 {
		panic(errors.New("incorrect input"))
	}
	for i := row1; i <= row2; i++ {
		for r := col1; r <= col2; r++ {
			s.rectangle[i][r] = newValue
		}
	}
}

func (s SubrectangleQueries) GetValue(row int, col int) int {
	return s.rectangle[row][col]
}
