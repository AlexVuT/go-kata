package main

import (
	"errors"
	"fmt"
	"strconv"
)

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func lengthOfBinaryTree(root *TreeNode) int {
	if root == nil {
		return 0
	}
	leftLength := lengthOfBinaryTree(root.Left)
	rightLength := lengthOfBinaryTree(root.Right)
	return max(leftLength, rightLength) + 1
}

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}

func deepestLeavesSum(root *TreeNode) int {
	if root == nil {
		return 0
	}
	queue := []*TreeNode{root}
	var sum int
	for len(queue) > 0 {
		size := len(queue)
		sum = 0
		for i := 0; i < size; i++ {
			node := queue[0]
			queue = queue[1:]
			sum += node.Val
			if node.Left != nil {
				queue = append(queue, node.Left)
			}
			if node.Right != nil {
				queue = append(queue, node.Right)
			}
		}
	}
	return sum
}

func treeFromArray(data []string) *TreeNode {
	if len(data) < 1 {
		fmt.Println("Empty array")
		return nil
	}
	node := createNode(data[0])
	var index int
	line := make([]*TreeNode, 0)
	line = append(line, node)
	index = 1

	for index < len(data) {
		newLine := make([]*TreeNode, 0)
		for i := 0; i < len(line); i++ {
			if line[i] != nil {
				if index >= len(data) {
					break
				}
				line[i].Left = createNode(data[index])
				newLine = append(newLine, line[i].Left)
				index++
				if index >= len(data) {
					break
				}
				line[i].Right = createNode(data[index])
				newLine = append(newLine, line[i].Right)
				index++
			}
		}
		line = newLine
	}
	return node
}

func createNode(text string) *TreeNode {
	if text == "null" {
		return nil
	}
	n, err := strconv.Atoi(text)
	if err != nil {
		panic(errors.New("incorrect value in array"))
	}
	return &TreeNode{Val: n}
}

func main() {
	tree := &TreeNode{
		Val: 1,
		Left: &TreeNode{
			Val: 2,
			Left: &TreeNode{
				Val: 5,
			},
			Right: &TreeNode{Val: 6,
				Left: &TreeNode{Val: 11},
			},
		},
		Right: &TreeNode{
			Val: 3,
			Left: &TreeNode{Val: 9,
				Right: &TreeNode{Val: 7},
			},
		},
	}
	fmt.Println(lengthOfBinaryTree(tree))
	fmt.Println(deepestLeavesSum(tree))

	slice := []string{"1", "2", "3", "4", "5", "null", "6", "7", "null", "null", "null", "null", "8"}
	node := treeFromArray(slice)
	fmt.Println(lengthOfBinaryTree(node))
	fmt.Println(deepestLeavesSum(node))

	slice1 := []string{"6", "7", "8", "2", "7", "1", "3", "9", "null", "1", "4", "null", "null", "null", "5"}
	node1 := treeFromArray(slice1)
	fmt.Println(lengthOfBinaryTree(node1))
	fmt.Println(deepestLeavesSum(node1))
}
