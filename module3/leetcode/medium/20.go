package main

func processQueries(queries []int, m int) []int {
	result := make([]int, 0)
	perm := make([]int, 0)
	for i, t := 0, 1; i < m; i++ {
		perm = append(perm, t)
		t++
	}

	for r := 0; r < len(queries); r++ {
		for j := 0; j < len(perm); j++ {
			if queries[r] == perm[j] {
				result = append(result, j)

				perm = append([]int{queries[r]}, append(perm[:j], perm[j+1:]...)...)
				break
			}
		}
	}
	return result
}
