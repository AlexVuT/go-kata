package main

import (
	"fmt"
	"sort"
)

func checkDigits(nums []int) bool {
	for iter := 0; iter < len(nums); iter++ {
		if nums[iter] <= 0 || nums[iter] > 100000 {
			return false
		}
		for i := 0 + iter; i < len(nums); i++ {
			if nums[iter] == nums[i] && iter != i {
				return false
			}
		}
	}
	return true
}

func sortStudents(students [][]int, exam int) [][]int {
	if len(students) < 1 || len(students) > 250 || exam < 0 || exam >= len(students[0]) {
		fmt.Println("Incorrect input")
		return nil
	}

	for i := 0; i < len(students); i++ {
		for r := 0 + i; r < len(students); r++ {
			if len(students[i]) != len(students[r]) {
				fmt.Println("Length of arrays is not equal")
				return nil
			}
		}
	}

	temp := make([]int, 0)
	for _, v := range students {
		temp = append(temp, v...)
	}
	if checkDigits(temp) == false {
		fmt.Println("Digits do not meet the requirements")
		return nil
	}

	sort.Slice(students, func(i, j int) bool {
		return students[i][exam] > students[j][exam]
	})
	return students
}

func main() {
	students := [][]int{{10, 6, 9, 1}, {7, 5, 11, 2}, {4, 8, 3, 15}}
	fmt.Println(students)
	sortStudents(students, 2)
	fmt.Println(students)

	students1 := [][]int{{3, 4}, {5, 6}}
	fmt.Println(students1)
	sortStudents(students1, 0)
	fmt.Println(students1)

	students2 := [][]int{{3, 4}, {5, 6}, {1, 6}}
	fmt.Println(students2)
	sortStudents(students2, 1)
}
