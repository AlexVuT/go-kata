package main

import "fmt"

func maxSum(data [][]int) int {
	if len(data) < 3 || len(data) > 150 {
		fmt.Println("Incorrect length")
		return 0
	}
	for _, k := range data {
		for _, v := range k {
			if v < 0 || v > 1000000 {
				fmt.Println("Incorrect elements")
				return 0
			}
		}
	}

	var result, temp int
	for i := 0; i <= len(data)-3; i++ {
		for r := 0; r <= len(data[i])-3; r++ {
			temp = data[i][r] + data[i][r+1] + data[i][r+2] +
				data[i+1][r+1] +
				data[i+2][r] + data[i+2][r+1] + data[i+2][r+2]
			if temp > result {
				result = temp
			}
		}
	}
	return result
}

//func main() {
//	data := [][]int{{6, 2, 1, 3}, {4, 2, 1, 5}, {9, 2, 8, 7}, {4, 1, 2, 9}}
//	fmt.Println(maxSum(data))
//
//	data1 := [][]int{{1, 2, 3}, {4, 5, 6}, {7, 8, 9}}
//	fmt.Println(maxSum(data1))
//}
