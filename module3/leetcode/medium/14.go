package main

import "fmt"

func xorQueries(nums []int, queries [][]int) []int {
	result := make([]int, 0)
	for i := 0; i < len(queries); i++ {
		if len(queries[i]) != 2 {
			fmt.Println("Incorrect length")
			return nil
		}
		if queries[i][0] > queries[i][1] || queries[i][0] < 0 || queries[i][1] < 0 ||
			queries[i][0] >= len(nums) || queries[i][1] >= len(nums) {
			fmt.Println("Incorrect elements")
			return nil
		}
		if queries[i][0] == queries[i][1] {
			result = append(result, nums[queries[i][1]])
			continue
		}
		temp := 0
		for r := queries[i][0]; r <= queries[i][1]; r++ {
			temp ^= nums[r]

		}
		result = append(result, temp)
	}

	return result
}

//func main() {
//	slice := []int{1, 3, 4, 8}
//	queries := [][]int{{0, 1}, {1, 2}, {0, 3}, {3, 3}}
//	fmt.Println(xorQueries(slice, queries))
//
//	slice1 := []int{4, 8, 2, 10}
//	queries1 := [][]int{{2, 3}, {1, 3}, {0, 0}, {0, 3}}
//	fmt.Println(xorQueries(slice1, queries1))
//}
