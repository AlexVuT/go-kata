package main

import "fmt"

func numTilePossibilities(tiles string) int {
	if len(tiles) < 1 || len(tiles) > 7 {
		fmt.Println("Incorrect length")
		return 0
	}
	letters := make(map[byte]int)
	for i := 0; i < len(tiles); i++ {
		if tiles[i] >= 'A' && tiles[i] <= 'Z' {
		} else {
			fmt.Println("Incorrect letter")
			return 0
		}
		letters[tiles[i]]++
	}
	return dfs(letters)
}

func dfs(letters map[byte]int) int {
	count := 0
	for ch, f := range letters {
		if f > 0 {
			count++
			letters[ch]--
			count += dfs(letters)
			letters[ch]++
		}
	}
	return count
}

//func main() {
//	tiles := "AAB"
//	fmt.Println(numTilePossibilities(tiles))
//
//	tiles1 := "AAABBC"
//	fmt.Println(numTilePossibilities(tiles1))
//}
