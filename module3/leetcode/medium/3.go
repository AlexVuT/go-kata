package main

import "fmt"

type ListNode struct {
	Val  int
	Next *ListNode
}

func mergeNodes(head *ListNode) *ListNode {
	var temp int
	result := make([]int, 0)

	for head != nil {
		if head.Val == 0 && head.Next != nil {
			if head.Next.Val == 0 {
				fmt.Println("Two consequent zeros")
				return nil
			}
		}

		if head.Val == 0 && temp != 0 {
			result = append(result, temp)
			temp = 0
		}
		temp += head.Val
		head = head.Next
	}
	list := fromArrayToNode(result)
	return list
}

func fromArrayToNode(nums []int) *ListNode {
	var start, curr *ListNode
	start = &ListNode{
		Val:  nums[0],
		Next: nil,
	}
	curr = start
	for i := 1; i < len(nums); i++ {
		node := &ListNode{
			Val:  nums[i],
			Next: nil,
		}
		curr.Next = node
		curr = curr.Next

	}
	return start
}

func main() {

}
