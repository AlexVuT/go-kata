package main

import (
	"fmt"
	"sort"
)

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func balanceBST(root *TreeNode) *TreeNode {
	nums := bstValuesToArray(root)
	res := balance(nums)
	return res
}

func bstValuesToArray(t *TreeNode) []int {
	result := make([]int, 0)
	left := make([]int, 0)
	right := make([]int, 0)
	result = append(result, t.Val)
	if t.Left != nil {
		left = bstValuesToArray(t.Left)
	}
	if t.Right != nil {
		right = bstValuesToArray(t.Right)
	}
	result = append(result, append(left, right...)...)
	sort.Slice(result, func(i, j int) bool {
		return result[i] < result[j]
	})
	return result
}

func balance(nums []int) *TreeNode {
	t := &TreeNode{}
	if len(nums) == 1 {
		t.Val = nums[0]
		return t
	}
	var median int
	left := make([]int, 0)
	right := make([]int, 0)

	for i := range nums {
		if nums[i] < 1 || nums[i] > 100000 {
			fmt.Println("Incorrect elements")
			return nil
		}
	}

	if len(nums)%2 == 0 {
		median = nums[len(nums)/2]
	} else {
		median = nums[(len(nums)-1)/2]
	}

	t.Val = median
	for i := range nums {
		if median == nums[i] {
			if i != 0 {
				left = nums[:i]
				t.Left = balance(left)
			}
			if i != len(nums)-1 {
				right = nums[i+1:]
				t.Right = balance(right)
			}
		}
	}

	return t
}

//func main() {
//
//}
//
//func arrayToBinaryTree(data []string) *TreeNode {
//	if len(data) < 1 {
//		fmt.Println("Empty array")
//		return nil
//	}
//	node := createNode(data[0])
//	var index int
//	line := make([]*TreeNode, 0)
//	line = append(line, node)
//	index = 1
//
//	for index < len(data) {
//		newLine := make([]*TreeNode, 0)
//		for i := 0; i < len(line); i++ {
//			if line[i] != nil {
//				if index >= len(data) {
//					break
//				}
//				line[i].Left = createNode(data[index])
//				newLine = append(newLine, line[i].Left)
//				index++
//				if index >= len(data) {
//					break
//				}
//				line[i].Right = createNode(data[index])
//				newLine = append(newLine, line[i].Right)
//				index++
//				if line[i].Left != nil {
//					if line[i].Val <= line[i].Left.Val {
//						fmt.Println("Incorrect binary tree. Left branch")
//						return nil
//					}
//				}
//				if line[i].Right != nil {
//					if line[i].Val >= line[i].Right.Val {
//						fmt.Println("Incorrect binary tree. Right branch")
//						return nil
//					}
//				}
//			}
//		}
//		line = newLine
//	}
//	return node
//}
//
//func createNode(text string) *TreeNode {
//	if text == "null" {
//		return nil
//	}
//	n, err := strconv.Atoi(text)
//	if err != nil {
//		panic(errors.New("incorrect value in array"))
//	}
//	return &TreeNode{Val: n}
//}
