package main

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func bstToGst(root *TreeNode) *TreeNode {
	sum := 0
	depthFirstSearch(root, &sum)
	return root
}

func depthFirstSearch(node *TreeNode, sum *int) {
	if node == nil {
		return
	}
	depthFirstSearch(node.Right, sum)
	node.Val += *sum
	*sum = node.Val
	depthFirstSearch(node.Left, sum)
}

func main() {

}

//func arrayToBinaryTree(data []string) *TreeNode {
//	if len(data) < 1 {
//		fmt.Println("Empty array")
//		return nil
//	}
//	node := createNode(data[0])
//	var index int
//	line := make([]*TreeNode, 0)
//	line = append(line, node)
//	index = 1
//
//	for index < len(data) {
//		newLine := make([]*TreeNode, 0)
//		for i := 0; i < len(line); i++ {
//			if line[i] != nil {
//				if index >= len(data) {
//					break
//				}
//				line[i].Left = createNode(data[index])
//				newLine = append(newLine, line[i].Left)
//				index++
//				if index >= len(data) {
//					break
//				}
//				line[i].Right = createNode(data[index])
//				newLine = append(newLine, line[i].Right)
//				index++
//				if line[i].Left != nil {
//					if line[i].Val <= line[i].Left.Val {
//						fmt.Println("Incorrect binary tree. Left branch")
//						return nil
//					}
//				}
//				if line[i].Right != nil {
//					if line[i].Val >= line[i].Right.Val {
//						fmt.Println("Incorrect binary tree. Right branch")
//						return nil
//					}
//				}
//			}
//		}
//		line = newLine
//	}
//	return node
//}
//
//func createNode(text string) *TreeNode {
//	if text == "null" {
//		return nil
//	}
//	n, err := strconv.Atoi(text)
//	if err != nil {
//		panic(errors.New("incorrect value in array"))
//	}
//	return &TreeNode{Val: n}
//}
