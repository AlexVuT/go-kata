package main

import (
	"errors"
)

type ListNode struct {
	Val  int
	Next *ListNode
}

func mergeInBetween(list1 *ListNode, a int, b int, list2 *ListNode) *ListNode {
	merge(list1, a, b, list2)
	return list1
}

func merge(list1 *ListNode, a int, b int, list2 *ListNode) {
	if a > b {
		panic(errors.New("incorrect input"))
	}
	curr := list1
	tail := &ListNode{}
	for j := 0; j <= b+1; j++ {

		tail.Val = list1.Val
		list1 = list1.Next
		tail.Next = list1

	}

	for i := 0; i < a-1; i++ {
		curr = curr.Next
	}
	curr.Next = list2
	curr = list2
	for r := 0; curr.Next != nil; r++ {
		curr = curr.Next
	}
	curr.Next = tail
}

//func main() {
//	slice := []int{0, 1, 2, 3, 4, 5}
//	list := arrayToLinkedList(slice)
//	slice1 := []int{1000000, 1000001, 1000002}
//	list1 := arrayToLinkedList(slice1)
//	mergeInBetween(list, 3, 4, list1)
//	fmt.Println(list.Next.Next.Next.Next.Next.Next)
//
//	slice2 := []int{0, 1, 2, 3, 4, 5, 6}
//	list2 := arrayToLinkedList(slice2)
//	slice3 := []int{1000000, 1000001, 1000002, 1000003, 1000004}
//	list3 := arrayToLinkedList(slice3)
//	mergeInBetween(list2, 2, 5, list3)
//	fmt.Println(list2.Next.Next.Next.Next.Next.Next)
//}
//
//func arrayToLinkedList(nums []int) *ListNode {
//	list := &ListNode{Val: nums[0]}
//	curr := &ListNode{}
//	list.Next = curr
//	for i := 1; i < len(nums); i++ {
//		curr.Val = nums[i]
//		if i != len(nums)-1 {
//			curr.Next = &ListNode{}
//			curr = curr.Next
//		}
//	}
//
//	return list
//}
