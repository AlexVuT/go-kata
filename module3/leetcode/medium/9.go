package main

import (
	"fmt"
	"sort"
)

func checkArithmeticSubarrays(nums []int, l []int, r []int) []bool {
	if len(nums) < 2 || len(nums) > 500 || len(l) != len(r) || len(l) < 1 || len(l) > 500 {
		fmt.Println("Incorrect input")
		return nil
	}

	result := make([]bool, 0)
	for i := 0; i < len(l); i++ {
		origin := make([]int, 0)
		for _, v := range nums {
			if v < -100000 || v > 100000 {
				fmt.Println("Incorrect elements")
				return nil
			}
			origin = append(origin, v)
		}
		if l[i] >= r[i] || l[i] < 0 || l[i] >= len(nums) || r[i] > len(nums) {
			fmt.Println("Incorrect elements")
			return nil
		}
		temp := origin[l[i]:(r[i] + 1)]
		sort.Slice(temp, func(i, j int) bool {
			return temp[i] < temp[j]
		})
		step := temp[1] - temp[0]
		for t := 0; t < len(temp)-1; t++ {
			if temp[t] != temp[t+1]-step {
				result = append(result, false)
				goto back
			}
		}
		result = append(result, true)
	back:
		continue
	}
	return result
}

func main() {
	nums := []int{4, 6, 5, 9, 3, 7}
	l := []int{0, 0, 2}
	r := []int{2, 3, 5}
	fmt.Println(checkArithmeticSubarrays(nums, l, r))

	nums1 := []int{-12, -9, -3, -12, -6, 15, 20, -25, -20, -15, -10}
	l1 := []int{0, 1, 6, 4, 8, 7}
	r1 := []int{4, 4, 9, 7, 9, 10}
	fmt.Println(checkArithmeticSubarrays(nums1, l1, r1))
}
