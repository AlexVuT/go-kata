package main

import "fmt"

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func checkAllUnique(nums []int) bool {
	for iter := 0; iter < len(nums); iter++ {
		for i := 0 + iter; i < len(nums); i++ {
			if nums[iter] == nums[i] && iter != i {
				return false
			}
		}
	}
	return true
}

func constructMaximumBinaryTree(nums []int) *TreeNode {
	t := &TreeNode{}
	if len(nums) < 1 || len(nums) > 1000 {
		fmt.Println("Incorrect input")
		return nil
	}
	if checkAllUnique(nums) == false {
		fmt.Println("Not all numbers in array are unique")
		return nil
	}
	if len(nums) == 1 {
		t.Val = nums[0]
		return t
	}
	var max int
	left := make([]int, 0)
	right := make([]int, 0)

	for i := range nums {
		if nums[i] < 0 || nums[i] > 1000 {
			fmt.Println("Incorrect elements")
			return nil
		}
		if max < nums[i] {
			max = nums[i]
		}
	}
	t.Val = max
	for i := range nums {
		if max == nums[i] {
			if i != 0 {
				left = nums[:i]
				t.Left = constructMaximumBinaryTree(left)
			}
			if i != len(nums)-1 {
				right = nums[i+1:]
				t.Right = constructMaximumBinaryTree(right)
			}
		}
	}

	return t
}

//func main() {
//	slice := []int{3, 2, 1, 6, 0, 5}
//	tree := constructMaximumBinaryTree(slice)
//	fmt.Println(tree.Right.Left)
//
//	slice1 := []int{3, 2, 1}
//	tree1 := constructMaximumBinaryTree(slice1)
//	fmt.Println(tree1.Right)
//
//	slice2 := []int{3, 2, 1, 6, 0, 6}
//	tree2 := constructMaximumBinaryTree(slice2)
//	fmt.Println(tree2)
//}
