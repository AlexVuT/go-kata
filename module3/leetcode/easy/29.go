package main

import "fmt"

func xorOperation(n int, start int) int {
	if n < 1 || n > 1000 || start < 0 || start > 1000 {
		fmt.Println("Incorrect input")
		return 0
	}
	var result int
	nums := make([]int, 0)
	for i := 0; i < n; i++ {
		nums = append(nums, start+(i*2))
	}
	result = nums[0]
	for i := 1; i < len(nums); i++ {
		result = result ^ nums[i]
	}
	return result
}

func main() {
	fmt.Println(xorOperation(5, 0))
	fmt.Println(xorOperation(4, 3))
	fmt.Println(xorOperation(5, 1))
}
