package main

import "fmt"

func createTargetArray(nums []int, index []int) []int {
	if len(nums) < 1 || len(index) < 1 || len(nums) > 100 || len(index) > 100 ||
		len(nums) != len(index) {
		fmt.Println("Incorrect input")
		return nil
	}
	result := make([]int, 0)
	for i := 0; i < len(nums); i++ {
		if index[i] < 0 || index[i] > i {
			fmt.Println("Incorrect index")
			return nil
		}
		result = append(result[:index[i]], append([]int{nums[i]}, result[index[i]:]...)...)
	}
	return result
}

func main() {
	nums := []int{0, 1, 2, 3, 4}
	index := []int{0, 1, 2, 2, 1}
	fmt.Println(createTargetArray(nums, index))

	nums1 := []int{1, 2, 3, 4, 0}
	index1 := []int{0, 1, 2, 3, 0}
	fmt.Println(createTargetArray(nums1, index1))

	nums2 := []int{1, 2, 3, 0}
	index2 := []int{0, 1, 2, 3, 0}
	createTargetArray(nums2, index2)
}
