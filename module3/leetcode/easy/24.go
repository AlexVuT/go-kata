package main

import "fmt"

func decode(encoded []int, first int) []int {
	if len(encoded) < 1 || len(encoded) > 10000 {
		fmt.Println("Incorrect length of array")
		return nil
	}
	result := make([]int, 0)
	result = append(result, first)
	var temp int
	for i := 0; i < len(encoded); i++ {
		if encoded[i] < 0 || encoded[i] > 100000 {
			fmt.Println("Incorrect elements in array")
			return nil
		}
		for r := 0; ; r++ {
			if r^result[i] == encoded[i] {
				temp = r
				result = append(result, temp)
				break
			}
		}
	}
	return result
}

func main() {
	slice := []int{1, 2, 3}
	fmt.Println(decode(slice, 1))

	slice1 := []int{6, 2, 7, 3}
	fmt.Println(decode(slice1, 4))
}
