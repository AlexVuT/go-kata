package main

import (
	"fmt"
	"math/rand"
	"time"
)

func runningSum(nums []int) []int {
	if len(nums) < 1 {
		fmt.Println("Empty array")
		return nil
	}
	if len(nums) > 1000 {
		fmt.Println("Array is too big")
		return nil
	}
	result := make([]int, 0)
	for i := 0; i < len(nums); i++ {
		temp := nums[i]
		r := i
		for r != 0 {
			temp += nums[r-1]
			r--
		}
		result = append(result, temp)
	}
	return result
}

func generateSliceSignedAndUnsigned(n int) []int {
	res := make([]int, 0)
	rand.NewSource(time.Now().UnixNano())
	for i := 0; i < n; i++ {
		gen := rand.Intn(1000)
		gen++
		sign := rand.Intn(2)
		if sign == 1 {
			gen = -gen
		}
		res = append(res, gen)
	}
	return res
}

func main() {
	slice10 := generateSliceSignedAndUnsigned(10)
	fmt.Println(slice10)
	fmt.Println(runningSum(slice10))
	slice100 := generateSliceSignedAndUnsigned(100)
	fmt.Println(slice100)
	fmt.Println(runningSum(slice100))
	slice1000 := generateSliceSignedAndUnsigned(1000)
	fmt.Println(slice1000)
	fmt.Println(runningSum(slice1000))
}
