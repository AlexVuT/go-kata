package main

import "fmt"

func smallestEvenMultiple(n int) int {
	if n < 1 || n > 150 {
		fmt.Println("Incorrect input")
		return 0
	}
	for i := 1; ; i++ {
		if i%n == 0 && i%2 == 0 {
			return i
		}
	}
}

func main() {
	fmt.Println(smallestEvenMultiple(5))
	fmt.Println(smallestEvenMultiple(6))
	fmt.Println(smallestEvenMultiple(9))
	smallestEvenMultiple(151)
}
