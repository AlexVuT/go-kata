package main

import (
	"fmt"
	"math/rand"
	"time"
)

func shuffle(nums []int, n int) []int {
	if n < 1 || n > 500 || len(nums) != 2*n {
		fmt.Println("Incorrect input")
		return nil
	}
	result := make([]int, 0)
	for i := 0; i < len(nums)/2; i++ {
		result = append(result, nums[i], nums[i+n])
	}
	return result
}

func generateSlice(n int) []int {
	res := make([]int, 0)
	rand.NewSource(time.Now().UnixNano())
	for i := 0; i < n; i++ {
		gen := rand.Intn(1000)
		gen++
		res = append(res, gen)
	}
	return res
}

func main() {
	slice10 := generateSlice(10)
	fmt.Println(slice10)
	fmt.Println(shuffle(slice10, len(slice10)/2))
	slice100 := generateSlice(100)
	fmt.Println(slice100)
	fmt.Println(shuffle(slice100, len(slice100)/2))
	slice250 := generateSlice(250)
	fmt.Println(slice250)
	fmt.Println(shuffle(slice250, len(slice250)/2))
}
