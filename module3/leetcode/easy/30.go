package main

import (
	"fmt"
)

func countGoodTriplets(nums []int, a int, b int, c int) int {
	if len(nums) < 3 || len(nums) > 100 || a < 0 || a > 1000 || b < 0 || b > 1000 || c < 0 || c > 1000 {
		fmt.Println("Incorrect input")
		return 0
	}

	var checkA, checkB, checkC, count int
	for i := 0; i < len(nums)-2; i++ {
		if nums[i] < 0 || nums[i] > 1000 {
			fmt.Println("Incorrect elements")
			return 0
		}
		checkA = nums[i] - nums[i+1]
		checkB = nums[i+1] - nums[i+2]
		checkC = nums[i] - nums[i+2]
		if checkA < 0 {
			checkA = -checkA
		}
		if checkB < 0 {
			checkB = -checkB
		}
		if checkC < 0 {
			checkC = -checkC
		}

		if checkA <= a && checkB <= b &&
			checkC <= c {
			count++
		}
	}
	return count
}

func main() {
	nums := []int{3, 0, 1, 1, 9, 7}
	fmt.Println(countGoodTriplets(nums, 7, 2, 3))
	nums1 := []int{1, 1, 2, 2, 3}
	fmt.Println(countGoodTriplets(nums1, 0, 0, 1))
}
