package main

import (
	"fmt"
	"math/rand"
	"sort"
	"time"
)

func findKthPositive(arr []int, k int) int {
	if k < 1 || k > 1000 {
		fmt.Println("Incorrect number")
		return 0
	}
	var count int
	example := generateSliceWithUniqueElements(1000)
	for i := 0; i < len(example); i++ {
		if checkUniqueElement(arr, example[i]) == true {
			count++
			if count == k {
				return example[i]
			}
		}
	}
	fmt.Println("Number not found")
	return 0
}

func generateSliceNotSorted(n int) []int {
	res := make([]int, 0)
	rand.NewSource(time.Now().UnixNano())
	for i := 0; i < n; i++ {
		gen := rand.Intn(3 * n)
		gen++
		for checkUniqueElement(res, gen) == false || gen > 1000 {
			gen = rand.Intn(i + 1)
			gen++
		}
		res = append(res, gen)
	}
	return res
}

func generateSliceWithUniqueElements(n int) []int {
	res := make([]int, 0)
	rand.NewSource(time.Now().UnixNano())
	for i := 0; i < n; i++ {
		gen := rand.Intn(i + 1)
		gen++
		for checkUniqueElement(res, gen) == false {
			gen = rand.Intn(i + 1)
			gen++
		}
		res = append(res, gen)
	}
	return res
}

func checkUniqueElement(nums []int, elem int) bool {
	for i := 0; i < len(nums); i++ {
		if nums[i] == elem {
			return false
		}
	}
	return true
}

func SortSliceFromSmallToBig(slice []int) []int {
	sort.Slice(slice, func(i, j int) bool {
		return slice[i] < slice[j]
	})
	return slice
}

func main() {
	slice := generateSliceNotSorted(10)
	fmt.Println(slice)
	slice = SortSliceFromSmallToBig(slice)
	fmt.Println(slice)
	fmt.Println(findKthPositive(slice, 7))

	slice1 := generateSliceNotSorted(100)
	fmt.Println(slice1)
	slice1 = SortSliceFromSmallToBig(slice1)
	fmt.Println(slice1)
	fmt.Println(findKthPositive(slice1, 54))

	slice2 := generateSliceNotSorted(500)
	fmt.Println(slice2)
	slice2 = SortSliceFromSmallToBig(slice2)
	fmt.Println(slice2)
	fmt.Println(findKthPositive(slice2, 377))
}
