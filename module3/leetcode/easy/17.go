package main

import "fmt"

func checkSymbols(sentences string) bool {
	if sentences[0] == ' ' || sentences[len(sentences)-1] == ' ' {
		return false
	}
	for i := 0; i < len(sentences); i++ {
		if sentences[i] == ' ' {
			if sentences[i+1] == ' ' {
				return false
			} else {
				continue
			}
		}
		if sentences[i] >= 'a' && sentences[i] <= 'z' {
			continue
		} else {
			return false
		}
	}
	return true
}

func mostWordsFound(sentences []string) int {
	if len(sentences) < 1 || len(sentences) > 100 {
		fmt.Println("Incorrect input")
		return 0
	}
	var max, count int
	for i := 0; i < len(sentences); i++ {
		if len(sentences[i]) < 1 || len(sentences[i]) > 100 {
			fmt.Println("Incorrect input")
			return 0
		}
		if checkSymbols(sentences[i]) == false {
			fmt.Println("Incorrect sentences")
			return 0
		}
		count = 1
		for r := 0; r < len(sentences[i]); r++ {
			if sentences[i][r] == ' ' {
				count++
			}
			if max < count {
				max = count
			}
		}
	}
	return max
}

func main() {
	sentences := []string{"alice and bob love leetcode", "i think so too", "this is great thanks very much"}
	fmt.Println(mostWordsFound(sentences))
	sentences1 := []string{"please wait", "continue to fight", "continue to win"}
	fmt.Println(mostWordsFound(sentences1))
	withTwoSpaces := []string{"alice  and bob love leetcode", "i think so too", "this is great thanks very much"}
	mostWordsFound(withTwoSpaces)
	withSymbols := []string{"hello world!"}
	mostWordsFound(withSymbols)
}
