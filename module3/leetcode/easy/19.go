package main

import (
	"fmt"
	"sort"
)

func minimumSum(num int) int {
	if num < 1000 || num > 9999 {
		fmt.Println("Incorrect input")
		return 0
	}
	var first, second, result int
	nums := make([]int, 0)
	nums = append(nums, num%10, (num%100)/10, (num%1000)/100, num/1000)
	sort.Ints(nums)
	first = nums[0]*10 + nums[2]
	second = nums[1]*10 + nums[3]
	result = first + second
	return result
}

func main() {
	fmt.Println(minimumSum(2932))
	fmt.Println(minimumSum(4009))
	fmt.Println(minimumSum(4444))
	minimumSum(417)
}
