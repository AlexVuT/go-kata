package main

import "fmt"

func kidsWithCandies(candies []int, extraCandies int) []bool {
	if len(candies) < 2 || len(candies) > 100 || extraCandies < 1 || extraCandies > 50 {
		fmt.Println("Incorrect input")
		return nil
	}
	result := make([]bool, 0)
	for i := 0; i < len(candies); i++ {
		if candies[i] < 1 || candies[i] > 100 {
			fmt.Println("Incorrect elements")
			return nil
		}
		for r := 0; r < len(candies); r++ {
			if candies[i]+extraCandies < candies[r] {
				result = append(result, false)
				break
			} else if r == len(candies)-1 {
				result = append(result, true)
			}
		}
	}
	return result
}

func main() {
	candies := []int{2, 3, 5, 1, 3}
	fmt.Println(kidsWithCandies(candies, 3))

	candies1 := []int{4, 2, 1, 1, 2}
	fmt.Println(kidsWithCandies(candies1, 1))

	candies2 := []int{12, 1, 12}
	fmt.Println(kidsWithCandies(candies2, 10))

	candiesTooMany := []int{34, 55, 17, 5, 6, 174, 54, 9}
	kidsWithCandies(candiesTooMany, 7)
}
