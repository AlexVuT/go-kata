package main

import "fmt"

func subtractProductAndSum(n int) int {
	var multiply, sum, result int
	if n < 1 || n > 100000 {
		fmt.Println("Incorrect input")
		return 0
	}
	if n == 100000 {
		return -1
	} else if n >= 10000 {
		multiply = n % 10
		multiply *= (n / 10000) * ((n % 10000) / 1000) * ((n % 1000) / 100) * ((n % 100) / 10)
		sum = n % 10
		sum += (n / 10000) + ((n % 10000) / 1000) + ((n % 1000) / 100) + ((n % 100) / 10)
	} else if n >= 1000 {
		multiply = n % 10
		multiply *= (n / 1000) * ((n % 1000) / 100) * ((n % 100) / 10)
		sum = n % 10
		sum += (n / 1000) + ((n % 1000) / 100) + ((n % 100) / 10)
	} else if n >= 100 {
		multiply = n % 10
		multiply *= (n / 100) * ((n % 100) / 10)
		sum = n % 10
		sum += (n / 100) + ((n % 100) / 10)
	} else if n >= 10 {
		multiply = n % 10
		multiply *= n / 10
		sum = n % 10
		sum += n / 10
	} else if n <= 9 {
		return 0
	}
	result = multiply - sum
	return result
}

func main() {
	fmt.Println(subtractProductAndSum(234))
	fmt.Println(subtractProductAndSum(4421))
	fmt.Println(subtractProductAndSum(7))
	fmt.Println(subtractProductAndSum(65789))
}
