package main

import (
	"fmt"
	"math/rand"
	"time"
)

func numIdenticalPairs(nums []int) int {
	if len(nums) < 1 {
		fmt.Println("Empty array")
		return 0
	}
	if len(nums) > 100 {
		fmt.Println("Array is too big")
		return 0
	}
	var count int
	for i := 0; i < len(nums); i++ {
		for j := 0 + i; j < len(nums); j++ {
			if nums[i] == nums[j] && i != j {
				count++
			}
		}
	}
	return count
}

func generateSlice(n int) []int {
	res := make([]int, 0)
	rand.NewSource(time.Now().UnixNano())
	for i := 0; i < n; i++ {
		gen := rand.Intn(100)
		gen++
		res = append(res, gen)
	}
	return res
}

func main() {
	slice := []int{1, 2, 3, 1, 1, 3}
	fmt.Println(slice)
	fmt.Println(numIdenticalPairs(slice))

	slice10 := generateSlice(10)
	fmt.Println(slice10)
	fmt.Println(numIdenticalPairs(slice10))

	slice50 := generateSlice(50)
	fmt.Println(slice50)
	fmt.Println(numIdenticalPairs(slice50))

	slice100 := generateSlice(100)
	fmt.Println(slice100)
	fmt.Println(numIdenticalPairs(slice100))

	slice101 := generateSlice(101)
	fmt.Println(slice101)
	fmt.Println(numIdenticalPairs(slice101))
}
