package main

import "fmt"

func numberOfMatches(n int) int {
	if n < 1 {
		fmt.Println("Not enough teams")
		return 0
	}
	if n > 200 {
		fmt.Println("Too many teams")
		return 0
	}
	var matches int
	for n > 1 {
		if n%2 == 0 {
			matches += n / 2
			n = n / 2
		} else if n%2 == 1 {
			matches += (n - 1) / 2
			n = (n-1)/2 + 1
		}
	}
	fmt.Println(matches)
	return matches
}

func main() {
	numberOfMatches(0)
	numberOfMatches(1)
	numberOfMatches(37)
	numberOfMatches(14)
	numberOfMatches(200)
	numberOfMatches(257)
}
