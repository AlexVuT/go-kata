package main

import (
	"fmt"
	"math/rand"
	"time"
)

type ParkingSystem struct {
	big    int
	medium int
	small  int
}

func Constructor(big int, medium int, small int) ParkingSystem {
	if big < 0 || medium < 0 || small < 0 ||
		big > 1000 || medium > 1000 || small > 1000 {
		fmt.Println("Incorrect input")
		return ParkingSystem{}
	}
	p := ParkingSystem{
		big:    big,
		medium: medium,
		small:  small,
	}
	return p
}

func (p *ParkingSystem) AddCar(carType int) bool {
	if carType == 1 {
		if p.big == 0 {
			return false
		}
		p.big--
		return true
	}
	if carType == 2 {
		if p.medium == 0 {
			return false
		}
		p.medium--
		return true
	}
	if carType == 3 {
		if p.small == 0 {
			return false
		}
		p.small--
		return true
	}
	fmt.Println("Incorrect input")
	return false
}

func (p *ParkingSystem) JustAnotherDay(n int) {
	if n > 1000 {
		fmt.Println("Too many requests")
		return
	}
	rand.NewSource(time.Now().UnixNano())
	for i := 0; i < n; i++ {
		x := rand.Intn(3)
		x++
		fmt.Println(p.AddCar(x))

	}
}

func main() {
	p := Constructor(1, 1, 0)
	fmt.Println(p.AddCar(1))
	fmt.Println(p.AddCar(2))
	fmt.Println(p.AddCar(3))
	fmt.Println(p.AddCar(1))
	fmt.Println()
	lot := Constructor(10, 10, 10)
	lot.JustAnotherDay(50)
}
