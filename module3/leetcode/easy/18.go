package main

import (
	"fmt"
	"math/rand"
	"time"
)

func differenceOfSum(nums []int) int {
	if len(nums) < 1 || len(nums) > 2000 {
		fmt.Println("Incorrect input")
		return 0
	}

	var result, elements, numbers, temp int
	for i := 0; i < len(nums); i++ {
		if nums[i] < 1 || nums[i] > 2000 {
			fmt.Println("Incorrect elements")
			return 0
		}
		elements += nums[i]
		if nums[i] < 10 {
			numbers += nums[i]
			continue
		}
		if nums[i] > 999 {
			temp = nums[i] % 10
			temp += ((nums[i] % 100) / 10) + ((nums[i] % 1000) / 100) + nums[i]/1000
			numbers += temp
			continue
		}
		if nums[i] > 99 {
			temp = nums[i] % 10
			temp += ((nums[i] % 100) / 10) + (nums[i] / 100)
			numbers += temp
			continue
		}
		if nums[i] > 9 {
			temp = nums[i] % 10
			temp += nums[i] / 10
			numbers += temp
			continue
		}
	}
	result = elements - numbers
	return result
}

func generateSlice(n int) []int {
	res := make([]int, 0)
	rand.NewSource(time.Now().UnixNano())
	for i := 0; i < n; i++ {
		gen := rand.Intn(2000)
		gen++
		res = append(res, gen)
	}
	return res
}

func main() {
	nums := []int{1, 15, 6, 3}
	fmt.Println(differenceOfSum(nums))

	nums1 := []int{1, 2, 3, 4}
	fmt.Println(differenceOfSum(nums1))

	numsWithZero := []int{24, 8, 94, 5, 0, 46}
	differenceOfSum(numsWithZero)

	numsWithSign := []int{77, 9, -3, 11, 57}
	differenceOfSum(numsWithSign)

	nums100 := generateSlice(100)
	fmt.Println(differenceOfSum(nums100))
	nums1000 := generateSlice(1000)
	fmt.Println(differenceOfSum(nums1000))
	nums2000 := generateSlice(2000)
	fmt.Println(differenceOfSum(nums2000))
}
