package main

import "fmt"

func convertTemperature(celsius float64) []float64 {
	if celsius < 0 || celsius > 1000 {
		fmt.Println("You can only input temperature in range from 0 to 1000")
		return nil
	}
	ans := make([]float64, 0)
	kelvin := celsius + 273.15
	fahrenheit := celsius*1.80 + 32.00
	ans = append(ans, kelvin, fahrenheit)
	fmt.Printf("[%.5f %.5f]\n", ans[0], ans[1])
	return ans
}

func main() {
	convertTemperature(36.50)
	convertTemperature(122.11)
	convertTemperature(473.37)
	convertTemperature(0)
	convertTemperature(-12)
	convertTemperature(1456)
}
