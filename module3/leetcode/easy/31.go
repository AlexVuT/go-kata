package main

import (
	"fmt"
	"sort"
)

func checkName(name string) bool {
	if name[0] >= 'A' && name[0] <= 'Z' {
	} else {
		return false
	}
	for i := 1; i < len(name); i++ {
		if name[i] >= 'a' && name[i] <= 'z' {
		} else {
			return false
		}
	}

	return true
}

func sortByHeight(people []string, height []int) []string {
	if len(people) != len(height) || len(people) < 1 || len(people) > 103 {
		fmt.Println("Incorrect input")
		return nil
	}
	for _, v := range people {
		if checkName(v) == false {
			fmt.Println("Incorrect name")
			return nil
		}
	}
	for i := 0; i < len(height); i++ {
		for r := 0 + i; r < len(height); r++ {
			if height[i] == height[r] && i != r {
				fmt.Println("Not all heights are unique")
				return nil
			}
		}
	}

	sort.Slice(people, func(i, j int) bool {
		return height[i] > height[j]
	})
	return people
}

func main() {
	names := []string{"Mary", "John", "Emma"}
	heights := []int{180, 165, 170}
	fmt.Println(sortByHeight(names, heights))

	names1 := []string{"Alice", "Bob", "Bob"}
	heights1 := []int{155, 185, 150}
	fmt.Println(sortByHeight(names1, heights1))

	namesIncorr := []string{"Mary", "John", "Emma", "sandy"}
	heights2 := []int{180, 165, 170, 177}
	sortByHeight(namesIncorr, heights2)

	names2 := []string{"Mary", "John", "Emma", "Adam"}
	heightsIncorr := []int{180, 165, 170, 165}
	sortByHeight(names2, heightsIncorr)
}
