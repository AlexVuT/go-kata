package main

import "fmt"

func smallerNumbersThanCurrent(nums []int) []int {
	if len(nums) < 2 || len(nums) > 500 {
		fmt.Println("Incorrect input")
		return nil
	}
	var count int
	result := make([]int, 0)
	for i := 0; i < len(nums); i++ {
		if nums[i] < 0 || nums[i] > 100 {
			fmt.Println("Incorrect elements")
			return nil
		}
		count = 0
		for r := 0; r < len(nums); r++ {
			if nums[i] > nums[r] && i != r {
				count++
			}
		}
		result = append(result, count)
	}
	return result
}

func main() {
	nums := []int{8, 1, 2, 2, 3}
	fmt.Println(smallerNumbersThanCurrent(nums))
	nums1 := []int{6, 5, 4, 8}
	fmt.Println(smallerNumbersThanCurrent(nums1))
	nums2 := []int{7, 7, 7, 7}
	fmt.Println(smallerNumbersThanCurrent(nums2))
	numsOne := []int{15}
	smallerNumbersThanCurrent(numsOne)
	numsLarge := []int{15, 56, 3, 0, 7, 189, 77}
	smallerNumbersThanCurrent(numsLarge)
}
