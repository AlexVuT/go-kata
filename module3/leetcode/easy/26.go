package main

import "fmt"

func decompressRLElist(nums []int) []int {
	if len(nums) < 2 || len(nums) > 100 || len(nums)%2 != 0 {
		fmt.Println("Incorrect input")
		return nil
	}
	result := make([]int, 0)
	for i := 0; i < len(nums); i += 2 {
		if nums[i] < 1 || nums[i] > 100 {
			fmt.Println("Incorrect elements")
			return nil
		}
		for r := 0; r < nums[i]; r++ {
			result = append(result, nums[i+1])
		}
	}
	return result
}

func main() {
	slice := []int{1, 2, 3, 4}
	fmt.Println(decompressRLElist(slice))

	slice1 := []int{1, 1, 2, 3}
	fmt.Println(decompressRLElist(slice1))

	slice2 := []int{1, 2, 3, 4, 7}
	decompressRLElist(slice2)

	slice3 := []int{1, 1, -2, 3}
	decompressRLElist(slice3)
}
