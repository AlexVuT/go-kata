package main

import "fmt"

func uniqueMorseRepresentations(words []string) int {
	var count int
	morze := []string{".-", "-...", "-.-.", "-..", ".", "..-.", "--.", "....", "..", ".---", "-.-", ".-..", "--", "-.", "---",
		".--.", "--.-", ".-.", "...", "-", "..-", "...-", ".--", "-..-", "-.--", "--.."}
	alphabet := []string{"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"}
	final := make([]string, 0)
	var check bool
	var text string
	for i := 0; i < len(words); i++ {
		text = ""
		slice := make([]byte, 0)
		slice = append(slice, words[i]...)
		word := make([]string, 0)
		for iter := 0; iter < len(slice); iter++ {
			word = append(word, string(slice[iter]))
		}
		for r := 0; r < len(slice); r++ {
			for t := 0; t < len(alphabet); t++ {
				if word[r] == alphabet[t] {
					text += morze[t]
				}
			}
		}
		final = append(final, text)
	}
	for b := 0; b < len(final); b++ {
		if check == false {
			count++
		}
		check = false
		for c := 0 + b; c < len(final); c++ {
			if final[b] == final[c] && b != c {
				check = true
				break
			}
		}
	}
	fmt.Println(final)
	return count
}

func main() {

	slice := []string{"gin", "zen", "gig", "msg", "gin", "zen", "gin"}
	slice1 := []string{"das", "sad", "qwerty", "zxc", "ytrewq", "zxc", "das"}
	fmt.Println(uniqueMorseRepresentations(slice))
	fmt.Println(uniqueMorseRepresentations(slice1))
}
