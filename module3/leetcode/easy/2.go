package main

import (
	"fmt"
	"math/rand"
	"time"
)

func getConcatenation(nums []int) []int {
	ans := make([]int, 0)
	for iter := 0; iter < 2; iter++ {
		for i := 0; i < len(nums); i++ {
			ans = append(ans, nums[i])
		}
	}
	return ans
}

func getConcatenation2(nums []int) []int {
	ans := make([]int, 0)
	for i := 0; i < 2; i++ {
		ans = append(ans, nums...)
	}
	return ans
}

func generateSlice(n int) []int {
	rand.NewSource(time.Now().UnixNano())
	res := make([]int, 0)
	for i := 0; i < n; i++ {
		res = append(res, rand.Intn(1000)+1)
	}
	return res
}

func main() {
	slice := generateSlice(10)
	slice1 := generateSlice(50)
	slice2 := generateSlice(100)

	slice3 := generateSlice(10)
	slice4 := generateSlice(50)
	slice5 := generateSlice(100)

	slice = getConcatenation(slice)
	slice1 = getConcatenation(slice1)
	slice2 = getConcatenation(slice2)

	slice3 = getConcatenation2(slice3)
	slice4 = getConcatenation2(slice4)
	slice5 = getConcatenation2(slice5)

	fmt.Println(slice)
	fmt.Println(slice1)
	fmt.Println(slice2)
	fmt.Println(slice3)
	fmt.Println(slice4)
	fmt.Println(slice5)

}
