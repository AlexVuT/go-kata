package main

import (
	"fmt"
	"math/rand"
	"time"
)

func maximumWealth(accounts [][]int) int {
	if len(accounts) < 1 || len(accounts) > 50 {
		fmt.Println("Incorrect length")
		return 0
	}

	var count int
	max := make([]int, 1)
	for i := 0; i < len(accounts); i++ {
		if len(accounts[i]) < 1 || len(accounts[i]) > 50 {
			fmt.Println("Incorrect length")
			return 0
		}
		count = 0
		for r := 0; r < len(accounts[i]); r++ {
			count += accounts[i][r]
		}
		if max[0] < count {
			max[0] = count
		}
	}
	return max[0]
}

func generateSlice(n int) []int {
	res := make([]int, 0)
	rand.NewSource(time.Now().UnixNano())
	for i := 0; i < n; i++ {
		gen := rand.Intn(100)
		gen++
		res = append(res, gen)
	}
	return res
}

func main() {
	account := [][]int{
		{1, 2, 3},
		{3, 2, 1},
	}
	fmt.Println(maximumWealth(account))

	account1 := [][]int{
		{1, 5},
		{7, 3},
		{3, 5},
	}
	fmt.Println(maximumWealth(account1))

	account2 := [][]int{
		{2, 8, 7},
		{7, 1, 3},
		{1, 9, 5},
	}
	fmt.Println(maximumWealth(account2))

	emptyAcc := make([][]int, 0)
	maximumWealth(emptyAcc)

	bigAcc := [][]int{
		generateSlice(50),
		generateSlice(50),
	}
	fmt.Println(bigAcc)
	fmt.Println(maximumWealth(bigAcc))
}
