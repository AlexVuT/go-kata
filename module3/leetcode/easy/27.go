package main

import "fmt"

func balancedStringSplit(s string) int {
	if len(s) < 2 || len(s) > 1000 || len(s)%2 != 0 {
		fmt.Println("Incorrect input")
		return 0
	}
	var l, r, count int
	for i := 0; i < len(s); i++ {
		if s[i] != 'L' && s[i] != 'R' {
			fmt.Println("Incorrect elements")
			return 0
		}
		if s[i] == 'L' {
			l++
		}
		if s[i] == 'R' {
			r++
		}
		if l == r {
			count++
			l = 0
			r = 0
		}
	}
	return count
}

func main() {
	s := "RLRRLLRLRL"
	fmt.Println(balancedStringSplit(s))
	s1 := "RLRRRLLRLL"
	fmt.Println(balancedStringSplit(s1))
	s2 := "LLLLRRRR"
	fmt.Println(balancedStringSplit(s2))

	s3 := "LLALLRRRR"
	balancedStringSplit(s3)
}
