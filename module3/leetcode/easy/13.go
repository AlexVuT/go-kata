package main

import "fmt"

func UniqueJewels(jewels string) bool {
	for i := 0; i < len(jewels); i++ {
		for r := 0 + i; r < len(jewels); r++ {
			if jewels[i] == jewels[r] && i != r {
				return false
			}
		}
	}
	return true
}

func checkJewelsAndStones(jewels string, stones string) bool {
	for i := 0; i < len(jewels); i++ {
		if (jewels[i] >= 'a' && jewels[i] <= 'z') ||
			(jewels[i] >= 'A' && jewels[i] <= 'Z') {
			continue
		} else {
			return false
		}
	}
	for i := 0; i < len(stones); i++ {
		if (stones[i] >= 'a' && stones[i] <= 'z') ||
			(stones[i] >= 'A' && stones[i] <= 'Z') {
			continue
		} else {
			return false
		}
	}
	return true
}

func numJewelsInStones(jewels string, stones string) int {
	if len(jewels) < 1 || len(stones) < 1 {
		fmt.Println("Empty string")
		return 0
	}
	if len(jewels) > 50 || len(stones) > 50 {
		fmt.Println("String is too big")
		return 0
	}
	if checkJewelsAndStones(jewels, stones) == false {
		fmt.Println("Incorrect jewels or stones")
		return 0
	}
	if UniqueJewels(jewels) == false {
		fmt.Println("Dublicated jewels")
		return 0
	}
	var count int

	for i := 0; i < len(jewels); i++ {
		for r := 0; r < len(stones); r++ {
			if jewels[i] == stones[r] {
				count++
			}
		}
	}
	return count
}

func main() {
	jewels := "aA"
	stones := "aAAbbbb"
	fmt.Println(numJewelsInStones(jewels, stones))

	jewels1 := "z"
	stones1 := "ZZ"
	fmt.Println(numJewelsInStones(jewels1, stones1))

	jewels2 := "aAbC"
	stones2 := "aaaccbbBbAAccBBBcCC"
	fmt.Println(numJewelsInStones(jewels2, stones2))

	jewelsNotEnglish := "AbcыезdfюMn"
	stonesNotEnglsih := "AaBdddffqqфQcC"
	numJewelsInStones(jewelsNotEnglish, stonesNotEnglsih)

	jewelsDublicate := "AaBbCCc"
	numJewelsInStones(jewelsDublicate, stones2)
}
