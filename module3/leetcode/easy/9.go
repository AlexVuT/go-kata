package main

import "fmt"

func finalValueAfterOperations(operations []string) int {
	if len(operations) < 1 {
		fmt.Println("No inputs")
		return 0
	}
	if len(operations) > 100 {
		fmt.Println("Too many inputs")
		return 0
	}
	var result int
	for i := 0; i < len(operations); i++ {
		if operations[i] == "++X" || operations[i] == "X++" {
			result++
		} else if operations[i] == "--X" || operations[i] == "X--" {
			result--
		} else {
			fmt.Println("Incorrect input")
			return 0
		}
	}
	return result
}

func main() {
	input := []string{"X--", "++X", "X++", "X++", "X--", "--X", "X--", "--X", "X--", "--X", "X--", "--X", "++X", "X++", "X++"}
	fmt.Println(finalValueAfterOperations(input))
	inputIncorrect := []string{"X--", "++X", "X++", "hello", "X++", "X--"}
	finalValueAfterOperations(inputIncorrect)
	inputEmpty := make([]string, 0)
	finalValueAfterOperations(inputEmpty)
}
