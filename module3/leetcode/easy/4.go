package main

import (
	"fmt"
	"math/rand"
	"time"
)

func checkAllUnique(nums []int) bool {
	for iter := 0; iter < len(nums); iter++ {
		for i := 0 + iter; i < len(nums); i++ {
			if nums[iter] == nums[i] && iter != i {
				return false
			}
		}
	}
	return true
}

func checkUniqueElement(nums []int, elem int) bool {
	for i := 0; i < len(nums); i++ {
		if nums[i] == elem {
			return false
		}
	}
	return true
}

func checkValues(nums []int) bool {
	for i := 0; i < len(nums); i++ {
		if nums[i] >= len(nums) || nums[i] < 0 {
			return false
		}
	}
	return true
}

func buildArray(nums []int) []int {
	if checkAllUnique(nums) == false {
		fmt.Println("Not all numbers in slice are unique")
		return nil
	}
	if checkValues(nums) == false {
		fmt.Println("At least one element is bigger or equal slice's length or less than zero")
		return nil
	}
	res := make([]int, 0)
	for i := 0; i < len(nums); i++ {
		res = append(res, nums[nums[i]])
	}
	fmt.Println(res)
	return res
}

func generateSliceWithUniqueElements(n int) []int {
	res := make([]int, 0)
	rand.NewSource(time.Now().UnixNano())
	for i := 0; i < n; i++ {
		gen := rand.Intn(n)
		for checkUniqueElement(res, gen) == false {
			gen = rand.Intn(i + 1)
		}
		res = append(res, gen)
	}
	return res
}

func main() {
	genSlice := generateSliceWithUniqueElements(10)
	genSlice1 := generateSliceWithUniqueElements(100)
	genSlice2 := generateSliceWithUniqueElements(500)
	fmt.Println(genSlice)
	buildArray(genSlice)
	fmt.Println(genSlice1)
	buildArray(genSlice1)
	fmt.Println(genSlice2)
	buildArray(genSlice2)
}
