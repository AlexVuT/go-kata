package main

import "fmt"

func countDigits(num int) int {
	var count, first, second, third, fourth int
	if num < 1 || num > 9999 {
		fmt.Println("Incorrect input")
		return 0
	}
	if num >= 1000 {
		first = num % 10
		second = (num % 100) / 10
		third = (num % 1000) / 100
		fourth = num / 1000
		if first == 0 || second == 0 || third == 0 || fourth == 0 {
			fmt.Println("Number contains zero")
			return 0
		}

		if num%first == 0 {
			count++
		}
		if num%second == 0 {
			count++
		}
		if num%third == 0 {
			count++
		}
		if num%fourth == 0 {
			count++
		}
		return count

	} else if num >= 100 {
		first = num % 10
		second = (num % 100) / 10
		third = num / 100
		if first == 0 || second == 0 || third == 0 {
			fmt.Println("Number contains zero")
			return 0
		}

		if num%first == 0 {
			count++
		}

		if num%second == 0 {
			count++
		}

		if num%third == 0 {
			count++
		}
		return count

	} else if num >= 10 {
		first = num % 10
		second = num / 10
		if first == 0 || second == 0 {
			fmt.Println("Number contains zero")
			return 0
		}

		if num%first == 0 {
			count++
		}

		if num%second == 0 {
			count++
		}
		return count

	} else if num <= 9 {
		return 1
	}
	return count
}

func main() {
	fmt.Println(countDigits(7))
	fmt.Println(countDigits(121))
	fmt.Println(countDigits(1248))
	countDigits(1048)
}
