package main

import "fmt"

func tribonacci(n int) int {
	if n > 37 {
		fmt.Println("Incorrect number")
		return 0
	}
	slice := make([]int, 0)
	slice = append(slice, 0, 1, 1)
	for i := 3; i < 38; i++ {
		slice = append(slice, slice[i-1]+slice[i-2]+slice[i-3])
	}
	return slice[n]
}

func main() {

	fmt.Println(tribonacci(4))
	fmt.Println(tribonacci(25))

}
