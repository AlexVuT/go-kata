package main

import (
	"fmt"
	"strings"
)

func defangIPaddr(address string) string {
	var protected string
	protected = strings.Replace(address, ".", "[.]", -1)
	return protected
}

func main() {
	addr := "1.1.1.1"
	addr1 := "192.168.1.1"
	addr = defangIPaddr(addr)
	fmt.Println(addr)
	addr1 = defangIPaddr(addr1)
	fmt.Println(addr1)
}
